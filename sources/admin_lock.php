<?php 
if(!defined("SUPINIT")){exit;}
if(!USER){exit;}
if(!ADMIN){exit;}
if(!defined("ADMIN_DORM")) {exit;}

if(ADMIN_DORM != 6){
  echo err("Только для 6-го общежития.");
  return;
}

if($_GET['since'] && ( ($s = strtotime($_GET['since'])) != -1 ) )
	$s = floor($s / 60);
else
	$s = start_of_the_day(t());
	
if($_GET['until'] && ( ($u = strtotime($_GET['until'])) != -1 ) )
	$u = floor($u / 60);
else
	$u = t();
	
if(($_GET['add'])){
	if(!($row = get_card($_GET['add'])))
		$row = array('card' => $_GET['add']);
	foreach($row as $key => $val)
		$row[$key] = htmlspecialchars($val);
?>
<center>
<form method="post" action="?act=admin&amp;sub=lock&amp;upd"  >
<table width="700px" cellspacing="10px">
<tr>
	<td colspan="2" align="center"><h3>Редактирование карты доступа</h3></td>
</tr>

<tr>
	<td align="right">Карта:</td>
	<td ><input type="text" name="card" style="width:400px; border:none" readonly="readonly"
    		 value="<?php echo $row['card'] ?>" /></td>
</tr>
<tr>
	<td align="right">Персона:</td>
	<td ><input type="text" name="name" style="width:500px" value="<?php echo $row['name'] ?>" /></td>
</tr>
<tr>
	<td align="right">Комната:</td>
	<td >
        <input type="text" name="dorm" style="width:20px" value="<?php echo $row['dorm'] ?>" /> - 
        <input type="text" name="room" style="width:50px" value="<?php echo $row['room'] ?>" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        Группа: <input type="text" name="group" style="width:60px" value="<?php echo $row['group'] ?>" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        День рождения: <input type="text" name="birthday" id="bd" style="width:130px" value="<?php echo $row['birthday'] ?>" />
   </td>
</tr>
<tr>
	<td align="right">Номер карты:</td>
	<td ><input type="text" name="cardnumber" id="cn" style="width:400px" value="<?php echo $row['cardnumber'] ?>" />
    <a href="javascript:gen()">Сгенерировать</a></td>
</tr>
<tr>
	<td align="right">Код:</td>
	<td ><span id='regcode'></span></td>
</tr>
<tr>
	<td align="right">Годна до:</td>
	<td ><input type="text" name="expire" id="vd" style="width:130px" value="<?php echo $row['expire'] ?>" /></td>
</tr>
<tr>
	<td colspan="2" align="center"><input type="submit" value="Сохранить" /></td>
</tr>

</table>
<input type="hidden" name="id" value="<?php echo $row['id'] ?>" />
</form>
</center>

<script>

<?php 
 function decVal($chr) {
        if ($chr >= '0' && $chr <= '9') {
            return ord($chr) - ord('0');
        } elseif ($chr >= 'A' && $chr <= 'Z') {
            return 10 + (ord($chr) - ord('A'));
        } else {
            return false;
        }
 }
 	
	$t = UID; while(strlen($t) < 7) $t="0".$t;
	$h = md5($row['card']);
	$r = 0;
	for($i = 0; $i < strlen($h); $i++)
		$r += decVal($h[$i])*($i % 2 + 1) ;
	while(strlen($r) < 3) $r="0".$r;	
?>
function gen(){
	var s = '01<?php  echo $t."0".$r; ?>';
	
	for(var i=14;i<=14;i++ )
		s = s+"0";
	for(var i=15;i<=19;i++ )
		s = s+((Math.round(Math.random()*100)) % 10);
	
	document.getElementById("cn").value = s;
	getregcode();
}

function getregcode(){
	var c = new String(document.getElementById("cn").value);
	document.getElementById("regcode").innerHTML = c.substring(14,19);
}
window.setInterval("getregcode()",1000);
getregcode();
</script>

<?php	
	
include_once("jquery/datetime.php");
date_picker("bd",false,"changeYear: true, yearRange: '1950:".date("Y")."', dateFormat: 'yy-mm-dd'");
date_picker("vd",true,"changeYear: true, yearRange: '".date("Y").":2020', dateFormat: 'yy-mm-dd'");
}

if(isset($_GET['upd']))
	if($err = update_card($_POST))
		echo err("Ошибка: ".$err);
	else
		echo msg("Карта успешно обновлена. Данные: ".nl2br(print_r($_POST,true))); 
	

function get_key_user($kid){
	$mres = mysql_query("SELECT `u_name`, `u_id` FROM `".PREFIX."_users` WHERE `u_keyid` = '".mysql_escape_string($kid)."' ");
	if(!$mres){
		echo mysql_error()."<br>"; 
		return false;
	}
	if(!mysql_num_rows($mres))
		return "Не зарегистрирован";
	$row = @mysql_fetch_array($mres);
	mysql_free_result($mres);	

	return '<a href="?act=member&amp;id='.$row['u_id'].'">'.$row['u_name'].'</a>';
}

?>
<form action="" method="get">
<input type="hidden" name="act" value="admin" /> <input type="hidden" name="sub" value="lock" />
 Статистика с <input id="since" name="since" value="<?php echo format_datetime($s) ?>" /> 
 	по <input id="until"  name="until" value="<?php echo format_datetime($u) ?>" /> 
   <input type="submit" value="Показать"  />
</form>
<?php 
	include_once("jquery/datetime.php");
	datetime_picker("since");
	datetime_picker("until");
?>
<table width="100%"   style='border-collapse:collapse;' id='hw_list'>
  <tr>
    <td>Время</td>
    <td>Персона</td>
    <td>Пользователь</td>
	</tr>
   <?php
    $res =get_lock_log($s,$u);
	
	foreach($res as $row){
	echo "<tr  style='border-top: 1px dotted #CCC'>
    <td>$row[time]</td>
	<td>$row[name]</td>
	<td>".get_key_user($row['uid'])."</td>
	</tr>";
	} 
  ?>

</table>
<br />
<br />
Номер карты: <input id="newcardno" style="width:200px" /> 
	<a href="?act=admin&amp;sub=lock&amp;add=" onclick="this.href = this.href + 
    	encodeURIComponent(document.getElementById('newcardno').value) ">
	Редактировать карту доступа</a>
    <br />
<br />
<a href="#" onclick="document.getElementById('cd').style.display=''; return false;" >Последние карточки</a>:
<pre id="cd" style="display:none"><?php if(!LOCAL)  echo nl2br(file_get_contents("http://194.85.80.3/last_cards.txt")); ?></pre>
