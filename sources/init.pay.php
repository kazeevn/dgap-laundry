<?php 
if(!isset($openproject_init)){die();}
//init requires - none
//func requires - user, mysql

define("RESERVE_STATUS_RESERVED", 1);
define("RESERVE_STATUS_CLOSED", 2);
define("RESERVE_STATUS_SUCCESS", 4);
define("RESERVE_STATUS_CANCELED", 8);



define("SYSTEM_PRINT_MIPT_RU",0);
define("SYSTEM_WASH",1);

define("ALLOW_NEGATIVE_MONEY", 3000);

$PAY_LAST_ERROR = 0;

function money_error($err = 0){
	global $PAY_LAST_ERROR;
	if($err == 0) $err = $PAY_LAST_ERROR;
	if($err == 0) return "Нет ошибки";
  
    switch ($PAY_LAST_ERROR) {
      case -1: return "Нет данных о пользователе.";
      case -2: return "Ошибка mysql.";
      case -3: return "Недостаточно средств на счету.";
      case -4: return "Резерв не найден.";
      case -5: return "Несоответствие автора резерва и стирки.";
	  	case -6: return "Резерв не найден.";
      default: return "Error # $PAY_LAST_ERROR";
    }
}

function reserve_money($summ, $sis, $time){

	$sis = $sis + 2000000;
	$reserve_time = ($time + 24*60)*60;

	global $PAY_LAST_ERROR;	
	if(!(defined("UID") && UID)){
		$PAY_LAST_ERROR = -1;
		return false;	
	}
  
  $mres = mysql_query("SELECT `u_money` - `u_reserved` as curr FROM `op_users` 
                        WHERE `u_id` = ".UID);
  if(!($mres && mysql_num_rows($mres))) {
    $PAY_LAST_ERROR = -2;
    return false;
  }
  $row = mysql_fetch_array($mres);
  
  if($row['curr'] - $summ < -ALLOW_NEGATIVE_MONEY) {
    $PAY_LAST_ERROR = -3;
    return false;
  }
  
  $mres = mysql_query("INSERT INTO `op_reserves` (`r_session`, `r_uid`, `r_summ`, `r_until`, `r_status`)
                  VALUES($sis, ".UID.", $summ, $reserve_time, 0)");
  if(!($mres && mysql_insert_id())) {
    $PAY_LAST_ERROR = -2;
    return false;
  }

  $mres = mysql_query("UPDATE `op_reserves`, `op_users` 
                       SET `u_reserved` = `u_reserved` + $summ,
                           `r_status` = `r_status` | ".RESERVE_STATUS_RESERVED."
                       WHERE `u_id` = ".UID." AND `r_id` = ".mysql_insert_id());
 
 if(!($mres && mysql_affected_rows())) {
    $PAY_LAST_ERROR = -2;
    return false;
  }
  
  return true;
}

function unreserve_money($sis){
  global $PAY_LAST_ERROR; 
  if(!(defined("UID") && UID)){
    $PAY_LAST_ERROR = -1;
    return false; 
  }
  return take_money(UID, $sis, false);
}

function take_money($uid, $sis, $take){
	//$sis = get_session_id($hw,$time);
	$sis = $sis + 2000000;
	
	global $PAY_LAST_ERROR;	
	
  $mres = mysql_query("SELECT `r_id`, `r_summ`, `r_uid` FROM `op_reserves` 
                        WHERE `r_session` = ".$sis."
                          AND NOT(`r_status` & ".RESERVE_STATUS_CLOSED.")");
  if(!($mres && mysql_num_rows($mres))) {
    $PAY_LAST_ERROR = -6;
    return false;
  }
  
  $row = mysql_fetch_array($mres); 
  if($row['r_uid'] != $uid){
    $PAY_LAST_ERROR = -5;
    return false;
  }

  if($take)
    $mres = mysql_query("UPDATE `op_reserves`, `op_users` 
                       SET `u_reserved` = `u_reserved` - $row[r_summ],
                           `u_money` = `u_money` - $row[r_summ],
                           `r_status` = `r_status` | ".(RESERVE_STATUS_CLOSED | RESERVE_STATUS_SUCCESS)."
                       WHERE `u_id` = ".$row['r_uid']." AND `r_id` = $row[r_id]");   
  else
    $mres = mysql_query("UPDATE `op_reserves`, `op_users` 
                       SET `u_reserved` = `u_reserved` - $row[r_summ],
                           `r_status` = `r_status` | ".(RESERVE_STATUS_CLOSED | RESERVE_STATUS_CANCELED)."
                       WHERE `u_id` = ".$row['r_uid']." AND `r_id` = $row[r_id]"); 

  if(!($mres && mysql_affected_rows())) {
    $PAY_LAST_ERROR = -2;
    return false;
  }
  
  return true;
  
  
}

?>
