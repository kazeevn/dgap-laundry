<?php 
if(!isset($openproject_init)){die();}
//requires - none

function dectohex($num, $digs = 6){
$r = dechex($num);
while(strlen($r)<$digs){$r = "0".$r;}
return $r;
}

function gethexip ($ip = '') {
if ($ip == ''){ $ip = (isset($_SERVER['HTTP_X_FORWARDED_FOR'])?$_SERVER['HTTP_X_FORWARDED_FOR']:$_SERVER['REMOTE_ADDR']); }
$ip = explode('.',$ip); 
return dectohex($ip[0],2).dectohex($ip[1],2).dectohex($ip[2],2).dectohex($ip[3],2);
}

function getintip ($ip = '') {
if ($ip == ''){ $ip = (isset($_SERVER['HTTP_X_FORWARDED_FOR'])?$_SERVER['HTTP_X_FORWARDED_FOR']:$_SERVER['REMOTE_ADDR']); }
$ip = explode('.',$ip); 
if(!isset($ip[3]))
	return 0;
return $ip[0]*256*256*256 + $ip[1]*256*256 + $ip[2]*256 + $ip[3];
}

function iptochar ($ip){
	$r = '';
	
	$r = ($ip % 256);
	$ip = floor($ip / 256);
	$r = ($ip % 256).".".$r;
	$ip = floor($ip / 256);
	$r = ($ip % 256).".".$r;
	$ip = floor($ip / 256);
	$r =  $ip .".".$r;

	return $r;
	
}

function getip(){
	return 	(isset($_SERVER['HTTP_X_FORWARDED_FOR'])?$_SERVER['HTTP_X_FORWARDED_FOR']:$_SERVER['REMOTE_ADDR']);
}

function getips ($ips) {
 while (strlen($ips)>7) { 
	 $ip = substr($ips,0,8);
	 $res[] = hexdec(substr($ip,0,2)).'.'.hexdec(substr($ip,2,2)).'.'.hexdec(substr($ip,4,2)).'.'.hexdec(substr($ip,6,2));
	 $ips = substr($ips,8);
 }
 if(!isset($res)) $res[] = NULL;
return $res;
}

function getipshex ($ips) {
 while (strlen($ips)>7) { 
 $res[] = substr($ips,0,8);
 $ips = substr($ips,8);
 }
return $res;
}

?>