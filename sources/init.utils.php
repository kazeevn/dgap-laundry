<?php
if(!isset($openproject_init)){die();}
//Requires - langs*

mb_internal_encoding("UTF-8");

date_default_timezone_set("Europe/Moscow");

	$http_path = dirname($_SERVER['PHP_SELF']);
	$http_path = explode("/", $http_path);
	$server_path = implode("/", $http_path);
	if($server_path[strlen($server_path)-1] != "/"){$server_path .= "/";}
	$server_path = "http://".(isset($_SERVER["HTTP_X_FORWARDED_HOST"])?$_SERVER["HTTP_X_FORWARDED_HOST"]:$_SERVER['HTTP_HOST']).$server_path;
define("SERVER_PATH",$server_path);

define("FULLQUERY","http://".(isset($_SERVER["HTTP_X_FORWARDED_HOST"])?$_SERVER["HTTP_X_FORWARDED_HOST"]:$_SERVER['HTTP_HOST']).$_SERVER['SCRIPT_NAME']."?".$_SERVER['QUERY_STRING']);

if(isset($_GET['do'])){
	if($_GET['do']=='session'){
	session_start();
	}
}

if(get_magic_quotes_gpc() == 1) {
	 striparray($_POST);
	 striparray($_GET);
	 striparray($_COOKIE);
}


function striparray(&$item) {
	foreach($item as $key=>$val)
		if(is_string($val)) $item[$key] = stripslashes($val);
		elseif(is_array($val)) striparray($val);
}

function redirect($url){
	if(headers_sent())
		echo "<script type='text/javascript'>document.location.href='{$url}'</script>\n";
	else
		header("Location: ".$url);
	exit();
}




function getimgcode($style, $width) {
return '
  <tr>
    <td style="'.$style.'">'.LAN_INDEX_15.':<br /><a href="#" onclick="document.getElementById(\'code\').src=\'?plain=1&amp;act=imgcode&amp;do=session&amp;code=\'+Math.random(); return false;">'.LAN_INDEX_16.'</a></td>
    <td ><img id="code" alt="'.LAN_INDEX_17.'" width="170px" height="60px" src="?plain=1&amp;act=imgcode&amp;do=session&amp;code='.rand(10000000,10000000000).'" /></td>
  </tr>
  <tr>
    <td style="'.$style.'">'.LAN_INDEX_18.':<br /><small>'.LAN_INDEX_19.'</small></td>
    <td><input name="code"  class="control" style="width:'.$width.';" /></td>
  </tr>
  ';
}


function protect($text){
$text = str_ireplace("<script","&lt;script",$text);
$text = str_ireplace("</script","&lt;/script",$text);

$offset = 0;

$text = strip_tags($text,"<tr><td><table><th><div><span><p><sub><sup><hr><a><img><h1><h2><h3><h4><h5><h6><ul><li><ol><br><strong><small><i><em><object><strike><u>");

while(($begin = strpos($text,'<',$offset))!==FALSE){
		$offset = $begin+2;
		if( ($end = strpos($text,'>',$begin))!==FALSE ){
			$elem = substr($text,$begin,$end-$begin+1);
			if(($abs = stripos($elem,"absolute"))!==FALSE){
				$text = substr_replace($text,"inherit",$begin+$abs,strlen("absolute"));
			}
		}

}

return $text;
}

function genpass($lenght = 11){
$res = '';
for($i = 0; $i <= $lenght; $i++){
switch (mt_rand (1,3)) {
 case 1:  { $res  .= chr(mt_rand(ord('0'),ord('9'))); break; }
 case 2:  { $res  .= chr(mt_rand(ord('a'),ord('z'))); break; }
 case 3:  { $res  .= chr(mt_rand(ord('A'),ord('Z'))); break; }
 }
}
return $res;
}


function getsize($intsize){
if($intsize<1024){$res = $intsize.' bytes';}
elseif($intsize<1024*1024){$res = round($intsize/1024,2).' K';}
else{$res = round($intsize/(1024*1024),2).' M';}
return $res;
}

function img($file, $alt, $rep = false){
	$pn  =  'images/'.$file;
	if(($file) && (file_exists($pn))){
		$photo_info = getimagesize($pn);
		echo "<img src='$pn' $photo_info[3] alt=\"$alt\" border=0 />";
		if($rep) echo " $alt";
	}
}


function costtostr($cost, $use_cops = true) {
	$rubs = floor($cost / 100);


	if(($rubs > 10)&&($rubs < 20)){
		$r = "рублей";
	}else{
		switch ($rubs % 10) {
			case 1:     $r = "рубль"; break;
			case 2:
			case 3:
			case 4: 	$r = "рубля"; break;
			default:	$r = "рублей"; break;
		}
	}

	if(!$use_cops) return ($cost/100)." ".$r;

	$cops = $cost % 100;

	if(($cops > 10)&&($cops < 20)){
		$c = "копеек";
	}else{
		switch ($cops % 10) {
			case 1:     $c = "копейка"; break;
			case 2:
			case 3:
			case 4: 	$c = "копейки"; break;
			default:	$c = "копеек"; break;
		}
	}
	if($cops < 10) $cops = "0".$cops;

	return $rubs." ".$r." ".$cops." ".$c;
}


function pointstostr($cost) {

	if(($cost > 10)&&($cost < 20)){
		$r = "очков";
	}else{
		switch ($cost % 10) {
			case 1:     $r = "очко"; break;
			case 2:
			case 3:
			case 4: 	$r = "очка"; break;
			default:	$r = "очков"; break;
		}
	}

	return $cost." ".$r;

}


function max_photo($ext = false){
 $res = (1024*1024);
 if ($ext) {
	 $res = getsize($res);
 }
 return $res;
}

function pict_resize($file,$to,$max_width,$max_height){
  $imagesize = getimagesize($file);
  if(!$imagesize) return false;


  $ratiow = ($max_width/$imagesize[0]);
  $ratioh = ($max_height/$imagesize[1]);

  $ratio = min($ratiow,$ratioh);

  $new_width = round($imagesize[0]*$ratio);
  $new_height = round($imagesize[1]*$ratio);
  $image_p = @imagecreatetruecolor($new_width,$new_height);
  switch ($imagesize['mime']) {
	  case "image/gif":
		  $image = @imagecreatefromgif($file);
	  break;
	  case "image/png":
		  $image = @imagecreatefrompng($file);
	  break;
	  case "image/bmp":
		  $image = @imagecreatefrombmp($file);
	  break;
	  default:
	  case "image/jpeg":
	  case "image/pjpeg":
		  $image = @imagecreatefromjpeg($file);
	break;
  }
  if(!$image) return false;
  imagecopyresampled($image_p,$image,0,0,0,0,$new_width,$new_height,$imagesize[0],$imagesize[1]);
  return imagejpeg($image_p,$to,100);
}

function err($text){
	return "<div class='err'>$text</div>";
}

function msg($text){
	return "<div class='msg'>$text</div>";
}

function timetostr($time){
	if($time == 0)
		return "0 мин";

	if($time < 0){
		$time = - $time;
		$a = "назад";
	} else
		$a = "";


	$m = $time % 60;
	$time = floor($time / 60);
	$h = $time % 24;
	$d = floor($time / 24);

	return trim(($d?"$d дн. ":"").($h?"$h ч. ":"").($m?"$m мин. ":"").$a," ");
}

function t(){
	return floor(time()/60);
}

function start_of_the_day($time){
	$ZERO_TIME =date("O",$time*60); //+0400
	$ZERO_TIME = -($ZERO_TIME/100)*60;
	$time -= $ZERO_TIME;
	$time = $time - ($time % (60*24));
	$time += $ZERO_TIME;
	return $time;
}

function format_date($time = 0){
	if($time == 0 )
		$time = t();

	return date("d.m.Y",$time * 60);
}

function format_time($time = 0){
	if($time == 0 )
		$time = t();

	return date("H:i",$time * 60);
}

function format_datetime($time = 0){
	if($time == 0 )
		$time = t();

	return date("d.m.Y H:i",$time * 60);
}

function format_intelly($time){
  if(start_of_the_day($time) == start_of_the_day(t()))
    return format_time($time);
  else
    return format_datetime($time);
}

function format_sup($time){
  return date("H<\s\u\p>i</\s\u\p>",$time * 60);
}

function format_short($start, $stop){
  if((start_of_the_day($start) == start_of_the_day(t()))||(start_of_the_day($stop) == start_of_the_day(t())))
    return format_sup($start)."- ".format_sup($stop);
  else
    return date("d.m",$start * 60)." ".format_sup($start);
}

function parse_select($a , $sel = 0){
	$t = "";
	foreach($a as $key=>$val)
		$t .= "<option value = '$key' ".(($key==$sel)?"selected=\"selected\"":"").">$val</option>";
	return $t;
}

function dtp($id){
	include_once("jquery/datetime.php");
	datetime_picker($id);
}

function dp($id){
	include_once("jquery/datetime.php");
	date_picker($id);
}

function send_sms($text, $uid = UID){
  $text = str_replace("№","#",$text);
	if(LOCAL){
		echo "SMS: $text to $uid <br/>";
		return array('ok' => 1);
	}
	$phone = getuser($uid,false,'u_phone');

/*
	$ans = file_get_contents("http://api.infosmska.ru/interfaces/sendmessage.ashx?login=Dimannn&pwd=WAAAGH&phones=7".$phone."&message=".urlencode($text)."&sender=Stiralka");
  $res = explode(":",$ans);
  if($res[0] == 'Error')
    $res['error'] = $res[1];
  elseif($res[0] == 'Ok')
    $res['ok'] = $res[1];
  else
    $res['error'] = "Unknown answer: ".$ans;
  if(isset($res['error']))*/{
  	global $prefs;
    unset($res);
    $res = file_get_contents("http://smsc.ru/sys/send.php?login=".$prefs['smsc']['login']."&psw=".$prefs['smsc']['password']."&charset=utf-8&translit=1&phones=+7".         $phone."&fmt=3&mes=".urlencode($text));
    $res = json_decode($res,true);
  }
  mysql_query("INSERT INTO `op_sms_log` (`sms_uid`,`sms_time`,`sms_text`, `sms_result`)
    VALUES ('".$uid."', NOW(), '".mysql_escape_string($text)."', '".mysql_escape_string(print_r($res,true))."');");

  return 	$res;
}

?>
