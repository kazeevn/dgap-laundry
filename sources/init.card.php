<?php 
if(!isset($openproject_init)){die();}
//Requires - none;

function _get_link(){
	global $prefs;
	$link = mysqli_connect(
		$prefs['rfid_dbase']['host'],
		$prefs['rfid_dbase']['username'],
		$prefs['rfid_dbase']['password'],
		$prefs['rfid_dbase']['database']);
	if(mysqli_connect_errno())
	        return false;
        mysqli_set_charset ( $link, "utf8" );

	return $link;
}

function get_info($surname, $firstname, $fields, $conditions = 0){
	if(!$link = _get_link())
		return false;
//        mysqli_set_charset ( $link, "utf8" );
	$mres = mysqli_query($link,"SELECT $fields, `name` AS SPEC_NAME_F FROM `users` 
					WHERE (`name` LIKE '".mysqli_escape_string( $link,$surname)." ".mysqli_escape_string($link,$firstname)."%')
					AND(`cardnumber` != '')".($conditions?"AND($conditions)":"")." ORDER BY `id` DESC LIMIT 0, 1");
	if(!$mres) 
            die(mysqli_error());
	
	$row = mysqli_fetch_array($mres);
	mysqli_free_result($mres);
	
	if(isset($row['SPEC_NAME_F'])){
		$un = explode(" ",$row['SPEC_NAME_F']);
		if(($un[0] != $surname)||($un[1] != $firstname))
                    return false;
	}
        
	if(!$row)
            return false; /*
		die("SELECT $fields, `name` AS SPEC_NAME_F FROM `users` 
					WHERE (`name` LIKE '".mysqli_escape_string( $link,$surname)." ".mysqli_escape_string($link,$firstname)."%')
					AND(`cardnumber` != '')".($conditions?"AND($conditions)":"")." ORDER BY `id` DESC LIMIT 0, 1");
                          */
	return $row;		
}


function update_info($id, $update){
	if(!$link = _get_link())
		return false;

	$r = mysqli_query($link,"UPDATE `users` SET $update WHERE (`id` = $id)AND(`cardnumber` != '')");
	
	if(!$r) echo mysqli_error($link);

	return $r;
		
}

function get_lock_log($since, $until){
	
	$since = date("Y-m-d H:i:s",$since*60);
	$until = date("Y-m-d H:i:s",$until*60);
	
	
	if(!$link = _get_link())
		return false;

	$r = mysqli_query($link,"SELECT `time`, `name`, `uid` FROM `vlog` 
					  		WHERE (`location` = 'washing')AND(`time` <= '$until')AND(`time` >= '$since')
					  		ORDER BY `time` DESC");
	
	if(!$r) return false;
	
	$res = array();
	while($row = mysqli_fetch_array($r))
		$res[] = $row;
	
	return $res;	
}

function get_card($card){
	if(!$link = _get_link())
		return false;
	
	$mres = mysqli_query($link,"SELECT `id`, `name`, `group`, `dorm`, `room`, `card`, `birthday`, `cardnumber`, `date`, `expire`
						 FROM `users` WHERE (`card` = '".mysqli_escape_string($link,$card)."') ORDER BY `id` DESC LIMIT 0, 1");
	if(!$mres) 
		return false;
	
	$row = mysqli_fetch_array($mres);
	mysqli_free_result($mres);	
	
	return $row;
}

function get_user_by_card($card_id){
	global $prefs;
	if (!$card = get_card($card_id)) return false;

	$mres = mysql_query("SELECT * FROM `".$prefs['dbase']['prefix']."_users` WHERE `u_keyid` = '".$card['id']."' ");
	if(!$mres){
		return false;
	}
	if(!mysql_num_rows($mres))
		return false;
	$row = @mysql_fetch_array($mres);
	mysql_free_result($mres);
	
	return $row;
}


function update_card($row){
	if(!$link = _get_link())
		return false;
		
	foreach($row as $key => $val)
		$row[$key] = mysqli_escape_string($link,$val);
		
	$mres = mysqli_query($link,"UPDATE `users` 
								SET `name` = '$row[name]', `group` = '$row[group]', `dorm` = '$row[dorm]', 
									`birthday` = '$row[birthday]', `cardnumber` = '$row[cardnumber]', 
									`expire` = '$row[expire]', `room` = '$row[room]' 
								WHERE (`id` = '$row[id]')AND(`card` = '$row[card]')");

	if(!$mres) 
		return mysqli_error($link);
	
	if(mysqli_affected_rows($link))
		return 0;
		
	$mres = mysqli_query($link,"INSERT INTO `users` 
						 (`id`, `name`, `group`, `dorm`, `room`, `card`, `birthday`, `cardnumber`, `expire`, `date`)
				  VALUES ('$row[id]', '$row[name]', '$row[group]', '$row[dorm]', '$row[room]', '$row[card]', 
								'$row[birthday]', '$row[cardnumber]', '$row[expire]', NOW())");
	if($mres) 
		return 0;	
	else
		return mysqli_error($link);
}


?>
