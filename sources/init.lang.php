<?php 
if(!isset($openproject_init)){die();}
//Requires - none;

function sendcookie($name, $val, $time = 0){
	$time = $time?(time()+$time):0;
	if(LOCAL){
		setcookie($name,$val,$time,'/');
	}else{
		setcookie($name,$val,$time,'/',$prefs['general']['cookie_dom']);
	}
}

define('DEFLANG',$prefs['general']['deflang']);
define('LANGCOOKIE',$prefs['general']['cookie'].'_lang');

if(isset($_GET['setlang'])){
	sendcookie(LANGCOOKIE,$_GET['setlang'],60*60*24*365*10);
	$lang = $_GET['setlang'];
}elseif(isset($_COOKIE[LANGCOOKIE])){
	$lang = $_COOKIE[LANGCOOKIE];
}else{
	$lang = DEFLANG;
}

load_langs();

if(!isset($langs[$lang])) $lang = DEFLANG;

define('LANG',addslashes($lang));
unset($lang);

include('sources/language/'.LANG.'/lang.php'); 

function load_langs(){
	global $langs;
	$dir = "sources/language/";
	if (is_dir($dir)) {
		if ($dh = opendir($dir)) {
			while (($file = readdir($dh)) !== false) {
			   if(is_dir("sources/language/".$file)&&($file!=".")&&($file!='..')){
					$l = fopen("sources/language/$file/lang.php","r");
					$beg = fread($l,1024);
					fclose($l);
					$beg = strstr($beg,"#!");
					$beg=substr($beg,2,strpos($beg,"!#")-2);
					$langs[$file] = $beg;		   
			   }
			}
			closedir($dh);
		}
	}	
}


function parse_big_lang(){
global $prefs;
if(isset($_COOKIE[LANGCOOKIE])) return ""; 
if(isset($_GET['setlang'])) return "";
	
global $langs;
$bl = 
'<div style="background-color:#FFFFFF; position:absolute; top:0px; left:0px; width:100%; height:100%; filter: alpha(opacity=50); -moz-opacity: .50; " id="div_back"></div>
<div style="background-color:#FFFFFF; position:absolute; top:10px; left:10px; width:440px; height:280px; border:solid 1px #46B3CA; " id="choose_lang" >
  <div id="welcome" class="post" >
        <h2 class="title" id="ttl" style="background: #FFFFFF url(images/img02.gif) repeat-x; ">OpenProject.org.ru</h2>
        <div class="story">
    		<table width="100%" border="0" cellspacing="10px">
            <tr>
            <td  colspan="2" align="center"><h3>Select language:</h3></td>
            </tr>
            <tr>
            <td  colspan="2" align="center" style="padding-bottom:10px; ">Openproject.org.ru is available on the following languages:</td>
            </tr>            
            <tr>
            <td width="50%" align="center"><a href="?'.($_SERVER['QUERY_STRING']?htmlentities($_SERVER['QUERY_STRING']).'&amp;':'').'setlang=en" ><img src="images/en_b.png" alt="'.$langs['en'].'" style="border:solid 1px #CCC" /></a></td>
            <td width="50%" align="center"><a href="?'.($_SERVER['QUERY_STRING']?htmlentities($_SERVER['QUERY_STRING']).'&amp;':'').'setlang=ru" ><img src="images/ru_b.png" alt="'.$langs['ru'].'" style="border:solid 1px #CCC" /></a></td>            
            </tr>
            <tr>
            <td width="50%" align="center" style="font-weight:bold; "><a href="?'.($_SERVER['QUERY_STRING']?htmlentities($_SERVER['QUERY_STRING']).'&amp;':'').'setlang=en" >'.$langs['en'].'</a></td>
            <td width="50%" align="center" style="font-weight:bold; "><a href="?'.($_SERVER['QUERY_STRING']?htmlentities($_SERVER['QUERY_STRING']).'&amp;':'').'setlang=ru" >'.$langs['ru'].'</a></td>            
            </tr>            
            </table>
        </div>
    </div>
</div>

<script type="text/javascript">
function msg_upd_pos() {
	var CW = (document.compatMode==\'CSS1Compat\' && !window.opera?document.documentElement.clientWidth:document.body.clientWidth);
	var BodyScrollLeft = (self.pageXOffset || (document.documentElement && document.documentElement.scrollLeft) || (document.body && document.body.scrollLeft));
	var ClientCenterX = parseInt(CW/2)+BodyScrollLeft;
	var CH = (document.compatMode==\'CSS1Compat\' && !window.opera?document.documentElement.clientHeight:document.body.clientHeight);
	var BodyScrollTop = (self.pageYOffset || (document.documentElement && document.documentElement.scrollTop) || (document.body && document.body.scrollTop));
	var ClientCenterY = parseInt(CH/2)+BodyScrollTop;
	document.getElementById("choose_lang").style.top  = ClientCenterY-150;
	document.getElementById("choose_lang").style.left = ClientCenterX-220;
	document.getElementById("choose_lang").style.display = "";
}
msg_upd_pos();
document.getElementById("div_back").style.height = (document.body.scrollHeight > document.body.offsetHeight)?document.body.scrollHeight:document.body.offsetHeight;
document.getElementById("div_back").style.width  =  (document.body.scrollWidth > document.body.offsetWidth)?document.body.scrollWidth:document.body.offsetWidth;

</script>';	

return $bl;
}

function parse_langs($big = true){
	global $langs;
	
	if($big) $res = "<p>Select language: </p>\n<p>"; 
	   else  $res = '<div style="position:absolute; top:2px; width:99%; left:0; text-align:right">';
	
	foreach($langs as $file => $beg)
	//	if($big || ($file != LANG))
			$res .= "<a href='?".($_SERVER['QUERY_STRING']?htmlentities($_SERVER['QUERY_STRING']).'&amp;':'')."setlang=$file' ><img src='images/$file".($big?'':'_s').".png' alt='$beg' style='border: 0; '/></a> ";		   
	
	if($big) $res .= "</p>";
		else $res .= "</div>";
	return $res;
}

function parse_langs_select(){
	global $langs;
	
	ob_start();
	
	?>
	<script type="text/javascript">
	function Select_lang(){
	document.location.href='index.php?<?php echo ($_SERVER['QUERY_STRING']?$_SERVER['QUERY_STRING'].'&amp;':''); ?>setlang='+document.getElementById("lang_select").value;
	}
	</script>
	<select name="lang" style="width:90% " id="lang_select" class="control" onchange="Select_lang()">
	<option value="0">Select language</option>
	<?php 
	foreach($langs as $file => $beg)
		echo "<option value='$file'>$beg</option>";		   
	
	echo '	</select>';
	
	$ls = ob_get_contents();
	ob_end_clean();
	return $ls;
}

function requirelang($file) {
    global $txt, $prefs;
  	$lang = "sources/language/".LANG."/$file.php";
	if(file_exists($lang)){include_once($lang);}
}


?>