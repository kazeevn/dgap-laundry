<?php
if(!defined("SUPINIT")){exit;}
if(!USER){exit;}
if(!ADMIN){exit;}
if(!defined("ADMIN_DORM")) {exit;}

global $hw_types; 
global $quants; 
global $reserve_events; 
global $hw_statuses; 

function hw_array($cond = "1"){
  $mres = mysql_query("SELECT `hw_id`, `hw_name` FROM `op_hardware` WHERE (".$cond.")AND(`hw_dorm` = ".ADMIN_DORM.")");	
	if(!$mres) 
		return false;
	$r = array();
	while($row = mysql_fetch_array($mres))
		$r[$row['hw_id']] = $row['hw_name'];
	mysql_free_result($mres);
	return $r;
}


if(isset($_GET['stop']) || isset($_GET['edit_stop'])){
	if($_GET['edit_stop']){
		$mres = mysql_query("SELECT * FROM `op_stops` WHERE `stop_id` = '".mysql_escape_string($_GET['edit_stop'])."'");
		echo mysql_error();
		$row = mysql_fetch_array($mres);
	} else
		$row = array();
?>
<center>
<form method="post" action="?act=admin&amp;sub=hardware&amp;add_sop"  >
<table width="500px" cellspacing="10px">
<tr>
	<td colspan="2" align="center"><h3>Приостановка оборудования</h3></td>
</tr>

<tr>
	<td width="40%" align="right">Оборудование:</td>
	<td width="60%" >
    	<select name="id" >
        	<?php echo parse_select(hw_array(), $row['stop_hid']?$row['stop_hid']:$_GET['stop']); ?>
        </select>    
    </td>
</tr>

<tr>
	<td align="right">Начало:</td>
    <td>
    	<input type="text" id="from" name="from" value="<?php echo format_datetime($row['stop_since']) ?>" />    	
    </td>
</tr>
<tr>
	<td align="right">Конец:</td>
    <td>
    	<input type="text" id="until" name="until" value="<?php echo format_datetime($row['stop_until']) ?>"  />
    </td>
</tr>

<tr>
	<td align="right">Пояснение:</td>
    <td><input type="text" name="info"   value="<?php echo $row['stop_comment'] ?>"/></td>
</tr>

<tr>
	<td colspan="2" align="center"><input type="submit" value="Сохранить"/></td>
</tr>
</table>
<?php if(isset($row['stop_id'])) echo '<input type="hidden" name="sid" value="'.($row['stop_id']).'" />'; ?>
</form>
</center>
<?php	
include("jquery/datetime.php");
datetime_picker("from");
datetime_picker("until");


}

if(isset($_GET['add']) || isset($_GET['edit'])){
	if($_GET['edit']){
    $mres = mysql_query("SELECT * FROM `op_hardware` WHERE (`hw_id` = '".$_GET['edit']."')AND(`hw_dorm` = ".ADMIN_DORM.")");
		$row = mysql_fetch_array($mres);
	} else
		$row = array();
	
?>
<center>
<form method="post" action="?act=admin&amp;sub=hardware&amp;upd"  >
<table width="500px" cellspacing="10px">
<tr>
	<td colspan="2" align="center"><h3>Редактирование оборудования</h3></td>
</tr>

<tr>
	<td width="40%" align="right">Тип оборудования:</td>
	<td width="60%">
    	<select name="type">
        	<?php echo parse_select($hw_types, $row['hw_type']); ?>
        </select>    
    </td>
</tr>

<tr>
	<td align="right">Название оборудования:</td>
	<td ><input type="text" name="name" value="<?php echo $row['hw_name'] ?>" /></td>
</tr>

<tr>
	<td width="40%" align="right">Квант времени:</td>
	<td width="60%">
    	<select name="quant">
        	<?php echo parse_select($quants, $row['hw_quant']); ?>
        </select>    
    </td>
</tr>

<tr>
	<td width="40%" align="right">Сдвиг времени:</td>
	<td width="60%">
		<input type="text" name="timeshift_h" style="text-align:right; width:40px"
        			value="<?php echo floor($row['hw_timeshift'] / 60) ?>" /> часов
        <input type="text" name="timeshift_m" style="text-align:right; width:40px" 
        			value="<?php echo ($row['hw_timeshift'] % 60) ?>" /> минут  
    </td>
</tr>

<tr>
	<td width="40%" align="right">Предзапись:</td>
	<td width="60%">
		<input type="text" name="presign_d" style="text-align:right; width:40px" 
        			value="<?php echo floor($row['hw_presign'] / (60*24)) ?>" /> дней
        <input type="text" name="presign_h" style="text-align:right; width:40px" 
        			value="<?php echo (floor($row['hw_presign']/60) % 24) ?>" /> часов
        <input type="text" name="presign_m" style="text-align:right; width:40px" 
        			value="<?php echo ($row['hw_presign'] % 60) ?>"  /> минут  
    </td>
</tr>

<tr>
	<td width="40%" align="right">Постзапись:</td>
	<td width="60%">
		<input type="text" name="postsign_h" style="text-align:right; width:40px"
        			value="<?php echo floor($row['hw_postsign'] / 60) ?>" /> часов
        <input type="text" name="postsign_m" style="text-align:right; width:40px"  
        			value="<?php echo ($row['hw_postsign'] % 60) ?>" /> минут  
    </td>
</tr>

<tr>
	<td width="40%" align="right">Резерв (для сушки):</td>
	<td width="60%">
	    &plusmn; <input type="text" name="reserve_time" style="text-align:right; width:40px"
        			 value="<?php echo ($row['hw_reserve']) ?>" /> минут
        от  <select name="reserve_event">
        	<?php echo parse_select($reserve_events, $row['hw_reserve_event']); ?>
       </select> 
    </td>
</tr>

<tr>
	<td width="40%" align="right">Стоимость записи:</td>
	<td width="60%">
		<input type="text" name="cost_r" style="text-align:right; width:40px"
        			value="<?php echo floor($row['hw_cost'] / 100) ?>" /> руб.
        <input type="text" name="cost_c" style="text-align:right; width:40px"  
        			value="<?php echo ($row['hw_cost'] % 100) ?>" /> коп.  
    </td>
</tr>

<tr>
	<td width="40%" align="right">Статус:</td>
	<td width="60%">
    	<select name="status">
        	<?php echo parse_select($hw_statuses, $row['hw_status']); ?>
        </select>    
    </td>
</tr>

<tr>
	<td colspan="2" align="center"><input type="submit" value="Сохранить" /></td>
</tr>

</table>
<?php if(isset($row['hw_id'])) echo '<input type="hidden" name="id" value="'.($row['hw_id']).'" />'; ?>
</form>
</center>
<?php }

if(isset($_GET['upd'])){
	$timeshift = $_POST['timeshift_h']*60 + $_POST['timeshift_m'];
	$presign   = ($_POST['presign_d']*24+$_POST['presign_h'])*60 + $_POST['presign_m'];
	$postsign  = $_POST['postsign_h']*60 + $_POST['postsign_m'];
	$cost  = $_POST['cost_r']*100 + $_POST['cost_c'];
	
	if(!isset($_POST['id']))
		if(mysql_query("INSERT INTO `op_hardware` (`hw_type` , `hw_name` , `hw_quant` , `hw_timeshift` , `hw_presign` ,
												   `hw_postsign` , `hw_reserve` , `hw_reserve_event` , `hw_cost`, `hw_status`, `hw_dorm` )
					   VALUES ('$_POST[type]', '".mysql_escape_string($_POST['name'])."', '$_POST[quant]',  '$timeshift',
					   '$presign', '$postsign', '$_POST[reserve_time]', '$_POST[reserve_event]', '$cost', '$_POST[status]', '".ADMIN_DORM."')"))
			echo msg("Оборудование добавлено.");
		else
			echo err(mysql_error());
	else
		if(mysql_query("UPDATE `op_hardware` SET `hw_type` = '$_POST[type]', `hw_name` = '".mysql_escape_string($_POST['name'])."',
								`hw_quant` = '$_POST[quant]', `hw_timeshift` = '$timeshift', `hw_presign` = '$presign',
								`hw_postsign` = '$postsign', `hw_reserve` = '$_POST[reserve_time]', 
								`hw_reserve_event` = '$_POST[reserve_event]', `hw_cost`  = '$cost',
								`hw_status` = '$_POST[status]' WHERE `hw_id` = '$_POST[id]'"))
			echo msg("Оборудование обновлено.");
		else
			echo err(mysql_error());		
	
}

if(isset($_GET['add_sop'])){
	$from = strtotime($_POST['from']);
	$until = strtotime($_POST['until']);
	
	if(($from === -1)||($until === -1))
		echo err("Неверный формат даты и времени.");
  else if ( $until <= $from) {
   echo err("Конец до начала!");
  } else {
		$from = floor($from / 60);
		$until = floor($until / 60);
		
		if(isset($_POST['sid'])){
			if(mysql_query("UPDATE `op_stops` SET `stop_hid` = '$_POST[id]', `stop_since` = '$from',
											 `stop_until` = '$until', `stop_comment` = '".mysql_escape_string($_POST['info'])."'
										WHERE `stop_id` = ".$_POST['sid']))
				echo msg("Приостановка изменена.");
			else
				echo err(mysql_error());
			
		}else{
			if(mysql_query("INSERT INTO `op_stops` (`stop_hid` , `stop_since` , `stop_until` , `stop_comment` )
										VALUES ('$_POST[id]', '$from', '$until', '".mysql_escape_string($_POST['info'])."');"))
				echo msg("Приостановка добавлена.");
			else
				echo err(mysql_error());
		}
	}

}

if(isset($_GET['rem_stop'])){

	if(mysql_query("DELETE FROM `op_stops` WHERE `stop_id` = ".$_GET['rem_stop']))
		echo msg("Приостановка удалена.");
	else
		echo err(mysql_error());		
	

}

?>
<a href="?act=admin&amp;sub=hardware&amp;add">Добавить</a>
<table width="100%"  style='border-collapse:collapse;' id='hw_list'>
<tr>
	<td>ID</td>
    <td>Тип</td>
    <td>Название</td>
    <td>Квант</td>
    <td>Сдвиг</td>
    <td>Предзапись</td>
    <td>Постзапись</td>
    <td>Резерв</td>
    <td>Цена</td>
    <td>Статус</td>
    
    <td>Опции</td>
</tr>

<?php
$stops = array();
$mres = mysql_query("SELECT * FROM `op_stops` WHERE `stop_until` > ".(t()-60*24));
while($row = mysql_fetch_array($mres)){
	$stop = array();
	$stop['id'] = $row['stop_id'];
	$stop['since'] = $row['stop_since'];
	$stop['until'] = $row['stop_until'];
	$stop['comment'] = $row['stop_comment'];
	$stops[$row['stop_hid']][] = $stop;
	
}
	


	$mres = mysql_query("SELECT * FROM `op_hardware` WHERE `hw_dorm` = ".ADMIN_DORM." ORDER BY `hw_type` ASC, `hw_name` ASC");
while($row = mysql_fetch_array($mres)){
	?>
    <tr  style='border-top: 1px dotted #CCC'>
        <td><?php echo $row['hw_id'] ?></td>
        <td><?php echo $hw_types[$row['hw_type']] ?></td>
        <td><?php echo $row['hw_name'] ?></td>
        <td><?php echo $quants[$row['hw_quant']] ?></td>
        <td><?php echo timetostr($row['hw_timeshift']) ?></td>
        <td><?php echo timetostr($row['hw_presign']) ?></td>
        <td><?php echo timetostr($row['hw_postsign']) ?></td>
        <td><?php echo timetostr($row['hw_reserve'])." от ".$reserve_events[$row['hw_reserve_event']] ?></td>
        <td><?php echo costtostr($row['hw_cost'], false) ?></td>
        <td><?php echo $hw_statuses[$row['hw_status']] ?></td> 
        <td>
            <a href="?act=admin&amp;sub=hardware&amp;edit=<?php echo $row['hw_id'] ?>">Edit</a>
            <a href="?act=admin&amp;sub=hardware&amp;stop=<?php echo $row['hw_id'] ?>">Stop</a>
            
        </td>
    
    </tr>
	<?php 
	if(isset($stops[$row['hw_id']])){
		foreach($stops[$row['hw_id']] as $stop){
			?>
                <tr>
                    <td></td>
                    <td align="right">Остановка </td>
                    <td colspan="2">c <?php echo format_datetime($stop['since']) ?></td>
                    <td colspan="2">по <?php echo format_datetime($stop['until']) ?></td>
                    <td colspan="4">Инфо: <?php echo $stop['comment'] ?></td>
                    <td>
                        <a href="?act=admin&amp;sub=hardware&amp;edit_stop=<?php echo $stop['id'] ?>">Edit</a>
                    </td>
               </tr>            		
			<?php
		}		
	}
} ?>

</table>

<?php 


?>
