<?php 
if(!defined("SUPINIT")){exit;}
if(!USER){exit;}
if(!POSTMAN){exit;}

if (isset($_POST["u_id"])){
	$mres = mysql_query("SELECT * FROM `".$prefs['dbase']['prefix']."_users` WHERE `u_id` = ".(int)($_POST["u_id"]));
	if (!$obj = mysql_fetch_object($mres)) die("Такого пользователя нет");
	require_once("mail.php");
	$text = 
"Добрый день, $obj->u_firstname!

Вам пришло бумажное письмо. Вы можете забрать его в холле 6-ки, в почтовом ящике.

Ваш почтальон, ".USERFULLNAME.".";
	send_mail($obj->u_email, "Вам пришло бумажное письмо", $text);
	die("Отправили письмо $obj->u_firstname $obj->u_surname");
}



?>
<table width="100%"  border="1" id="users_table">
  <thead>
  <tr>
    <th>Имя</th>	
    <th>Группа</th>	
    <th>Комната</th>	   
    <th>Действия</th> 
  </tr>
  <tr class="search">
    <td><input type="text" name="name"/></td>	
    <td><input type="text" name="group"/></td>	
    <td><input type="text" name="room"/></td>	   
  </tr>
  <tbody>

  <?php
    $mres = mysql_query("SELECT * FROM `".$prefs['dbase']['prefix']."_users` ORDER BY `u_surname` ASC");
	$n = 0;
	while($row = @mysql_fetch_array($mres)){
	$d = unserialize($row['u_adata']);
	echo "<tr data-u_id='$row[u_id]'>
    <td><span id='name_$row[u_id]'>$row[u_surname] $row[u_firstname]</span></td>
	<td><span id='group_$row[u_id]'>".htmlspecialchars($row['u_group'])."</span></td>
	<td><span id='room_$row[u_id]'>".htmlspecialchars($row['u_room'])."</span></td>
	<td class='alert'><a>Оповестить</a></td>
    </tr>";
	} 
  ?>
</table>

<?php include_once("jquery/jquery.php"); ?>
<div id="alert_dialog" title="Оповещение пользователя" align="center">
Оповестить пользователя <div class="name" style="font-weight:bold;"></div> о письме?<br><br>
<input type="button" onclick="post_alert(this);" value="Оповестить" /></table>
</div>
<script type="text/javascript">

var $search_tr = $("#users_table .search");

$search_tr.find("input").css({width:"97%"}).on('input', make_search);

function make_search(){
	var name_re  = new RegExp($search_tr.find("[name=name]").val());
	var group_re = new RegExp($search_tr.find("[name=group]").val());
	var room_re  = new RegExp($search_tr.find("[name=room]").val());
	$("#users_table tbody tr").each(function(){
		var $this = $(this);
		var $spans = $this.find("span");
		var matches = (name_re .test($spans.eq(0).html())) && 
					  group_re.test($spans.eq(1).html()) &&
					  room_re .test($spans.eq(2).html());
		if (!matches) $this.hide();
		else $this.show();
	});
}
var $alert_dialog = $("#alert_dialog");
$("#users_table tbody a").on('click', function(){
	$alert_dialog.find(".name")
		.html($(this).parent().siblings().eq(0).html());
	$alert_dialog.find("input")
		.data('u_id', $(this).parent().parent().data("u_id"));
	$alert_dialog.dialog('open');
	return false;
});

function post_alert(el){
	$.post('', {u_id: $(el).data("u_id")});
	$("#name_"+$(el).data("u_id")).parent().parent().css({"background-color":"hsl(105, 100%, 70%)"}).find("a").off('click');
	$alert_dialog.dialog('close');
	return false;
}


$(function(){
	$("#alert_dialog").dialog({ autoOpen: false, modal: true, resizable: false });
});
</script>

