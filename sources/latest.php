<?php
$washes = current_washes();

echo "<div id='current_div' style='position:absolute; margin-left: 200px; top: 73px; left: 0px; width:20px; height: 88px; overflow-x: auto; font-size:x-small; overflow-y: hidden;'>";
echo "<div id='latest_inner_div' style='height: 70px; width: 1500px'>";
  
  echo "<table  id='table_latest'>";
  echo "<tr height='15px'>";
  echo "<th>Записи:</th>";
  foreach($washes as $w)
    echo "<th colspan='3'>$w[hw_name]</th>";
  echo "</tr>";
  
  
  echo "<tr height='15px'><td>Последняя:</td>";
  foreach($washes as $w)
    if(!is_null($w['l_start'])){
      echo "<td align='right'>".format_short($w['l_start'],$w['l_stop']).": ";
      echo "<td align='left' >$w[l_name]</td>";
      echo "<td align='left' >$w[l_room]</td>";
    }else echo "<td colspan='3' align='center'><i>нет записей</i></td>";
  echo "</tr>";
  echo "<tr height='15px'><td>Текущая:</td>";
  foreach($washes as $w)
    if(!is_null($w['c_start'])){
      $now = (($w['c_start'] < t())&&($w['c_stop']>t()));
      echo "<td align='right' ".($now?"style='color:#000'":"").">".format_short($w['c_start'],$w['c_stop']).":</td>";
      echo "<td align='left'".($now?"style='color:#000'":"").">$w[c_name]</td>";
      echo "<td align='left' >$w[c_room]</td>";
    }else echo "<td colspan='3' align='center'><i>".($w['stoped']?"остановлена":"доступна запись")."</i></td>";
  echo "</tr>";
  echo "<tr height='15px'><td>Следующая:</td>";
  foreach($washes as $w)
    if(!is_null($w['n_start'])){
      echo "<td align='right'>".format_short($w['n_start'],$w['n_stop']).":</td>";
      echo "<td align='left'>$w[n_name]</td>";
      echo "<td align='left' >$w[n_room]</td>";
    }else echo "<td colspan='3' align='center'><i>следующей записи нет</i></td>";
  echo "</tr>";
  echo "</table>";
  

  echo "</div>";
  echo "</div>";

  include_once("jquery/jquery.php");
?>
<script>
function upd_size(){
  
  $("#current_div").css("width",($(window).width()-5-200)+"px");
  $("#latest_inner_div").css("width",($("#table_latest").width()+4)+"px");
  
}
$(document).ready(upd_size);
$(window).resize(upd_size);
upd_size();
</script>