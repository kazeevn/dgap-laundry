<?php
if(!defined("SUPINIT")){exit;}

function send_mail($to, $subject, $text){
global $prefs;
require_once('phpmailer/class.phpmailer.php');
$mail = new PHPMailer;
include('phpmailer/language/phpmailer.lang-en.php');
$mail->language = $PHPMAILER_LANG;
$mail->CharSet = "utf-8";
$mail->From = $prefs['mail']['from'];
$mail->FromName = $prefs['mail']['fromname'];
$mail->Subject = $subject;
$mail->Body = $text;
$mail->Mailer = $prefs['mail']['mailer'];
$mail->Sendmail = ($prefs['mail']['sendmail'])?($prefs['mail']['sendmail']):(ini_get('sendmail_path'));
$mail->Host = $prefs['mail']['smtp_server'] ;
$mail->Port = $prefs['mail']['smtp_port'] ;
if($prefs['mail']['ssl']=='1') 
	$mail->SMTPSecurity   = "ssl"; 
if(($prefs['mail']['smtp_username'])&&($prefs['mail']['smtp_password'])){
	$mail->SMTPAuth = TRUE;
	$mail->Username = $prefs['mail']['smtp_username'];
	$mail->Password = $prefs['mail']['smtp_password'];
	}
$mail->AddAddress($to);
$result = $mail->Send();
$mail->ClearAddresses();
if (!$result)
	echo $mail->ErrorInfo."<br />\n";
return $result;
}

?>