<?php 
if(!defined("SUPINIT")){exit;}
if(!USER){exit;}
if(!ADMIN){exit;}
if(!defined("ADMIN_DORM")) {
  $l = get_user_room_and_dorm();
  $dorm = $l['dorm'];
  if($dorm == 0)
    die(err("Не задан номер общежития."));
  
  define("ADMIN_DORM",$dorm);
}

if(isset($_GET['del'])){
	mysql_query("DELETE FROM `".$prefs['dbase']['prefix']."_users` WHERE `u_id` = '".addslashes($_GET['del'])."'");
	echo "Пользователь удалён.";
	}
	
if(isset($_GET['block'])){
	addflag(FLAG_BLOCKED,$_GET['block']);
	echo "Пользователь заблокирован.";
}


if(isset($_GET['unblock'])){
	remflag(FLAG_BLOCKED,$_GET['unblock']);
	echo "Пользователь разблокирован.";
}

if(isset($_GET['approve'])){
	addflag(FLAG_NAME_CONFIRMED,$_GET['approve']);
	echo "Пользователь подтвержден.";
}

if(isset($_GET['forbid'])){
	$data = loadadata($_GET['forbid']);
	$data['nc_error']=$_GET['r'];
	saveadata($data,$_GET['forbid']);
	echo "Пользователь не подтвержден.";
}

if(isset($_GET['val'])){
	$p = explode("|",$_GET['val']);
	//print_r($p);
	if(mysql_query("UPDATE ".PREFIX."_users SET `u_group` = '".mysql_escape_string($p[1])."', 
						`u_room` = '".mysql_escape_string($p[2])."' 
						WHERE (`u_id` = '".mysql_escape_string($p[0])."')AND(`u_class` = '')"))
		echo "Данные обновлены";
	else
		echo "Ошибка: ".mysql_error();
	return;
}

function gf($f,$row){
	return (($row['u_flags'] & $f) == $f);	
}

?>
<table width="100%"  border="1" style="text-align:center ">
  <tr>
  	<td>#</td>
    <td>ID</td>
    <td>Login</td>
    <td>Имя</td>	
    <td>E-mail</td>
    <td>Группа</td>	
    <td>Комната</td>	
    <td>Телефон</td>	
    
    <td>Действия</td> 
  </tr>
  <script language="javascript">
  function check($uid, $uname){
  if(confirm('Вы уверены, что хотите удалить этого пользователя: '+$uname)){
  document.location.href='<?php echo $_SERVER['SCRIPT_NAME']; ?>?act=admin&sub=users&del='+$uid;
  }
  }
  </script>
  <?php
    $mres = mysql_query("SELECT * FROM `".$prefs['dbase']['prefix']."_users` ORDER BY `u_surname` ASC");
	$n = 0;
	while($row = @mysql_fetch_array($mres)){
	$d = unserialize($row['u_adata']);
	echo "<tr>
	<td>".(++$n)."</td>
    <td>".$row['u_id']."</td>
	<td><a href='?act=member&amp;id=$row[u_id]' >".$row['u_name']."</a></td>
    <td><span id='name_$row[u_id]'>$row[u_surname] $row[u_firstname]</span>".(gf(FLAG_NAME_CONFIRMED,$row)?",+":
			(isset($d['nc_error'])?"":
			($row['u_document']?", <a href='images/users/$row[u_document].jpg' target='_blank'>Док</a>, 
				<a href='?act=admin&sub=users&approve=$row[u_id]' onclick='return confirm(\"Approve?\")'>Да</a>, 
				<a href='#' onclick='forbid($row[u_id]); return false;'>Нет</a>":"")))."</td>
	<td>$row[u_email]".(gf(FLAG_EMAIL_CONFIRMED,$row)?",+":"")."</td>
	<td><span id='group_$row[u_id]'>".htmlspecialchars($row['u_group'])."</span></td>
	<td><span id='room_$row[u_id]'>".htmlspecialchars($row['u_room'])."</span></td>
	<td>$row[u_phone]".(gf(FLAG_PHONE_CONFIRMED,$row)?",+":"")."</td>
	
	<td>".
	($row['u_id']==UID?"&nbsp;":(gf(FLAG_BLOCKED,$row)?"<a href='?act=admin&sub=users&unblock=$row[u_id]'>Разбл</a>":"<a href='?act=admin&sub=users&block=$row[u_id]'>Блок</a>").
	($row['u_class']==""?" <a href='#' onclick=\"edit_user('".$row['u_id']."' ); return false;\">Правка</a>":"").
	" <a href='".$_SERVER['SCRIPT_NAME']."?act=admin&sub=stuff&edit=".$row['u_id']."'>Права</a>")."</td>
    </tr>";
	} 
  ?>
</table>
<script type="text/javascript">
function forbid(id){
	var s = prompt("Введите причину отказа:","");
	if(s != null){
	s = s.replace(new RegExp(['&'],["g"]),'%26');
	document.location.href='?act=admin&sub=users&forbid='+id+'&r='+s;
	}
}
</script>

<?php include_once("jquery/jquery.php"); ?>
<div id="dialog" title="Редактирование пользователя" align="center">

	<table cellpadding="5px">
    
        <tr>
        	<td>ID: </td>
        	<td><span id="id"></span></td>
        </tr>
    
         <tr>
        	<td>Имя: </td>
        	<td><span id="name"></span></td>
        </tr>
    
    	<tr>
        	<td>Группа: </td>
        	<td><input id="group" /></td>
        </tr>
        
    	<tr>
        	<td>Комната: </td>
        	<td><input id="room" /></td>
        </tr>        
        
        <tr>
        	<td colspan="2" align="center"><input type="button" onclick="set();" value="Сохранить" /></td>
        </tr>
    </table>
</div>

<script type="text/javascript">
	function edit_user(id){
			document.getElementById("room").value = 
					document.getElementById("room_"+id).innerHTML;
			document.getElementById("group").value = 
					document.getElementById("group_"+id).innerHTML;
			document.getElementById("name").innerHTML = 
					document.getElementById("name_"+id).innerHTML;					
			document.getElementById("id").innerHTML = id;					
			$("#dialog").dialog("open");
	}
	
	function set(){
		checkval("admin_users","set",document.getElementById("id").innerHTML+"|"+document.getElementById("group").value+
							"|"+document.getElementById("room").value,'s',"alert");	
	}


	$("#dialog").dialog({ autoOpen: false, modal: true, resizable: false });
</script>