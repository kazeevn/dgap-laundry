<?php
if(!defined("SUPINIT")){exit;}

//wget -O - -q "http://wash.fopf.mipt.ru/index.php?act=cron&plain=1" > ~/logs/cron-`date +%F`.log

define("SIGNUP_STATUS_PAID",1);
define("SIGNUP_STATUS_PAID_OK",2);
define("SIGNUP_STATUS_PAID_FAILED_HW",4);
define("SIGNUP_STATUS_PRE_SMS_SENT",8);
define("SIGNUP_STATUS_POST_SMS_SENT",16);
define("SIGNUP_STATUS_CHECKED",32);


echo format_time()." --- \n";

// Проверяем стирку на отмену.
$mres = mysql_query("SELECT `s_id` , `s_hid` , `s_uid` , `s_start`, `s_stop`, `s_status`, `hw_name` 
                      FROM `op_signups` 
                      JOIN `op_hardware` ON (`hw_id` = `s_hid`)
                      WHERE (NOT(`s_status` & ".SIGNUP_STATUS_CHECKED."))AND(`s_start` < ".(t()+20).")");
if($mres && mysql_num_rows($mres))
while($row = mysql_fetch_array($mres)){
  
  $take = check_hardware($row['s_hid'],$row['s_start'], $row['s_stop']);
  
  $status = $row['s_status'] | SIGNUP_STATUS_CHECKED;
  if(!$take){
    $status = $status | SIGNUP_STATUS_PAID | SIGNUP_STATUS_PAID_FAILED_HW;
    $res = take_money($row['s_uid'],$row['s_id'],false);
    if(!$res)
      echo "  Ошибка при снятии денег: ".money_error()."\n";
    echo "  У $row[s_uid] не сняты деньги за $row[s_hid].$row[s_start] с результатом $res\n";
    $text =  "К сожалению, запись на \"".$row['hw_name']."\" в ".format_time($row['s_start'])." отменена. Приносим извинения за неудобства.";
    $res = send_sms($text, $row['s_uid']);
    if(isset($res['error']))
      echo "  Ошибка SMS $row[s_uid]: ".$res['error']."\n";
    else
      echo "  SMS cancel $row[s_uid], $row[s_hid].$row[s_start]\n"; 
  }  

  mysql_query("UPDATE `op_signups` SET `s_status` = ".$status." WHERE `s_id` = ".$row['s_id']);
    
}

// Снимаем деньги за стирки.
$mres = mysql_query("SELECT `s_id` , `s_hid` , `s_uid` , `s_start`, `s_stop`, `s_status` 
						FROM `op_signups` WHERE (NOT(`s_status` & ".SIGNUP_STATUS_PAID."))AND(`s_start` < ".(t()-60).")");
if($mres && mysql_num_rows($mres))
	while($row = mysql_fetch_array($mres)){
		$take = check_hardware($row['s_hid'],$row['s_start'], $row['s_stop']);
		$res = take_money($row['s_uid'],$row['s_id'],$take);
		$row['s_status'] = $row['s_status'] | SIGNUP_STATUS_PAID;
		if($res)
			$row['s_status'] = $row['s_status'] | ($take?SIGNUP_STATUS_PAID_OK:SIGNUP_STATUS_PAID_FAILED_HW);
		else 
			echo "	Ошибка при снятии денег: ".money_error()."\n";
		echo "	У $row[s_uid] ".($take?"":"не ")."сняты деньги за $row[s_hid].$row[s_start] с результатом $res\n";
		mysql_query("UPDATE `op_signups` SET `s_status` = ".$row['s_status']." WHERE `s_id` = ".$row['s_id']);
	}
	
// Отправляем предварительные смски
$mres = mysql_query("SELECT `s_id` , `s_hid` , `s_uid` , `s_start`, `s_status`, `hw_name` 
						FROM `op_signups` JOIN `op_hardware` ON (`hw_id` = `s_hid`)
						WHERE (`s_pre_sms`)AND(`s_start` - `s_pre_sms` < ".(t()+2).")AND(NOT(`s_status` & ".SIGNUP_STATUS_PRE_SMS_SENT."))
                  AND(NOT(`s_status` & ".SIGNUP_STATUS_PAID_FAILED_HW."))");
if($mres && mysql_num_rows($mres))
	while($row = mysql_fetch_array($mres)){
		$text =  format_time($row['s_start'])." запись на \"".$row['hw_name']."\"";
	  	$res = send_sms($text, $row['s_uid']);
		if(isset($res['error']))
			echo "	Ошибка SMS $row[s_uid]: ".$res['error']."\n";
		else
			echo "	SMS pre $row[s_uid], $row[s_hid].$row[s_start]\n";
		$row['s_status'] = $row['s_status'] | SIGNUP_STATUS_PRE_SMS_SENT;	
		mysql_query("UPDATE `op_signups` SET `s_status` = ".$row['s_status']." WHERE `s_id` = ".$row['s_id']);
	}
	
// Отправляем конечные смски
$mres = mysql_query("SELECT `s_id` , `s_hid` , `s_uid` , `s_start`, `s_stop`, `s_status` , `hw_name` 
						FROM `op_signups` JOIN `op_hardware` ON (`hw_id` = `s_hid`)
						WHERE (`s_post_sms`)AND(`s_stop` - `s_post_sms` < ".(t()+2).")AND(NOT(`s_status` & ".SIGNUP_STATUS_POST_SMS_SENT."))
                  AND(NOT(`s_status` & ".SIGNUP_STATUS_PAID_FAILED_HW."))");
if($mres && mysql_num_rows($mres))
	while($row = mysql_fetch_array($mres)){
		$text =  format_time($row['s_stop'])." пора забирать вещи из \"".$row['hw_name']."\"";
	  	$res = send_sms($text, $row['s_uid']);
		if(isset($res['error']))
			echo "	Ошибка SMS $row[s_uid]: ".$res['error']."\n";
		else
			echo "	SMS post $row[s_uid], $row[s_hid].$row[s_start]\n";	
		$row['s_status'] = $row['s_status'] | SIGNUP_STATUS_POST_SMS_SENT;	
		mysql_query("UPDATE `op_signups` SET `s_status` = ".$row['s_status']." WHERE `s_id` = ".$row['s_id']);
	}

?>