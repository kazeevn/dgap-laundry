<?php
define ("PAGE_TITLE", "Регистрация нового пользователя");
define ("LAN_REGISTER_1", "Заполнены не все поля.");
define ("LAN_REGISTER_2", "Пароли не совпадают");
define ("LAN_REGISTER_3", "Пользователь с этим именем уже существует.");
define ("LAN_REGISTER_4", "Пользователь с этой электронной почтой уже существует.");
define ("LAN_REGISTER_5", "Пароль слишком короток");
define ("LAN_REGISTER_6", "Пользователь зарегистрирован.");
define ("LAN_REGISTER_7", 'Registration on');
define ("LAN_REGISTER_11", "Регистрация");
define ("LAN_REGISTER_12", "Логин");
define ("LAN_REGISTER_13", "Пароль");
define ("LAN_REGISTER_14", "Ещё раз");
define ("LAN_REGISTER_15", "Электронная почта");
define ("LAN_REGISTER_16", "Регистрация");
define ("LAN_REGISTER_17", 'Текст, который Вы ввели, не соответствует изображению!');
define ("LAN_REGISTER_18", "Регистрируясь, Вы заявляете, что Вы согласны с нашим <br /><a href='?act=tos' target='_blank'>правилами пользования</a>.");
?>
