<?php
//#!Русский (Russian)!#
define("CHARSET","utf-8"); //iso-8859-1

define ("LAN_INDEX_1", "На главную");
define ("LAN_INDEX_2", "Поиск");
define ("LAN_INDEX_3", "Форум");
define ("LAN_INDEX_4", "О Нас");
define ("LAN_INDEX_5", "Правила");
define ("LAN_INDEX_6", "OpenProject");
define ("LAN_INDEX_7", "бесплатный каталог проектов");
define ("LAN_INDEX_8", "Профиль");
define ("LAN_INDEX_9", "Последнее");
define ("LAN_INDEX_10", "К сожалению, эта функция еще не поддерживается.");
define ("LAN_INDEX_11", "Разработка");
define ("LAN_INDEX_12", "Форма для связи.");
define ("LAN_INDEX_13", "Дизайн");
define ("LAN_INDEX_14", "Опубликуй свою работу за 5 минут!");
define ("LAN_INDEX_15", "Анти-спам изображение");
define ("LAN_INDEX_16", "Сменить (если не читаемо)");
define ("LAN_INDEX_17", "Изображение");
define ("LAN_INDEX_18", "Анти-спам текст");
define ("LAN_INDEX_19", "Введите текст на рисунке с учётом регистра");
?>