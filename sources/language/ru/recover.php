<?php
define ("PAGE_TITLE", "Восстановление пароля");
define ("LAN_RECOVER_1", "Восстановление пароля");
define ("LAN_RECOVER_2", "Пользователь с этим именем и электронной почтой не найден.");
define ("LAN_RECOVER_3", 'Password recovery.');
define ("LAN_RECOVER_4", "Письмо, содержащее дальнейшие шаги восстановления пароля отправлено вам на email.");
define ("LAN_RECOVER_5", 'Пароль слишком короток. <br>');
define ("LAN_RECOVER_6", 'Пароли не совпадают. <br>');
define ("LAN_RECOVER_7", 'Неправильный код активации. <br>');
define ("LAN_RECOVER_8", 'Пароль изменён.');
define ("LAN_RECOVER_9", 'Введите новый пароль');
define ("LAN_RECOVER_10", 'Срок действия ссылки окончен.');
define ("LAN_RECOVER_11", 'Новый пароль');
define ("LAN_RECOVER_12", 'Ещё раз');
define ("LAN_RECOVER_13", 'Восстановить');
define ("LAN_RECOVER_14", "Введите данные Вашего профиля");
define ("LAN_RECOVER_15", "Имя пользователя");
define ("LAN_RECOVER_16", "Электронная почта");
define ("LAN_RECOVER_17", "Восстановить");
define ("LAN_RECOVER_18", 'Текст, который Вы ввели, не соответствует изображению!');
?>
