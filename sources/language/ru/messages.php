<?php
define("PAGE_TITLE","Личные сообщения");
define("LAN_MESSAGES_1",'Сообщения:');
define("LAN_MESSAGES_2","Вы уверены?");
define("LAN_MESSAGES_3","Входящие сообщения");
define("LAN_MESSAGES_4","Написать сообщение");
define("LAN_MESSAGES_5","Время");
define("LAN_MESSAGES_6","От");
define("LAN_MESSAGES_7","Тема");
define("LAN_MESSAGES_8","Текст");
define("LAN_MESSAGES_9","Действия");
define("LAN_MESSAGES_10","Ответить");
define("LAN_MESSAGES_11","Удалить");
define("LAN_MESSAGES_12","Кому");
define("LAN_MESSAGES_13","Тема");
define("LAN_MESSAGES_14","Сообщение");
define("LAN_MESSAGES_15","Исходящие сообщения");
define("LAN_MESSAGES_16","Новое сообщение");
?>
