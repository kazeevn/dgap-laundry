<?php
define("LAN_PM_1","Неверный UID!");
define("LAN_PM_2","Пользователь не найден!");
define("LAN_PM_3","Нельзя отправлять пустое сообщение!");
define("LAN_PM_4",'Сообщение отправлено');
define("LAN_PM_5","Новое сообщение");
define("LAN_PM_6","Тема");
define("LAN_PM_7","Сообщение");
define("LAN_PM_8","Вы уверены?");
define("LAN_PM_9","Отправить");
define("LAN_PM_10","Отмена");
define("LAN_PM_11","Закрыть");
define("LAN_PM_12","Пользователь");
define("LAN_PM_13","не найден");
define("LAN_PM_14","Невозможно отправить пустое сообщение!");
define("LAN_PM_15","Ошибка при отправке сообщения!");
define("LAN_PM_16","Кому");
?>