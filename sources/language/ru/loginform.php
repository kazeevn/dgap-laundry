<?php
define ("LAN_LOGINFORM_1", "Неверная пара логин-пароль.");
define ("LAN_LOGINFORM_2", "Не введён логин или пароль.");
define ("LAN_LOGINFORM_3", 'Логин');
define ("LAN_LOGINFORM_4", 'Пароль');
define ("LAN_LOGINFORM_5", 'Вход');
define ("LAN_LOGINFORM_6", 'Регистрация');
define ("LAN_LOGINFORM_7", 'Восстановить пароль');
define ("LAN_LOGINFORM_8", 'Добро пожаловать');
define ("LAN_LOGINFORM_9", 'Мои проекты');
define ("LAN_LOGINFORM_11", 'Профиль');
define ("LAN_LOGINFORM_12", 'Выход');
define ("LAN_LOGINFORM_13", 'Вход пользователя');
define ("LAN_LOGINFORM_14", 'Администрирование');
define ("LAN_LOGINFORM_15", 'Модерирование');
define ("LAN_LOGINFORM_16", 'Мои объявления');
define ("LAN_LOGINFORM_17", 'Войти на');
define ("LAN_LOGINFORM_18", '1 Час');
define ("LAN_LOGINFORM_19", '1 День');
define ("LAN_LOGINFORM_20", '1 Неделя');
define ("LAN_LOGINFORM_21", '1 Месяц');
define ("LAN_LOGINFORM_22", 'Навсегда');
define ("LAN_LOGINFORM_23", 'Сессию');
define ("LAN_LOGINFORM_24", 'Создать проект');
define ("LAN_LOGINFORM_25", 'Сообщения');
define("LAN_LOGINFORM_26",'<p>Зарегистрируйтесь на нашем сайте и вы получите:</p>
<ul style="margin:0; padding:0; list-style-position:inside; ">
<li>Страницу для вашего проекта</li>
<li>Место для хранения файлов</li>
<li>Форум для каждого проекта</li>
<li>Новости и информациою</li>
<li style="font-weight:bold">Возможность опубликовать все ваши проекты и создать красивую и удобную страницу для каждого из них!</li>
</ul>
<p>Не верите? Перейдите по ссылке ниже и введите ваше имя и email в <strong>небольшом всплывающем окне регистрации</strong>!</p> ');

?>