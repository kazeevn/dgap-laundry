<?php
define('MAIL_RECOVER_PWD',"Dear $txt[1],
	Someone, may be you, requested to recover password for your \"".$prefs['general']['sitename']."\" account.
If it were you, please click on the link below or copy it to your browser. Otherwise, simply delete this message.

$txt[2]
	
Note: this link is valid for 12 hours.
	Sincerely yours, ".$prefs['general']['siteadmin']."");

define("MAIL_REGISTER","Dear $txt[1],
	Thank you  for registering an account on \"".$prefs['general']['sitename']."\". Now you have access to all features it provide.
	Your login: $txt[1]
	Your password: $txt[2]
	Please save this information or remember it. We hope you will enjoy our service!
	To confirm your email address follow the corresponding link in your profile page.
	
	Sincerely yours, ".$prefs['general']['siteadmin']."");

define("MAIL_CONFIRM_EMAIL","Dear $txt[1],
	Someone, may be you, requested e-mail adress confirmation on \"".$prefs['general']['sitename']."\". If it were you please click the link below or copy it to your browser adress bar. Otherwise, simply delete this message. 
	
	$txt[2]
	
Note: this link is valid for 12 hours.
	Sincerely yours, ".$prefs['general']['siteadmin']."");
?>

