<?php //<meta http-equiv="Content-Type" content="text/html; charset=utf8>
if(!defined("SUPINIT")){exit;}
if(!USER){exit;}
if(!ADMIN){exit;}

$l = get_user_room_and_dorm();
$dorm = $l['dorm'];
if($dorm == 0)
    die(err("Не задан номер общежития."));

define("ADMIN_DORM",$dorm);

define('CAPTION',"Администрирование стиралки общежития № ".ADMIN_DORM);?>
<table width="100%"  border="0">
  <tr>
    <td width="12.5%"><a href="<?php echo $_SERVER['SCRIPT_NAME']."?act=admin&sub=users" ?>">Пользователи</a></td>
    <td width="12.5%"><a href="<?php echo $_SERVER['SCRIPT_NAME']."?act=admin&sub=stuff" ?>">Персонал</a></td>
    <td width="12.5%"><a href="<?php echo $_SERVER['SCRIPT_NAME']."?act=admin&sub=hardware" ?>">Оборудование</a></td>
    <td width="12.5%"><a href="<?php echo $_SERVER['SCRIPT_NAME']."?act=admin&sub=price" ?>">Ценовые модификаторы</a></td>
    <td width="12.5%"><a href="<?php echo $_SERVER['SCRIPT_NAME']."?act=admin&sub=stats" ?>">Статистика</a></td> 
    <td width="12.5%"><a href="<?php echo $_SERVER['SCRIPT_NAME']."?act=admin&sub=transactions" ?>">Переводы</a></td> 
	<td width="12.5%"><a href="<?php echo $_SERVER['SCRIPT_NAME']."?act=admin&sub=lock" ?>">Замок</a></td>
	<td width="12.5%"><a href="<?php echo $_SERVER['SCRIPT_NAME']."?act=admin&sub=pages" ?>">Страницы</a></td>	
	<td width="12.5%"><a href="<?php echo $_SERVER['SCRIPT_NAME']."?act=admin&sub=cleanup" ?>">Чистка</a></td>
  </tr>
</table><br />
<?php 
	$file = 'admin_'.(isset($_GET['sub'])?$_GET['sub'].".php":"default.php");
	$script = $file;
	$lang = "language/".LANG."/".$file;
	if(file_exists("sources/".$lang)){require_once($lang);}
	if(file_exists("sources/".$script)){require_once($script);} 
?>
