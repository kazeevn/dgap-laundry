<?php
if(!isset($openproject_init)){die();}
//Requires - db, utils, ip, card

define("UINCOOKIE",$prefs['general']['cookie']);
define("PASCOOKIE",$prefs['general']['cookie'].'_pass');

if(isset($_GET['do'])){
	if($_GET['do']=='logout'){
		sendcookie(UINCOOKIE,'0',0);
		sendcookie(PASCOOKIE,'',0);
		if(!isset($_GET['from']))
			redirect($_SERVER['SCRIPT_NAME']);
		else
			redirect(SERVER_PATH.$_GET['from']);
	} elseif ($_GET['do']=='card-login'){
		if(cardLogin() === true){
			redirect(SERVER_PATH);
		} else {
			define("LOGINERROR", 3);
			$protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');
            header($protocol . ' 401 Authorization required');		
		}
	}
}

if(isset($_POST['login'])){
	$un = $_POST['username'];
	$ps = $_POST['password'];
	
	if($un && $ps){
		if(login($un,$ps, (isset($_POST['cookielength'])?$_POST['cookielength']:0))){
			if(isset($_POST['redirect']))
				redirect(SERVER_PATH.$_POST['redirect']);
			else
				redirect(FULLQUERY); 
			exit;
		}else
			define("LOGINERROR",1);
	}else
		define("LOGINERROR",2);
};

if( isset($_COOKIE[UINCOOKIE]) && isset($_COOKIE[PASCOOKIE])  )
	define("USER",auth($_COOKIE[UINCOOKIE],$_COOKIE[PASCOOKIE]));
else
	define("USER",FALSE);
	
if(!USER){define("UID",0); 	 define("ADMIN",0);  define("MODERATOR",0);  define("CASHIER",0); }

function login($username, $password, $cl = 0){
	global $prefs;
	$mres = mysql_query("SELECT `u_id` FROM `".PREFIX."_users` 
                        WHERE `u_name` = '".mysql_escape_string($username)."'
                          AND `u_password` = '".md5($password)."'");
	if(!$mres) return false;
	if(!mysql_num_rows($mres)) return false;
	
	sendcookie($prefs['general']['cookie'],$username,$cl?60*$cl:0);
	sendcookie($prefs['general']['cookie'].'_pass',md5($password),$cl?60*$cl:0);

	return true;
}

function cardLogin(){
	global $prefs;
	if (!$card = @$_POST['card']) return false;
	if (!$user = get_user_by_card($card)) return false;
	if (sha1($prefs['term']['salt'].$card) !== @$_POST['key']) return false;

	sendcookie($prefs['general']['cookie'],$user['u_name'],60*5);
	sendcookie($prefs['general']['cookie'].'_pass',$user['u_password'],60*5);

	return true;

}

function auth($un, $pass){
	global $prefs;
	$mres = mysql_query("SELECT `u_id`, `u_name`, `u_class`, `u_flags`, `u_ips`, `u_keyid`,
                              `u_money`, `u_reserved`, `u_surname`, `u_firstname`
                        FROM `".PREFIX."_users` 
                        WHERE `u_name` = '".mysql_escape_string($un)."'
                        AND `u_password` = '".mysql_escape_string($pass)."'");
	if(!$mres) return false;
	if(!mysql_num_rows($mres)) return false;
  $row = @mysql_fetch_array($mres);
	mysql_free_result($mres);	
	
	define("USERNAME",$row['u_name']);
  define("UID",$row['u_id']);
  define("PASSWORD",$pass);

  $ips = $row['u_ips'];
	if(!in_array($hip = gethexip(),getipshex($ips))){
		$ips = $ips.$hip;	
    mysql_query("UPDATE `".PREFIX."_users` SET `u_ips` = '$ips' WHERE `u_id` = ".UID);
  }
	define("UCLASSES",$row['u_class']);
	define("ADMIN",in_array('1',explode('.',UCLASSES)));
	define("MODERATOR",in_array('2',explode('.',UCLASSES)));
	define("CASHIER",in_array('3',explode('.',UCLASSES)));
	define("POSTMAN",in_array('4',explode('.',UCLASSES)));
	define("BALANCE", ($row['u_money']-$row['u_reserved']));
	define("FULLBALANCE",$row['u_money']);
	define("RESERVED",$row['u_reserved']);
	define("KEYID",$row['u_keyid']);
	define("USERFULLNAME", $row['u_firstname']." ".$row['u_surname']);
	global $UFLAGS;
	$UFLAGS = $row['u_flags'];	
	
	return 	true;
}

function getuser($uid, $ext = false, $fld = 'u_name', $nocache = false){
	global $prefs;
	global $cache;
	if(($fld == 'u_name')&&($uid == UID)) 
		return USERNAME;
	if(isset($cache[$uid][$fld]))
		$row = $cache[$uid];
	else{
		$mres = mysql_query("SELECT `".$fld."` FROM `".$prefs['dbase']['prefix']."_users` WHERE `u_id` = '".mysql_escape_string($uid)."' ");
		if(!$mres){
			echo mysql_error()."<br>"; 
			return false;
		}
		if(!mysql_num_rows($mres))
			return false;
		$row = @mysql_fetch_array($mres);
		mysql_free_result($mres);	
		if(!$nocache) $cache[$uid][$fld] = $row[$fld];
	}
	if(!$ext)
		return $row[$fld];
	return '<a href="?act=member&amp;id='.$uid.'">'.$row['u_name'].'</a>';
}

function getuid($uname){
global $prefs;
$mres = mysql_query("SELECT `u_id` FROM `".$prefs['dbase']['prefix']."_users` WHERE `u_name` = '".mysql_escape_string($uname)."' ");
if(!$mres){echo mysql_error()."<br>"; return false;}
if(!mysql_num_rows($mres)){return false;}
$row = @mysql_fetch_array($mres);
mysql_free_result($mres);
return $row['u_id'];
}


function saveadata ($adata, $uid = 0) {
if($uid == 0){$uid = UID;}
if($uid == 0){return; }
if((sizeof($adata) == 0)||($adata==false)||($adata=="")){$r = ''; }else{$r = serialize($adata); }
mysql_query('UPDATE `'.PREFIX.'_users` SET `u_adata` = \''.$r.'\' WHERE `u_id` = '.mysql_escape_string($uid));
}

function loadadata ($uid = 0) {
if($uid == 0){$uid = UID;}
if($uid == 0){return; }
$mres = mysql_query('SELECT `u_adata` FROM `'.PREFIX.'_users` WHERE `u_id` = '.mysql_escape_string($uid));
if(!$mres){return NULL;}
if(!mysql_num_rows($mres)){return NULL;}
$row = mysql_fetch_array($mres);
mysql_free_result($mres);
return unserialize( $row['u_adata'] );
}

function saveflags ($flags, $uid = 0) {
	if($uid == 0){$uid = UID;}
	if($uid == 0){return false; }
	$res = mysql_query('UPDATE `'.PREFIX.'_users` SET `u_flags` = \''.$flags.'\' WHERE `u_id` = '.mysql_escape_string($uid));
	if($res && ($uid == UID)){ 
		global $UFLAGS;
		$UFLAGS = $flags;
	}
	if($res && ($card_id = getuser($uid,false,u_keyid))){
			$conf = user_confirmed($uid);
			update_info($card_id, '`access_laundry` = '.($conf?"1":"0"));
	}
	return $res;
}

function loadflags ($uid = 0) {
	if($uid == 0){
		global $UFLAGS;
		return $UFLAGS;
	}
	$mres = mysql_query('SELECT `u_flags` FROM `'.PREFIX.'_users` WHERE `u_id` = '.mysql_escape_string($uid));
	if(!$mres){return 0;}
	if(!mysql_num_rows($mres)){return 0;}
	$row = mysql_fetch_array($mres);
	mysql_free_result($mres);
	return $row['u_flags'];
}

function addflag ($flag, $uid = 0) {
	saveflags(loadflags($uid) | $flag, $uid);
}

function remflag ($flag, $uid = 0) {
	$f = loadflags($uid);
	saveflags($f - ($f & $flag), $uid);
}

function getflag($flag, $uid = 0) {
	return ((loadflags($uid) & $flag)==$flag);
}

function user_confirmed($uid = 0) {
	$good = FLAG_EMAIL_CONFIRMED | FLAG_PHONE_CONFIRMED | FLAG_NAME_CONFIRMED;
	$bad = FLAG_BLOCKED;
	return ((loadflags($uid) & ($good | $bad)) == $good);
}

function get_user_room_and_dorm($uid = UID, $room = ""){
  if($uid)
    $ur = getuser($uid,false,'u_room');
  else
    $ur = $room;

  $place = explode("-",$ur);
  if(!is_numeric($place[0]))
    $place[0] = 0;
  if(!$place[1])
    $place[1] = 0;
  return array('dorm' => $place[0], 'room' => $place[1]);
}

define('FLAG_EMAIL_CONFIRMED',1);
define('FLAG_PHONE_CONFIRMED',2);
define('FLAG_NAME_CONFIRMED',4);
define('FLAG_BLOCKED',8);

if(getflag(FLAG_BLOCKED))
	define("OFFMESSAGE","Ваш аккуант заблокирован.<br /><a href='?do=logout'>Выход</a>");


?>
