<?php 
if(!defined("SUPINIT")){exit;}
define('CAPTION', LAN_RECOVER_1);

if(isset($_POST['recover'])){
if(!isset($_SESSION['img_text'])){return 'Internal error!';}
if($_SESSION['img_text'] != $_POST['code']){echo LAN_RECOVER_18;}else{
if(!dbExist("(`u_name` = '".addslashes($_POST['username'])."') AND (`u_email` = '".$_POST['email']."') ")){echo LAN_RECOVER_2;}else{
session_destroy();

$act_code = genpass();
$uid = getuid($_POST['username']);

$txt[1] = $_POST['username'];
$txt[2] = SERVER_PATH."?act=recover&user=".$uid."&code=".$act_code;

requirelang('mail');

require_once('mail.php');
send_mail($_POST['email'], LAN_RECOVER_3, MAIL_RECOVER_PWD);

echo LAN_RECOVER_4;

$data = loadadata($uid);
$data['rec_code'] = md5($act_code);
$data['rec_date'] = time();
saveadata($data,$uid);

//newpassform();
return;
}}}

if((isset($_GET['user']))&&(isset($_GET['code']))){
 if(!check_code($_GET['user'], $_GET['code'])) {return; }
 newpassform($_GET['user'], $_GET['code']); 
 return;
}

if(isset($_POST['setpass'])){
if(strlen($_POST['password'])<5){echo LAN_RECOVER_5; newpassform($_POST['uid'], $_POST['code']); return;}
if($_POST['password']!=$_POST['repass']){echo LAN_RECOVER_6; newpassform($_POST['uid'], $_POST['code']); return;}
if(!check_code($_POST['uid'], $_POST['code'])) {return; }

if(!mysql_query("UPDATE `".$prefs['dbase']['prefix']."_users` SET `u_password` = '".md5($_POST['password'])."' WHERE `u_id` = ".addslashes($_POST['uid']))){echo mysql_error(); newpassform($_POST['uid'], $_POST['code']); return;}

 $data = loadadata($_POST['uid']);
 unset($data['rec_code']);
 unset($data['rec_date']);
 saveadata($data, $_POST['uid']);

echo LAN_RECOVER_8;
return;
}

function check_code($uid, $code){
 if(!is_numeric($uid)){return false;}
 $data = loadadata($uid);
 if($data == NULL){ return false; }
 if(!isset($data['rec_code'])){ return false;}
 if(!isset($data['rec_date'])){ return false;}
 if(md5($code) != $data['rec_code']){ return false; }
 if((time()-$data['rec_date'])>(60*60*12)){echo LAN_RECOVER_10; return false; }
 return true;
}

function newpassform($uid, $code){
echo '<form method="post" name="newpass" action="?act=recover">
<center><table width="90%"  border="0" cellpadding="5px">
  <tr>
    <td colspan="2" style="text-align:center ">'.LAN_RECOVER_9.':</td>
  </tr>  
  <tr>
    <td width="40%" style="text-align:right ">'.LAN_RECOVER_11.':</td>
    <td width="60%"><input name="password" type="password"  class="control" style="width:200px;"></td>
  </tr>
  <tr>
    <td style="text-align:right ">'.LAN_RECOVER_12.':</td>
    <td><input name="repass"  type="password"  class="control" style="width:200px;"></td>
  </tr>  
  <tr>
    <td colspan="2"  style="text-align:center "><input name="setpass" type="submit" value="'.LAN_RECOVER_13.'" class="control"></td>
  </tr>
</table></center>
<input type="hidden" name="uid" value="'.$uid.'" />
<input type="hidden" name="code" value="'.$code.'" />
</form>';
}

?>
<form method="post" action="<?php echo $_SERVER['SCRIPT_NAME'].'?'.$_SERVER['QUERY_STRING'].'&amp;do=session' ?>" name="recoverform">
<table width="90%"  border="0" cellpadding="5px">
  <tr>
    <td colspan="2" style="text-align:center "><h3><?php echo LAN_RECOVER_14; ?>:</h3></td>
  </tr>  
  <tr>
    <td width="40%" style="text-align:right "><?php echo LAN_RECOVER_15; ?>:</td>
    <td width="60%"><input name="username" class="control" style="width:200px; " /></td>
  </tr>
  <tr>
    <td style="text-align:right "><?php echo LAN_RECOVER_16; ?>:</td>
    <td><input name="email"  class="control" style="width:200px;" /></td>
  </tr>
  <?php echo getimgcode('text-align:right', '200px') ?> 
  <tr>
    <td colspan="2"  style="text-align:center "><input name="recover" type="submit" value="<?php echo LAN_RECOVER_17; ?>" class="control" /></td>
  </tr>
</table>
</form>
