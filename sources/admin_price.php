<?php
if(!defined("SUPINIT")){exit;}
if(!USER){exit;}
if(!ADMIN){exit;}
if(!defined("ADMIN_DORM")) {exit;}

global $a_statuses; 

if(isset($_GET['add']) || isset($_GET['edit'])){
	if($_GET['edit']){
		$mres = mysql_query("SELECT * FROM `op_actions` WHERE 
          (`a_id` = '".mysql_escape_string($_GET['edit'])."')AND(`a_dorm` = ".ADMIN_DORM.")");
		$row = mysql_fetch_array($mres);
	} else
		$row = array();
	
?>
<center>
<form method="post" action="?act=admin&amp;sub=price&amp;upd"  >
<table width="700px" cellspacing="10px">
<tr>
	<td colspan="2" align="center"><h3>Редактирование ценового модификатора</h3></td>
</tr>

<tr>
	<td align="right">Комментарий:</td>
	<td ><input type="text" name="comment" style="width:500px" value="<?php echo $row['a_comment'] ?>" /></td>
</tr>

<tr>
	<td align="left" colspan="2">Код модификатора:</td>
</tr>

<tr>
	<td align="left" colspan="2" style="color:#000">
    	<span style="color:#039">function</span> 
        	(<?php echo htmlspecialchars(get_price_counter_arguments()) ?>) {
    </td>
</tr>
<tr>
	<td align="left" colspan="2" style="color:#000">
    	<textarea wrap="off" name="code" onkeypress="checkTab(event)"  style="width:650px; margin-left:40px;" rows="15"><?php echo $row['a_code'] ?></textarea>
    </td>
</tr>
<tr>
	<td align="left" colspan="2" style="color:#000">}</td>
</tr>
<tr>
	<td align="left" colspan="2"><sup>*</sup> Замечание: вся записываемая информация должна храниться в массиве $user_data</td>
</tr>


<tr>
	<td width="40%" align="right">Статус:</td>
	<td width="60%">
    	<select name="status">
        	<?php echo parse_select($a_statuses, $row['a_status']); ?>
        </select>    
    </td>
</tr>

<tr>
	<td colspan="2" align="center"><input type="submit" value="Сохранить" /></td>
</tr>

</table>
<?php if(isset($row['a_id'])) echo '<input type="hidden" name="id" value="'.($row['a_id']).'" />'; ?>
</form>
</center>

<script type="text/javascript">
// Set desired tab- defaults to four space softtab
var tab = "	";
       
function checkTab(evt) {
    var t = evt.target;
    var ss = t.selectionStart;
    var se = t.selectionEnd;
 
    // Tab key - insert tab expansion
    if (evt.keyCode == 9) {
        evt.preventDefault();
               
        // Special case of multi line selection
        if (ss != se && t.value.slice(ss,se).indexOf("n") != -1) {
            // In case selection was not of entire lines (e.g. selection begins in the middle of a line)
            // we ought to tab at the beginning as well as at the start of every following line.
            var pre = t.value.slice(0,ss);
            var sel = t.value.slice(ss,se).replace(/n/g,"n"+tab);
            var post = t.value.slice(se,t.value.length);
            t.value = pre.concat(tab).concat(sel).concat(post);
                   
            t.selectionStart = ss + tab.length;
            t.selectionEnd = se + tab.length;
        }
               
        // "Normal" case (no selection or selection on one line only)
        else {
            t.value = t.value.slice(0,ss).concat(tab).concat(t.value.slice(ss,t.value.length));
            if (ss == se) {
                t.selectionStart = t.selectionEnd = ss + tab.length;
            }
            else {
                t.selectionStart = ss + tab.length;
                t.selectionEnd = se + tab.length;
            }
        }
    }
           
    // Backspace key - delete preceding tab expansion, if exists
   else if (evt.keyCode==8 && t.value.slice(ss - 4,ss) == tab) {
        evt.preventDefault();
               
        t.value = t.value.slice(0,ss - 4).concat(t.value.slice(ss,t.value.length));
        t.selectionStart = t.selectionEnd = ss - tab.length;
    }
           
    // Delete key - delete following tab expansion, if exists
    else if (evt.keyCode==46 && t.value.slice(se,se + 4) == tab) {
        evt.preventDefault();
             
        t.value = t.value.slice(0,ss).concat(t.value.slice(ss + 4,t.value.length));
        t.selectionStart = t.selectionEnd = ss;
    }
    // Left/right arrow keys - move across the tab in one go
    else if (evt.keyCode == 37 && t.value.slice(ss - 4,ss) == tab) {
        evt.preventDefault();
        t.selectionStart = t.selectionEnd = ss - 4;
    }
    else if (evt.keyCode == 39 && t.value.slice(ss,ss + 4) == tab) {
        evt.preventDefault();
        t.selectionStart = t.selectionEnd = ss + 4;
    }
}
</script>

<?php }

if(isset($_GET['upd'])){
	
	if(create_price_counter($_POST['code'])){

		if(!isset($_POST['id']))
			if(mysql_query("INSERT INTO `op_actions` (`a_comment` , `a_code` , `a_status`, `a_dorm` )
						   VALUES ('".mysql_escape_string($_POST['comment'])."', 
								   '".mysql_escape_string($_POST['code'])."', 
								   '$_POST[status]', '".ADMIN_DORM."')"))
				echo msg("Модификатор добавлен.");
			else
				echo err(mysql_error());
		else
			if(mysql_query("UPDATE `op_actions` SET `a_comment` = '".mysql_escape_string($_POST['comment'])."',
													`a_code` = '".mysql_escape_string($_POST['code'])."',
													`a_status` = '$_POST[status]' WHERE `a_id` = '$_POST[id]'"))
				echo msg("Модификатор обновлен.");
			else
				echo err(mysql_error());		
	
	} else
		echo err("Ошибка при проверке функции.");
}

if(isset($_GET['del'])){
  if(mysql_query("DELETE FROM `op_actions` WHERE (`a_id` = '".mysql_escape_string($_GET['del'])."')AND(`a_dorm` = ".ADMIN_DORM.") "))
   echo msg("Удалено");
 else
   echo err(mysql_error());
}

?> 

<a href="?act=admin&amp;sub=price&amp;add">Добавить</a>
<table width="100%"  style='border-collapse:collapse;' id='hw_list'>
<tr>
	<td>ID</td>
    <td>Комментарий</td>
    <td>Статус</td>
    
    <td>Опции</td>
</tr>
<?php

$mres = mysql_query("SELECT `a_id`, `a_comment`, `a_status` FROM `op_actions` WHERE `a_dorm` = ".ADMIN_DORM);
while($row = mysql_fetch_array($mres)){
?>
    <tr  style='border-top: 1px dotted #CCC'>
        <td><?php echo $row['a_id'] ?></td>
        <td><?php echo $row['a_comment'] ?></td>
        <td><?php echo $a_statuses[$row['a_status']] ?></td> 
        <td>
            <a href="?act=admin&amp;sub=price&amp;edit=<?php echo $row['a_id'] ?>">Edit</a>
            <a href="?act=admin&amp;sub=price&amp;del=<?php echo $row['a_id'] ?>">Delete</a>
            
        </td>
    
    </tr>
<?php } ?>
</table>
