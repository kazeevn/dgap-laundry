<?php 
if(!defined("SUPINIT")){exit;}
if(!USER){exit;}
if(!ADMIN){exit;}
if(UID!=1203){exit;}
if(!defined("ADMIN_DORM")) {exit;}


$infos['general'] = 'Общие настройки';
$infos['cookie'] = 'Имя cookie*<br /><small>При изменении необходимо будет заново войти</small>';
$infos['cookie_dom'] = 'Домен сайта.*<br /><small>Во всех поддоменах будут действительны cookie</small>';
$infos['siteadminemail'] = 'E-mail администратора<br /><small>Для подписи в низу страницы</small>';
$infos['siteadmin'] = 'Имя администратора<br /><small>Для подписи в низу страницы</small>';
$infos['sitename'] = 'Название сайта<br /><small>Отображается в заголовке страниц</small>';
$infos['declaimer'] = 'Подпись<br /><small>Отображается в низу страницы</small>';
$infos['mail'] = 'Настройки почты';
$infos['mailer'] = 'Способ отправки (sendmail, mail, smtp)';
$infos['smtp_server'] = 'Адрес SMTP сервера';
$infos['smtp_port'] = 'Порт SMTP сервера';
$infos['smtp_username'] = 'Имя пользователя SMTP сервера';
$infos['smtp_password'] = 'Пароль SMTP сервера';
$infos['sendmail'] = 'Путь sendmail<br /><small>Оставьте пустым для настроек php.ini</small>';
$infos['from'] = 'Обратный адрес<br /><small>В письмах будет указан этот адрес</small>';
$infos['fromname'] = 'Автор письма<br /><small>Письма будут отправляться от этого имени</small>';
$infos['ssl'] = 'Использовать SLL? (1 или 0)';
$infos['bfExplorer_ut'] = 'Таблица пользователей bfExplorer';
$infos['smf_boards'] = 'Таблица форумов';
$infos['smf_moders'] = 'Таблица модераторов форума';
$infos['smf_members'] = 'Таблица пользователей форума';
$infos['smf_topics'] = 'Таблица тем форума';
$infos['smf_messages'] = 'Таблица сообщений форума';
$infos['addons'] = 'Интеграции';
$infos['dbase'] = 'Настройки MySQL';
$infos['host'] = 'Сервер MySQL*';
$infos['username'] = 'Имя пользователя*';
$infos['password'] = 'Пароль*';
$infos['database'] = 'Имя базы данных*';
$infos['prefix'] = 'Префикс названий таблиц*';
$infos['warning'] = 'Внимание! Неправильные значения в полях, отмеченных * приведут к полному отказу сайта.';
$infos['deflang'] = 'Язык по умолчанию*';


if(isset($_POST['edit'])){
	
	$f = fopen('prefs.php','w');
	fwrite($f,"<?php\n\$prefs[] = array();\n");
	fwrite($f,"//This is an automaticly generated config file.\n");
	foreach($_POST['prefs'] as $mainkey => $subvalue){
		fwrite($f,"//".getinfo($mainkey).".\n");
		foreach($subvalue as $key => $value){
		fwrite($f,"\$prefs['$mainkey']['$key'] = '$value';\n");
		}
		fwrite($f,"\n");
	}
	
	fwrite($f,"?>");
	fclose($f);
	redirect('?act=admin&sub=config');
};

function getinfo($txt){
	global $infos;
	if(isset($infos[$txt]))
			return $infos[$txt];
		else
			return $txt; 
}

?>
<form method="post">
<table width="100%"  cellpadding="5px" border="0" cellspacing="5px">
<?php
foreach($prefs as $key => $value){
  echo '<tr><td colspan="2" style="text-align:center ">'.getinfo($key).'</td></tr>';
  foreach($value as $key2 => $value2)
  	echo '
  <tr>
   <td align="right">'.getinfo($key2).' </td>
    <td width="75%"><input name="prefs['.$key.']['.$key2.']" class="control" type="'.((strpos($key2,'password') ===false)?'text':'password').'" style="width:90%;" value="'.htmlspecialchars($value2).'"></td>
  </tr>';
}
?>
  <tr>
    <td colspan="2" style="text-align:center "><?php echo getinfo("warning");  ?></td>
  </tr>
  <tr>
    <td colspan="2" style="text-align:center "><input type="submit" name="edit" class="control" value="Изменить"></td>
  </tr>
</table>
</form>
