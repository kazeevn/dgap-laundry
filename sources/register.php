<?php 
if(!defined("SUPINIT")){exit;}
define('CAPTION',LAN_REGISTER_11);
if(USER){echo err("You are allredy registered user."); return;}
//if(isset($_POST['username'])){echo '<div class="err">'.adduser()."</div>";}

function check_username($username){
	if($username == "")
		return array("status"=>"0", "img" => "cross.png","error"=>"Не введено имя пользователя.");
	
	if(dbExist("`u_name` = '".addslashes($username)."'"))
		return array("status"=>"0", "img" => "cross.png","error"=>"Данный пользователь уже зарегистрирован.");
	
	return array("status"=>"1", "img" => "tick.png");
}

function check_password($password){
	if($password == "")
		return array("status"=>"0", "img" => "cross.png","error"=>"Не введен пароль.");

	if(strlen($password) < 4)
			return array("status"=>"0", "img" => "cross.png","error"=>"Пароль слишком короткий");
			
	return array("status"=>"1", "img" => "tick.png");

}

function check_repass($password, $repass){
  if($repass == "")
    return array("status"=>"0", "img" => "cross.png","error"=>"Не введен пароль.");

  if($password != $repass)
      return array("status"=>"0", "img" => "cross.png","error"=>"Пароли не совпадают");
      
  return array("status"=>"1", "img" => "tick.png");

}

function check_name($name){
	if($name == "")
		return array("status"=>"0", "img" => "cross.png","error"=>"Не введено имя.");
	
	if(!preg_match("'^[а-яё]+$'ui",$name))
		return array("status"=>"0", "img" => "cross.png","error"=>"Неверно введено имя! Используйте только кирилицу.");
	else
		return array("status"=>"1", "img" => "tick.png");
} 

function check_surname($name){
	if($name == "")
		return array("status"=>"0", "img" => "cross.png","error"=>"Не введено имя.");
	
	if(!preg_match("'^[а-яё\-]+$'ui",$name))
		return array("status"=>"0", "img" => "cross.png","error"=>"Неверно введена фамилия! Используйте только кирилицу.");
	else
		return array("status"=>"1", "img" => "tick.png");
}

function check_phone($phone, $row){
	if((!$phone) && ($row['phone']))
		return array("status"=>"1", "img" => "tick.png", "value"=>$row['phone']);
	
	if($phone == "")
		return array("status"=>"0", "img" => "cross.png","error"=>"Не введен телефон.");
	if(! preg_match("'^9[0-9]{9}$'",$phone))
		return array("status"=>"0", "img" => "cross.png","error"=>"Неверно введен телефон.");
	else
		return array("status"=>"1", "img" => "tick.png");
}

function check_room($room, $row){
	if($row['room'])
		return array("status"=>"2", "img" => "lock.png","value"=>$row['dorm']."-".$row['room']);
	
	if($room == "")
		return array("status"=>"0", "img" => "cross.png","error"=>"Не введен номер комнаты.");
		
	if(!preg_match("'^[1-9]-[1-9][0-9]*[а-я]*$'ui",$room))
		return array("status"=>"0", "img" => "cross.png","error"=>"Неверно введен номер комнаты.");
	else
		return array("status"=>"1", "img" => "tick.png");
}

function check_cardno($surname, $name, $cardno, &$row){
	if(get_info($surname,$name,"`id`")){
		$row = get_info($surname,$name,"`cardnumber`, `group`, `room`, `dorm`, `phone`, `email`, `id`","SUBSTRING(`cardnumber`,15,5) = '".mysql_escape_string($cardno)."'");
		if($row && (substr($row['cardnumber'],14,5) == $cardno))
			if(dbExist("`u_keyid` = '".addslashes($row['id'])."'")){
				$row = false;
				return array("status"=>"0", "img" => "cross.png","error"=>"Данная карта уже используется зарегистрированным пользователем.");
			} else
				return array("status"=>"1", "img" => "tick.png");
		else {
			$row = false;
			return array("status"=>"0", "img" => "cross.png","error"=>"Неверно введен номер карты.");
		}
	}else
		return array("status"=>"0", "img" => "cross.png","error"=>"По данным имени и фамилии карта не найдена. <a target='_blank' href='?act=regcard'>Помощь</a>");
		
}

function check_group($group, $row){
	if($row['group'])
		return array("status"=>"2", "img" => "lock.png","value"=>$row['group']);
	
	if($group == "")
		return array("status"=>"0", "img" => "cross.png","error"=>"Не введена группа.");
	else
		return array("status"=>"1", "img" => "tick.png");
}

function check_email($email, $row){
	if((!$email) && ($row['email']))
		return array("status"=>"1", "img" => "tick.png", "value"=>$row['email']);
	
	if($email == "")
		return array("status"=>"0", "img" => "cross.png","error"=>"Не введен email.");
		
	if(!preg_match("/^([a-z0-9_\-]+\.)*[a-z0-9_\-]+@([a-z0-9][a-z0-9\-]*[a-z0-9]\.)+[a-z]{2,4}$/i",$email))
		return array("status"=>"0", "img" => "cross.png","error"=>"Неверно введен email");
	else
		return array("status"=>"1", "img" => "tick.png");
}


function get_array($text){
		$res = array();
		$itms = explode(chr(5),$text);
		foreach($itms as $it){
			$v = explode(chr(6),$it);
			if(isset($v[1]))
				$res[$v[0]]=$v[1];
		}
		return $res;
}

if(isset($_GET['sub'])){
	
	$i = get_array($_GET['val']);	
	$r = array();
	
	$r['username'] = check_username($i['username']);
	$r['password'] = check_password($i['password']);
	$r['repass'] = check_repass($i['password'],$i['repass']);
  $r['name'] = check_name($i['name']);
	$r['surname'] = check_surname($i['surname']);
	
	$row = false;
	$r['cardno'] = check_cardno($i['surname'],$i['name'],$i['cardno'], $row);
	
	
	$r['group'] = check_group($i['group'],$row);
	$r['room'] = check_room($i['room'],$row);
	$r['phone'] = check_phone($i['phone'],$row);
	$r['email'] = check_email($i['email'],$row);
		
		
	
	if($_GET['sub']=='check'){
		echo json_encode($r);
	}
	
	if($_GET['sub']=='register'){
		$allok = isset($row['id']);
		foreach($r as $key=>$v){
			if($v['status'] == 0)
				$allok = false;
			if(isset($v['value']))
				$i[$key] = $v['value'];
		}
		
		if($allok){
			echo reguser($i,$row['id']);			
		}else
			echo "Не все поля заполнены верно!";
	}	
	
	return;
}


function reguser($i,$keyid){

	global $prefs;
	if(mysql_query("INSERT INTO `".$prefs['dbase']['prefix']."_users` 
					(`u_name` , `u_password`, `u_email`, `u_reged`, `u_ips`, `u_phone`, 
					 `u_room`, `u_group`, `u_firstname`, `u_surname`, `u_keyid`, `u_flags`, `u_money`, `u_reserved`) 
			VALUES ('".mysql_escape_string($i['username'])."', MD5( '".mysql_escape_string($i['password'])."' ) ,
					'".mysql_escape_string($i['email'])."', NOW(), '".gethexip()."', '".mysql_escape_string($i['phone'])."', 
					'".mysql_escape_string($i['room'])."',	'".mysql_escape_string($i['group'])."', 
					'".mysql_escape_string($i['name'])."', '".mysql_escape_string($i['surname'])."', 
					'".mysql_escape_string($keyid)."', '".FLAG_NAME_CONFIRMED."', 3000, 0);")){
		
		$rm = explode('-',$i['room']);
		$dorm = is_numeric($rm[0])?$rm[0]:0;
		$room = is_numeric($rm[1])?$rm[1]:0;
		
		update_info($keyid,"`dorm` = '$dorm', `room` = '$room'");
		
		global $txt;
	
		$txt[1] = $i['username'];
		$txt[2] = $i['password'];
	
		requirelang('mail');
	
		require_once('mail.php');
		send_mail($i['email'], LAN_REGISTER_7.$prefs['general']['sitename'], MAIL_REGISTER);
		
		//redirect($_SERVER['SCRIPT_NAME'].'?act=message&text='.urlencode(LAN_REGISTER_6).(defined("POPUP")?"&popup=1":""));
		
		return "OK";
	}else{
		return "Ошибка при регистрации";
	}
	


}

?>
<form name="adduserform">
<span style="color:#900">Внимание!</span> Если Вы ещё не зарегистрировали социальную карту, её необходимо <a href="?act=regcard">зарегистрировать</a> <strong>ДО</strong> заполнения формы.<br />
<br />

<table width="95%">
<tr>
<td valign="top">
<table width="400px"  border="0" cellpadding="5px" cellspacing="5px">
  <tr><td colspan="2" align="left"><h3>Заполните информацию о себе:</h3></td></tr>
  
  <tr><td colspan="2" align="center"><strong>Данные учетной записи</strong></td></tr>  

  <tr>
    <td width="40%" style="text-align:right "><?php echo LAN_REGISTER_12; ?>:</td>
    <td width="60%"><input name="username" id="inp_username" class="control" style="width:200px; " onchange="check_input();" value="<?php echo (isset($_POST['username'])?$_POST['username']:''); ?>" />
  	<img src="images/cross.png" id="img_username" /></td>
  </tr>
  <tr>
    <td colspan="2" align="right" style="padding-right:30px; color:#900" id="err_username"></td>
  </tr> 
  
  
   
  <tr>
    <td style="text-align:right "><?php echo LAN_REGISTER_13; ?>:</td>
    <td><input name="password" type="password" id="inp_password" class="control" onchange="check_input();" style="width:200px;" />
    <img src="images/cross.png" id="img_password" /></td>
  </tr>
  <tr>
    <td colspan="2" align="right" style="padding-right:30px; color:#900" id="err_password"></td>
  </tr> 
  
  
  <tr>
    <td style="text-align:right "><?php echo LAN_REGISTER_14; ?>:</td>
    <td><input name="repass" type="password" id="inp_repass" class="control" onchange="check_input();" style="width:200px;" />
    <img src="images/cross.png" id="img_repass" /></td>
  </tr>
  <tr>
    <td colspan="2" align="right" style="padding-right:30px; color:#900" id="err_repass"></td>
  </tr> 
  
   
  <tr><td colspan="2" align="center"><strong>Данные социальной карты</strong></td></tr>  

  <tr>
    <td style="text-align:right ">Последние 5 цифр номера социальной карты:</td>
    <td><input name="cardno"  class="control" id="inp_cardno" maxlength="5" style="width:200px;" onchange="check_input();" value="<?php echo (isset($_POST['cardno'])?$_POST['cardno']:''); ?>"   />
    </td>
  </tr>     
  <tr><td colspan="2" align="right" style="padding-right:30px;" align="center"><a href="http://ru.wikipedia.org/wiki/%D0%A1%D0%BE%D1%86%D0%B8%D0%B0%D0%BB%D1%8C%D0%BD%D0%B0%D1%8F_%D0%BA%D0%B0%D1%80%D1%82%D0%B0#.D0.A1.D1.82.D1.80.D1.83.D0.BA.D1.82.D1.83.D1.80.D0.B0_.D0.BD.D0.BE.D0.BC.D0.B5.D1.80.D0.B0_.D1.81.D0.BE.D1.86.D0.B8.D0.B0.D0.BB.D1.8C.D0.BD.D0.BE.D0.B9_.D0.BA.D0.B0.D1.80.D1.82.D1.8B" target="_blank">Почему эти цифры вводить безопасно.</a></td></tr>  
   
   <tr>
    <td style="text-align:right ">Фамилия:</td>
    <td><input name="surname"  class="control" id="inp_surname" style="width:200px;" onchange="check_input();" value="<?php echo (isset($_POST['surname'])?$_POST['surname']:''); ?>"   />
    <img src="images/cross.png" id="img_surname" style="display:none" /></td>
  </tr>     
  <tr style="display:none">
    <td colspan="2" align="right" style="padding-right:30px; color:#900" id="err_surname"></td>
  </tr> 
  

  <tr>
    <td style="text-align:right ">Имя:</td>
    <td><input name="name"  class="control" id="inp_name" style="width:200px;" onchange="check_input();" value="<?php echo (isset($_POST['name'])?$_POST['name']:''); ?>"   />
    <img src="images/cross.png" id="img_name"  style="display:none"/><img src="images/cross.png" id="img_cardno" /></td>
  </tr> 
  <tr style="display:none">
    <td colspan="2" align="right" style="padding-right:30px; color:#900" id="err_name"></td>
  </tr> 
  
  <tr >
    <td colspan="2" align="right" style="padding-right:30px; color:#900" id="err_cardno"></td>
  </tr> 
  
  <tr><td>&nbsp;</td></tr>
   
  <tr>
    <td style="text-align:right ">Группа:</td>
    <td><input name="group"  class="control" id="inp_group" style="width:200px;" onchange="check_input();" value="<?php echo (isset($_POST['group'])?$_POST['group']:''); ?>"   />
    <img src="images/cross.png" id="img_group" /></td>
  </tr>     
  <tr>
    <td colspan="2" align="right" style="padding-right:30px; color:#900" id="err_group"></td>
  </tr>  
  
  
  

  <tr>
    <td style="text-align:right ">Комната, в которой Вы проживаете (пример: 6-523):</td>
    <td><input name="room"  class="control" id="inp_room" style="width:200px;" onchange="check_input();" value="<?php echo (isset($_POST['room'])?$_POST['room']:''); ?>"   />
    <img src="images/cross.png" id="img_room" /></td>
  </tr>
 <tr>
    <td colspan="2" align="right" style="padding-right:30px; color:#900" id="err_room"></td>
  </tr>  
  
  
  
  <tr>
    <td style="text-align:right "><?php echo LAN_REGISTER_15; ?>:</td>
    <td><input name="email"  class="control" id="inp_email" style="width:200px;" onchange="check_input();" value="<?php echo (isset($_POST['email'])?$_POST['email']:''); ?>"   />
    <img src="images/cross.png" id="img_email" /></td>
  </tr>
  <tr>
    <td colspan="2" align="right" style="padding-right:30px; color:#900" id="err_email"></td>
  </tr>
  
  
    
 <tr>
    <td style="text-align:right ">Мобильный телефон <br /> (пример 9201234567, без 8):</td>
    <td>+7 <input name="phone" maxlength="10" id="inp_phone"  class="control" onchange="check_input();" style="width:180px;" value="<?php echo (isset($_POST['phone'])?$_POST['phone']:''); ?>"   />
    <img src="images/cross.png" id="img_phone" /></td>
  </tr>
  <tr>
    <td colspan="2" align="right" style="padding-right:30px; color:#900" id="err_phone"></td>
  </tr>  
   <tr>
    <td colspan="2" align="center" >Ваш мобильный телефон и email будут использованы при активации учётной записи</td>
  </tr>   
  
  
  
  <?php /*echo getimgcode('text-align:right', '200px')*/ ?> 
  </table>
  </td><td valign="top">
  	<img src="images/card.jpg" style="width: 100%; max-width:400px; padding-top:140px" /><br />
<br />

  	<h3>Правила пользования стиральной комнатой:</h3>
    <br />
  	<iframe src="?act=terms&amp;popup=1" width="100%" height="150px" scrolling="auto" frameborder="0" 
    	style="border:1px solid #5AD; padding: 10px; padding-right:1px" ></iframe>
  	<div style="padding-left:20px; padding-top:20px">
    <p><input type="checkbox" name="rule1" id="r1" value="1" onchange="en()" <?php if(isset($_POST['rule1'])) echo 'checked="checked"'; ?> /> <label for="r1">Я согласен с правилами пользования стиральной комнатой.</label></p>
    <!---<p><input type="checkbox" name="rule2" id="r2" onchange="en()" <?php if(isset($_POST['rule2'])) echo 'checked="checked"'; ?> /> Я понимаю, что несу ответственность за свои действия в стиральной комнате, в том
    			числе материальную и дисциплинарную.</p>
    <p><input type="checkbox" name="rule3" id="r3" onchange="en()" <?php if(isset($_POST['rule3'])) echo 'checked="checked"'; ?> /> Я согласен, что мои контактные данные могут быть предоставлены другим пользователям в случае
    			необходимости связаться со мной.</p>--->
    </div>
  <div id="debug" style="display:none"></div>
  <script type="text/javascript">
  
  var all_clear = false;
  var last_s = '';
  
  function en(){
	  var c = all_clear;
	 if(! document.getElementById("r1").checked )
	 	c = false;
	/* if(! document.getElementById("r2").checked )
	 	c = false;
	 if(! document.getElementById("r3").checked )
	 	c = false;*/
		
	document.getElementById("b").disabled = !c;
	  
  }
  
  var Inputs=["username","password", "repass","name","surname","phone","room","email","group", "cardno"];
  

  function set_valid(res){
	  document.getElementById("debug").innerHTML = res;
	 eval("var r = "+res+";");
	 
	 
	 /*var s = "";
	 for(var t in r)
	 	s = s+t+"="+r[t]["status"]+";";
	 alert(s);*/
	 all_clear = true;
	 for(var t in r){
		document.getElementById("img_"+t).src = "images/"+r[t]["img"];
		
		if(r[t]["error"] == undefined)
			document.getElementById("err_"+t).innerHTML = "&nbsp;";
		else
			document.getElementById("err_"+t).innerHTML = r[t]["error"];
			
		if(r[t]["status"] == 2)
			document.getElementById("inp_"+t).readOnly = true;
		else
			document.getElementById("inp_"+t).readOnly = false;
			
		if(r[t]["value"] != undefined)
			document.getElementById("inp_"+t).value = r[t]["value"];

		if(r[t]["status"] == 0)
		 all_clear = false;	
	 }	   
	 
	 en();
  }
  function chr(AsciiNum) {
	return String.fromCharCode(AsciiNum)
	
  }
  function get_s(){
	  	var s = "";
		for(var i = 0; i < Inputs.length; i++) 
  			s = s+Inputs[i]+chr(6)+document.getElementById("inp_"+Inputs[i]).value+chr(5);	  
	  	return s;
  }
  
  function check_input(){
	  var s = get_s();
	  if(s != last_s)
	  	checkval("register","check",s,"s","set_valid",true);
  	  last_s = s;
  }
  
  function after_reg(text){
	  s = new String(text);
	  if(s.length < 6)
	    document.location.href = "?act=message&text=<?php echo urlencode(LAN_REGISTER_6); ?>";
	  else{
		en();
	  	alert(text);
	 }
		
  }
  
  function reg(){
		checkval("register","register",get_s(),"s","after_reg");  
  }
  
  window.setInterval("check_input();",1000);
  </script>
  
  </td></tr>
  <tr>
    <td colspan="2"  style="text-align:center "><input name="adduser" type="button" id="b"  
	<?php if(!(isset($_POST['rule1']))) echo 'disabled="disabled"'; /*&&isset($_POST['rule2'])&&isset($_POST['rule3'])*/?>
    	 value="<?php echo LAN_REGISTER_16; ?>" class="control" onclick="reg(); this.disabled = true;" /></td>
  </tr>
  <tr>
    <td colspan="2"  style="text-align:center ">Все регистрации проходят ручную проверку.</td>
  </tr>
</table>
</form>