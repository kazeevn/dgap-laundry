<?php
if(!defined("SUPINIT")){exit;}

function popup($id, $caption, $src, $width, $height){
?>
<div style="background-color:#FFFFFF; position:absolute; top:10px; left:10px; width:<?php echo $width+40; ?>px; border:solid 1px #46B3CA; display:none; " id="popup_<?php echo $id; ?>" >
  <div id="welcome" class="post" >
        <h2 class="title" id="ttl" style="background: #FFFFFF url(images/img02.gif) repeat-x; color:#586BAA; ">
        	<span style="float:left; "><?php echo $caption; ?></span>
            <span style="float:right; padding-right:10px;"><a onclick="javascript:close_<?php echo $id; ?>();" href="#">X</a></span>
        </h2>
        <div class="story">
            <iframe id="popup_<?php echo $id; ?>_frame" frameborder="0" width="<?php echo $width; ?>" height="<?php echo $height; ?>" scrolling="no" allowtransparency="allowtransparency" >
            </iframe>
        </div>
    </div>
</div>
<script type="text/javascript">
function show_<?php echo $id; ?>() {
	document.getElementById("popup_<?php echo $id; ?>_frame").src = "<?php echo addslashes($src); ?>";	
	var elem = document.getElementById("popup_<?php echo $id; ?>").style;
	var CW = (document.compatMode=='CSS1Compat' && !window.opera?document.documentElement.clientWidth:document.body.clientWidth);
	var BodyScrollLeft = (self.pageXOffset || (document.documentElement && document.documentElement.scrollLeft) || (document.body && document.body.scrollLeft));
	var CH = (document.compatMode=='CSS1Compat' && !window.opera?document.documentElement.clientHeight:document.body.clientHeight);
	var BodyScrollTop = (self.pageYOffset || (document.documentElement && document.documentElement.scrollTop) || (document.body && document.body.scrollTop));
	elem.top  = parseInt(CH/2)+BodyScrollTop -<?php echo round(($height+20)/2); ?> + "px";
	elem.left = parseInt(CW/2)+BodyScrollLeft-<?php echo round(($width+80)/2); ?> + "px";
	elem.display = "";
	return false;
}
function close_<?php echo $id; ?>() {
	document.getElementById("popup_<?php echo $id; ?>").style.display = "none";
	return false;
}	
</script>
<?php
}

?>