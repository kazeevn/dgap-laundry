<?php
define('CAPTION',"Пополнение баланса");
if(!defined("SUPINIT")){exit;}
if(!USER){ echo err("Для пополнения счёта выполните вход."); return;}

if(isset($_POST['summ'])) {
  $phone = $_POST['phone'];
	if(!is_numeric($phone))
		echo err("Неверно введен номер телефона");
  elseif(!(is_numeric($_POST['summ']) && ($_POST['summ'] > 0)))
    echo err("Неверно введена сумма для перевода");
  else{
    $money = $_POST['summ'] * 100;
    $mres = mysql_query("INSERT INTO  `op_transactions` (`t_uid` ,`t_summ` ,`t_status` ,`t_comment`)
                  VALUES (".UID.", $money, 0,  'Пополнение с qiwi, телефон ".mysql_escape_string($phone)."');");
    if(!($mres && mysql_insert_id()))
      echo err("Ошибка базы данных");
    else {
      $sis = mysql_insert_id();
			include("qiwi/test.php");
			$rc = createBill($phone, $money/100, $sis, 'Пополнение счёта пользователя "'.USERNAME.'"');
			if($rc == 0)
				echo msg("Вам выставлен счёт, оплатите его на сайте <a href='https://w.qiwi.ru/'>QIWI-кошелек</a>");
			else {
				mysql_query("UPDATE `op_transactions` SET `t_status` = ".(1000+$rc)."  WHERE `t_id` = ".$sis);
				echo err("При выставлении счёта произошла ошибка.");
			}
    }
  }
  
}
include('../prefs.php');
if (!@$prefs['qiwi']['disabled']):
?>
<form method="post">
<table cellspacing="10px">
<tr><td colspan="3"><h3>Поплнение счёта стиралки через QIWI Кошелек:</h3></td></tr>
<tr>
	<td rowspan="2"><img src="images/qiwi.jpg" /></td>
  <td>Телефон:</td>  
  <td colspan="2">+7 <input type="text" name="phone" value="<?php echo getuser(UID,false,'u_phone');	?>" />
</tr>
<tr>
  <td>Сумма:</td>
  <td><input type="text" value="50" name="summ"  style="width: 50px; text-align: right" /> руб.
  <td><input type="submit" value="Пополнить"/></td>
</tr>

</table>
</form>
  	<p>С помощью <a href="https://w.qiwi.ru/" target="_blank">QIWI Кошелька</a> вы можете пополнить счёт <strong>моментально</strong> и <strong>без комиссии</strong>!</p>
<p>Для этого:<br />
<strong>1</strong>. Сформируйте заказ;<br />
<strong>2</strong>. Оплатите автоматически созданный счет на оплату: наличными в терминалах QIWI, на сайте <a href="https://w.qiwi.ru/" target="_blank">QIWI Кошелька</a> или с помощью приложений для социальных сетей и <a href="https://w.qiwi.ru/mobile.action" target="_blank">мобильных телефонов и планшетов</a>.</p>

<p>QIWI Кошелек легко <a href="http://w.qiwi.ru/fill.action" target="_blank">пополнить</a> в терминалах QIWI и партнеров, салонах сотовой связи, супермаркетах, банкоматах или интернет-банк.</p>

<p>Оплатить счет на оплату можно не только со счета QIWI Кошелька, но и банковской картой.</p>

<p>Если у вас еще нет QIWI Кошелька – вы можете зарегистрировать его на <a href="http://w.qiwi.ru/" target="_blank">сайте</a> QIWI Кошелька или в любом из приложений за несколько минут.</p>

<p><a href="http://ishopnew.qiwi.ru/files/qiwi_instruction.html">Инструкции</a></p>
<?php
else:
	echo 'К сожалению, на стороне Qiwi что-то сейчас не работает. Попробуйте зайти через день.';
endif;
