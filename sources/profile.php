<?php 
if(!defined("SUPINIT")){exit;}
define('CAPTION', LAN_PROFILE_8);
if(!USER){echo "Please login to access this page."; return;}

$mres = mysql_query('SELECT `u_name`, `u_reged`, `u_email`, `u_firstname`, `u_surname`, `u_room`, `u_group`, `u_document` FROM `'.PREFIX.'_users` WHERE `u_id` = '.UID);
global $user_row;
$user_row = mysql_fetch_array($mres);


if(isset($_POST['changepass'])&&(UID!=-1)){echo changepass($_POST['oldpass'],$_POST['newpass'],$_POST['repass']);}
if(isset($_POST['changeemail'])&&(UID!=-1)){echo changeemail($_POST['email']);}
if(isset($_POST['changephone'])&&(UID!=-1)){echo changephone($_POST['phone']);}
if(isset($_POST['confirmphone'])&&(UID!=-1)){echo confirmphone($_POST['phone_code']);}
/*if(isset($_POST['changeinfo'])&&(UID!=-1)){echo changeinfo($_POST['changeinfo']);}*/
/*if(isset($_POST['cardno'])&&(UID!=-1)){echo check_no($_POST['cardno']);}*/
/*if(isset($_FILES['studak'])) echo upload_photo($_FILES['studak']);

function upload_photo($photo){
	if(getflag(FLAG_NAME_CONFIRMED)){
		return err("Ваша личность уже подтверждена.");	
	}
	if($photo['size']==0) return err('Выберите изображение!');
	if($photo['size']>max_photo()) return err('Изображение слишком большое!');
	$p_ext = '.'.substr($photo['name'],strrpos($photo['name'],'.')+1);
	$pf = 'images/users/';	
	$pnu = $pf.'upl_'.UID.$p_ext;
	$nn = UID."_".genpass();
	$pnn = $pf.$nn.".jpg";
	move_uploaded_file($photo['tmp_name'],$pnu);
	if(!pict_resize($pnu,$pnn,800,800)){
		unlink($pnu);
		return err('Загруженный файл не является изображением!');
	}
	unlink($pnu);
	
	$old_photo = getuser(UID,false,'u_document',true);
	
	if(mysql_query("UPDATE `".PREFIX."_users` SET `u_document` = '$nn' WHERE `u_id` = ".UID))
		{
			if(file_exists($pf.$old_photo.".jpg")) unlink($pf.$old_photo.".jpg");
			$data = loadadata();
			unset($data['nc_error']);
			saveadata($data);
			return msg('Изображение загруженено. Оператор проверит данные в ближашее время.');
		}else{
			unlink($pnn);
			return err(mysql_error());
		}
}*/

if(isset($_GET['sub'])){
	if($_GET['sub']=='econf'){
		$data = loadadata();
		if(getflag(FLAG_EMAIL_CONFIRMED)){
			echo LAN_PROFILE_49;
			return;
		}
		if(isset($data['ec_time']))
			if(($tm = (time()-$data['ec_time']))<(60*60*12)){
					printf(LAN_PROFILE_50,13-round($tm/3600));
					return;
			}
		
		$code = genpass();
		$txt[2] = SERVER_PATH."?act=profile&confirm=".$code;
		$txt[1] = USERNAME;
		requirelang('mail');
		
		require_once('mail.php');
		if(!send_mail(getuser(UID,false,'u_email'), LAN_PROFILE_51, MAIL_CONFIRM_EMAIL))
			return;
	
		$data['ec_time']=time();
		$data['ec_code']=$code;
		saveadata($data);
		echo LAN_PROFILE_52;
		return;
	}
}
if(isset($_GET['confirm'])){
	$data = loadadata();	
	if(isset($data['ec_time']))
		if( (time()-$data['ec_time']) < (60*60*12) )
			if(isset($data['ec_code']))
				if($data['ec_code']==$_GET['confirm']){
					unset($data['ec_time']);
					unset($data['ec_code']);
					saveadata($data);
					addflag(FLAG_EMAIL_CONFIRMED);
					update_info(KEYID,"`email` = '".mysql_escape_string(getuser(UID,false,'u_email'))."'");
					echo msg(LAN_PROFILE_53);										
				}else
					echo err(LAN_PROFILE_54);
			else
				echo err(LAN_PROFILE_55);
		else{
			echo err(LAN_PROFILE_56);
			unset($data['ec_time']);
			unset($data['ec_code']);
			saveadata($data);
		}
}


function changeemail($em){
	if (!preg_match("/^([a-z0-9_\-]+\.)*[a-z0-9_\-]+@([a-z0-9][a-z0-9\-]*[a-z0-9]\.)+[a-z]{2,4}$/i",$em)) 
		return err("Invalid email format.");	
	
	global $prefs;
	if(dbExist("`u_email` = '".htmlspecialchars($em)."'"))
		return err(LAN_PROFILE_1);
		
	if(!mysql_query("UPDATE `".$prefs['dbase']['prefix']."_users` SET `u_email` = '".htmlspecialchars($em)."' 
					WHERE `u_id` = '".UID."' "))
		return err(mysql_error());
		
	$data = loadadata();
	unset($data['ec_time']);
	unset($data['ec_code']);
	saveadata($data);
	remflag(FLAG_EMAIL_CONFIRMED);
	
	return msg(LAN_PROFILE_2);
}


function changephone($phone){
	if (!preg_match("'^9[0-9]{9}$'",$phone)) 
		return "Неверный формат телефона.";	
	
	$data = loadadata();
	
	if(isset($data['pc_time']))
		return err("СМС для подтверждения старого номера телефона уже была выслана. Подождите ".(13-round($tm/3600))." часов.");
	
	if(dbExist("`u_phone` = '".mysql_escape_string($phone)."'"))
		return err("Данный номер уже зарегистрирован в системе.");
	
	if(!mysql_query("UPDATE `".PREFIX."_users` SET `u_phone` = '".htmlspecialchars($phone)."' 
					WHERE `u_id` = '".UID."' "))
		return err(mysql_error());
		
	unset($data['pc_time']);
	unset($data['pc_code']);
	saveadata($data);
	remflag(FLAG_PHONE_CONFIRMED);
	
	return msg("Телефонный номер изменён.");
}

/*function changeinfo(){
	if(getflag(FLAG_NAME_CONFIRMED)){
		return err("Ваша личность уже подтверждена.");	
	}
	if( ($_POST['name'] == '')||($_POST['surname'] == '')||($_POST['group'] == '')||($_POST['room'] == '')){return err("Не заполнены обязательные поля.");}
	if(!preg_match("'^[а-я]+$'ui",$_POST['name'])) return  err("Неверно введено имя! Используйте только кирилицу.");
	if(!preg_match("'^[а-я\-]+$'ui",$_POST['surname'])) return  err("Неверно введена фамилия! Используйте только кирилицу.");
	if(!preg_match("'^[1-9]+-[1-9][0-9а-я]*$'ui",$_POST['room'])) return  err("Неверно введен номер комнаты. Используйте формат 6-523 (номер общежития - номер комнаты).");
	
	if(!mysql_query("UPDATE `".PREFIX."_users`  SET `u_room` = '".addslashes($_POST['room'])."', 
					`u_group` = '".addslashes($_POST['group'])."', `u_firstname` = '".addslashes($_POST['name'])."',
					`u_surname` = '".addslashes($_POST['surname'])."' WHERE `u_id` = ".UID))
			return err(mysql_error());
	$data = loadadata();
	unset($data['nc_error']);
	saveadata($data);
	remflag(FLAG_NAME_CONFIRMED);
		
	return msg("Личная информация изменена.");		
}*/


if(isset($_GET['phoneconf'])) confphone();

function confphone(){
	if(!getflag(FLAG_EMAIL_CONFIRMED)){
		echo err("Сначала подтвердите e-mail адрес.");
		return ;
	}
	
	if(!getflag(FLAG_NAME_CONFIRMED)){
		echo err("Сначала подтвердите вашу личность.");
		return ;
	}	
	
	$data = loadadata();
	if(getflag(FLAG_PHONE_CONFIRMED)){
		echo err("Телефонный номер уже подтвержден!");
		return;
	}
	if(isset($data['pc_time']))
		if(($tm = (time()-$data['pc_time']))<(60*60*12)){
				echo err("СМС для подтверждения телефона уже была выслана. Подождите ".(13-round($tm/3600))." часов");
				return;
		}
	
	$code = rand(100000,999999);
	
	$res = send_sms("Код регистрации в стиралке: \"".$code."\"");
	//print_r($res);
	if(isset($res['error'])){
		echo err("Произошла ошибка при отправке SMS: ".$res['error']);
		return;
	}
	
	$data['pc_time']=time();
	$data['pc_code']=$code;
	saveadata($data);
	echo msg("Код для подтверждения номера $phone выслан на телефон в СМС. Срок действия кода: 12 часов.");
	return;
}

function confirmphone($code){
	sleep(3);
	$data = loadadata();	
	if(isset($data['pc_time']))
		if( (time()-$data['pc_time']) < (60*60*12) )
			if(isset($data['pc_code']))
				if($data['pc_code']==$code){
					unset($data['pc_time']);
					unset($data['pc_code']);
					saveadata($data);
					addflag(FLAG_PHONE_CONFIRMED);
					update_info(KEYID,"`phone` = '".mysql_escape_string(getuser(UID,false,'u_phone'))."'");
					echo msg("Телефонный номер подтвержден.");										
				}else
					echo err("Неверный код подтверждения телефона.");
			else
				echo err("Внутренняя ошибка подтверждения номера.");
		else{
			echo err("Время подтверждения номера истекло.");
			unset($data['ec_time']);
			unset($data['ec_code']);
			saveadata($data);
		}
}

function changepass($op,$np,$rp){
  global $prefs;
  if ($np != $rp){return err(LAN_PROFILE_3);}
  if (strlen($np)<5){return err(LAN_PROFILE_4);}
  $mres = mysql_query("SELECT `u_password` FROM `".$prefs['dbase']['prefix']."_users` WHERE `u_id` = '".UID."' ");
  if(!$mres){return err(mysql_error()); }
  if(!mysql_num_rows($mres)){return err(LAN_PROFILE_5);}
  $row = @mysql_fetch_array($mres);
  if(!(md5($op) ==  $row['u_password'])){return err(LAN_PROFILE_6);}
  
  if(!mysql_query("UPDATE `".$prefs['dbase']['prefix']."_users` SET `u_password` = '".md5($np)."' WHERE `u_id` = '".UID."' ")){return err(mysql_error());}
  sendcookie($prefs['general']['cookie'].'_pass',md5($np));
  return msg(LAN_PROFILE_7);
}

function em_confirmed(){
	if(getflag(FLAG_EMAIL_CONFIRMED))
		return 1;
	$data = loadadata();
	if(isset($data['ec_time']))
		if( (time()-$data['ec_time']) < (60*60*12) )
			return 2;
		else{
			unset($data['ec_time']);
			unset($data['ec_code']);
			saveadata($data);
		}
	return 0;		
}

function phone_confirmed(){
	if(getflag(FLAG_PHONE_CONFIRMED))
		return 1;
	$data = loadadata();
	if(isset($data['pc_time']))
		if( (time()-$data['pc_time']) < (60*60*12) )
			return 2;
		else{
			unset($data['pc_time']);
			unset($data['pc_code']);
			saveadata($data);
		}
	return 0;		
}

/*function check_no($no){
	global $user_row;
	if(getflag(FLAG_NAME_CONFIRMED))
		return err("Личность уже подтверждена.");
	if(check_card_no($user_row['u_surname'],$user_row['u_firstname'],$no)){
		addflag(FLAG_NAME_CONFIRMED);	
		return msg("Личность подтверждена.");
	}else
		return err("Неверный номер карты!");
}

function name_confirmed(){
	global $user_row;
	if(getflag(FLAG_NAME_CONFIRMED))
		return 1;
	if(find_card($user_row['u_surname'],$user_row['u_firstname']))
		return 2;
	return 0;		
}*/


include('tiny_mce/wysiwyg.php');
?>
<form method="post" name="changepassform" action="?act=profile">
<table width="90%"  border="0" cellpadding="5px">
  <tr>
    <td colspan="2"><strong><div style="text-align:center "><?php echo LAN_PROFILE_9; ?>:</div></strong></td>
  </tr>
  <tr>
    <td width="40%" style="text-align:right "><?php echo LAN_PROFILE_10; ?>:</td>
    <td width="60%"><input name="oldpass" type="password" class="control" style="width:200px; "></td>
  </tr>
  <tr>
    <td style="text-align:right "><?php echo LAN_PROFILE_11; ?>:</td>
    <td><input name="newpass" type="password" class="control" style="width:200px;"></td>
  </tr>
  <tr>
    <td style="text-align:right "><?php echo LAN_PROFILE_12; ?>:</td>
    <td><input name="repass" type="password" class="control" style="width:200px;"></td>
  </tr>
  <tr>
    <td colspan="2"  style="text-align:center "><input name="changepass" type="submit" <?php if(UID==-1) echo 'disabled="disabled"'; ?> value="<?php echo LAN_PROFILE_13; ?>" class="control"></td>
  </tr>
</table>
</form>
<br />
<form method="post" name="emailconf" action="?act=profile">
<table width="90%"  border="0" cellpadding="5px">
  <tr>
    <td colspan="2"><strong><div style="text-align:center "><?php echo LAN_PROFILE_14; ?>:</div></strong></td>
  </tr>
  <tr>
    <td width="40%" style="text-align:right "><?php echo LAN_PROFILE_15; ?>:</td>
    <td width="60%"><input name="email" class="control" style="width:200px; " value="<?php echo getuser(UID,false,'u_email');	?>"></td>
  </tr>
  <tr>
    <td colspan="2"  style="text-align:center "><input name="changeemail" type="submit" <?php if(UID==-1) echo 'disabled="disabled"'; ?> value="<?php echo LAN_PROFILE_16; ?>" class="control"></td>
  </tr>
  <tr>
    <td width="40%" style="text-align:right "><?php echo LAN_PROFILE_61; ?>:</td>
    <td width="60%" id="lbl_em"><?php
	switch(em_confirmed()){
		case 0: {echo "<a href=\"#\" style='color:#C00' onclick=\"checkval('profile','econf','0','null','alert'); document.getElementById('lbl_em').innerHTML = '".LAN_PROFILE_57."'; return false;\" title=\"".LAN_PROFILE_58."\">".LAN_PROFILE_59."</a>"; break; }
		case 1: {echo LAN_PROFILE_60; break;};
		case 2: {echo LAN_PROFILE_57; break;}	
	}
			?></td>
  </tr>  
</table>
</form>

<br />
<form method="post" name="phoneconf" action="?act=profile">
<table width="90%"  border="0" cellpadding="5px">
  <tr>
    <td colspan="2"><strong><div style="text-align:center ">Настройка номера телефона:</div></strong></td>
  </tr>
  <tr>
    <td width="40%" style="text-align:right ">Сотовый телефон:</td>
    <td width="60%">+7 <input name="phone" class="control" style="width:200px; " maxlength="10" value="<?php echo getuser(UID,false,'u_phone');	?>"></td>
  </tr>
   <tr>
    <td></td><td><small>Ваш рабочий телефон, в формате 9201234567, без 8 или +7, 10 цифр.</small></td>
  </tr>  
  <tr>
    <td colspan="2"  style="text-align:center "><input name="changephone" type="submit" onclick="return confirm('Вы уверены, что хотите сменить номер телефона?\nПотребуется повторная проверка.');" <?php if(UID==-1) echo 'disabled="disabled"'; ?> value="<?php echo LAN_PROFILE_16; ?>" class="control"></td>
  </tr>
  <tr>
    <td width="40%" style="text-align:right "><?php echo LAN_PROFILE_61; ?>:</td>
    <td width="60%" id="lbl_ph"><?php
	switch($pc = phone_confirmed()){
		case 0: {echo "<a style='color:#C00' href=\"?act=profile&amp;phoneconf=1\" title=\"Подтвердить номер телефона\">Номер телефона не подтвержден</a>"; break; }
		case 1: {echo "Номер телефона подтвержден"; break;};
		case 2: {echo "Номер телефона подтверждается"; break;}	
	}
			?></td>
  </tr>  
  <?php if($pc == 2) { ?> 
  <tr>
    <td width="40%" style="text-align:right ">Код подтверждения:</td>
    <td width="60%"><input name="phone_code" class="control" style="width:200px; " maxlength="10" value=""></td>
  </tr>
   <tr>
  <td></td><td><small>Выслан Вам в SMS на телефон.</small></td>
  </tr>  
  <tr>
    <td colspan="2"  style="text-align:center "><input name="confirmphone" type="submit" <?php if(UID==-1) echo 'disabled="disabled"'; ?> value="Подтвердить" class="control"></td>
  </tr>
  <?php } ?>
</table>
</form>

<br />
<?php /*
$mres = mysql_query('SELECT `u_firstname`, `u_surname`, `u_room`, `u_group` FROM `'.PREFIX.'_users` WHERE `u_id` = '.UID);
$user_row = mysql_fetch_array($mres);
$nc = getflag(FLAG_NAME_CONFIRMED);
?>
<table width="90%"  border="0" cellpadding="5px">
<form method="post" name="infoconf" action="?act=profile">
  <tr>
    <td colspan="2"><strong><div style="text-align:center ">Персональные данные:</div></strong></td>
  </tr>
  <tr>
    <td style="text-align:right ">Имя:</td>
    <td><input name="name"  class="control" style="width:200px;" value="<?php echo $user_row['u_firstname']; ?>" <?php if($nc) echo 'readonly="readonly"'; ?>    /></td>
  </tr> 
  <tr>
    <td></td><td><small>Полное имя в соответствии с записью в социальной карте.</small></td>
  </tr>  
  <tr>
    <td style="text-align:right ">Фамилия:</td>
    <td><input name="surname"  class="control" style="width:200px;" value="<?php  echo $user_row['u_surname']; ?>" <?php if($nc) echo 'readonly="readonly"'; ?>    /></td>
  </tr>     
  <tr>
    <td></td><td><small>Фамилия в соответствии с записью в социальной карте.</small></td>
  </tr>  
  <tr>
    <td style="text-align:right ">Группа:</td>
    <td><input name="group"  class="control" style="width:200px;" value="<?php  echo $user_row['u_group']; ?>"  <?php if($nc) echo 'readonly="readonly"'; ?>   /></td>
  </tr>     
    <tr>
    <td style="text-align:right ">Общежитие и комната:</td>
    <td><input name="room"  class="control" style="width:200px;" value="<?php  echo $user_row['u_room'];?>"  <?php if($nc) echo 'readonly="readonly"'; ?> /></td>
  </tr>
  <tr>
    <td></td><td><small>Комната, в которой Вы проживаете, в формате 6-523 (номер общежития - номер комнаты).</small></td>
  </tr>
  <?php  if(!$nc){ ?> 
  <tr>
    <td colspan="2"  style="text-align:center "><input name="changeinfo" onclick="return confirm('Вы уверены, что хотите изменить информацию?');"  type="submit" <?php if(UID==-1) echo 'disabled="disabled"'; ?> value="<?php echo LAN_PROFILE_16; ?>" class="control"></td>
  </tr>
  <?php } ?>
  <tr>
    <td width="40%" style="text-align:right "><?php echo LAN_PROFILE_61; ?>:</td>
    <td width="60%" id="lbl_ph"><?php
	switch($nc = name_confirmed()){
		case 0: {echo "Личность не подтверждена, <a href='?act=regcard'>карта не найдена</a>."; break; }
		case 2: {echo "Личность не подтверждена, карта найдена."; break;};
		case 1: {echo "Личность подтверждена"; break;}
	}
			?></td>
  </tr>  
  <?php if($nc == 2) { ?> 
 </form>
<form method="post" name="nameconf" action="?act=profile">
  <tr>
    <td style="text-align:right ">Номер социальной карты:</td>
    <td><input name="cardno"  class="control" style="width:200px;" type="text"  value="<?php echo $_POST['cardno']; ?>" /></td>
  </tr>
  <tr>
    <td></td><td><small>19 значный номер на белой стороне социальной карты.</small></td>
  </tr>
   <tr>
    <td colspan="2"  style="text-align:center "><input name="check_card" type="submit" <?php if(UID==-1) echo 'disabled="disabled"'; ?> value="Проверить" class="control"></td>
  </tr>
  <?php } ?>
</form>
</table>*/