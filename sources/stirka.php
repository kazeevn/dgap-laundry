<?php
if(!defined("SUPINIT")){exit;}
define('CAPTION',"Запись на стирку");
define('PAGE_TITLE',"Запись на стирку");
//if(!ADMIN){echo err("Запись будет доступна 17 октября"); return;}

if(!USER){echo err("Необходимо войти в систему для записи на стирку"); exit;}

if(!user_confirmed()){
	echo err("Завершите все проверки для записи на стирку.");
	return;
}

function load_timetable(){
	$params = explode("|",$_GET['val']);
	$date = strtotime($params[0]);
	$dorm = is_numeric($params[1])?$params[1]:0;
	
	if($date === -1){
		echo "Неверный формат даты\n";
		return;
	}
	$date = floor($date / 60);

	$tm1 = microtime();
	$H = get_timetable($date);
	$tm2 = microtime();

	?>
    <p id="d1" style="display: none">
		<img src='images/dryer.png' alt='Б' title='Сушильный барабан' style="margin-bottom:-3px" /> - доступен сушильный барабан
		</p>
		<p id="d2" style="display: none">
		<img src='images/line.jpg' alt='M' title='Сушильное место' style="margin-bottom:-3px"  /> - доступно сушильное место
		</p>
    <div id="mhint" style="position: absolute; top: 10px; left: 10px; z-index:100; 
      border: 1px solid #BB0; background-color: #FFD;
      padding: 3px; display:none;"></div> 
	<?php
	
	$last_type = 0;
	
	foreach($H as $wid => $w){
		if($w['dorm'] != $dorm)
			continue;
		
		if($last_type != $w['type']){
      if($last_type) echo "</div>";
			$last_type = $w['type'];
      
      if($last_type != 1){
        echo "<p style='font-weight: bold; margin: 5px; cursor: pointer' onclick=\"$('#items_$last_type').slideToggle('fast')\">";
        switch($last_type){
          //case 1: echo "Стиральные машины"; break;
          case 2: echo "Сушильные барабаны"; break;
          case 3: echo "Сушильные места"; break;
        }
        echo ".</p>";
      }
      
      echo "<div id='items_$last_type' ".($last_type != 1?"style='display: none'":"").">";
		}
			
		//if(($w['type'] != 1)&&(!ADMIN ))
			//continue;
		?>
			<table class="tt" width="195px">
			<tr><th colspan="2"><?php echo $w['name']; ?></th></tr>
		<?php
		foreach($w['sessions'] as $s => $sess){//, $s
			$status =  $w['sessions'][$s];
			$a = '';
			
			if(isset($status['dry']) && $status['dry'][2]) {
				$a .= "<img src='images/dryer.png' alt='Б' title='Сушильный барабан' style=\"margin-bottom:-3px\" />";
        $d1 = 1;
      } else
        $d1 = 0;
      
			if(isset($status['dry']) && $status['dry'][3]){
				$a .= "<img src='images/line.jpg' alt='M' title='Сушильное место' style=\"margin-bottom:-3px\" />";	
        $d2 = 1;
      } else
        $d2 = 0;
      
      if($a)
				$a = "<span style='float:right'>&nbsp;$a</span>";
		
			if($status['day'] != 0)
				continue;
		
			echo "<tr><td".($s<t()?" style='background-color:#F5F5F5'":"").">".format_time($s).
						" &mdash; ".format_time($s+$w['quant'])."</td>
						<td style='background-color:$status[color]".(isset($status['act'])?"; cursor:pointer":"")."'  onmouseover=\"hint('".addslashes($status["hint"])."',$d1,$d2)\"
            onmouseout=\"unhint()\" ".(isset($status['act'])?" onclick='$status[act]' ":"").">
			$status[text]$a</td></tr>";
		} 
		
		?>
			</table>
		<?php 
	} 
	echo "</div>" ; //items_

}

function signup_form() {
	$sign = explode(".",$_GET['val']);
	if(!(is_numeric($sign[0])&&is_numeric($sign[1]))){
		echo "Ошибка при записи, повторите попытку или обратитесь к администратору.";
		return ;
	}

	$hw_id = $sign[0]; $session_id = $sign[1];
	$HW = get_timetable($session_id);
	$hw = $HW[$hw_id];
	$session = $hw['sessions'][$session_id];

	if($session['dry'][2]) 
		$id_2 = "2.".$session['dry'][2]['hw'].".".$session['dry'][2]['session'];
	else
		$id_2 = "";

	if($session['dry'][3]) 
		$id_3 = "3.".$session['dry'][3]['hw'].".".$session['dry'][3]['session'];
	else
		$id_3 = "";			

	if($hw['type']!=1){
		echo err("Запись возможна только на стирку.");
		return;
	}

	?>
	<form name="signup_form">
	<table cellspacing="4px">
	<?php 
	parse_session($hw, $session_id, t(), $price); // price by reference
	echo  '<input type="hidden" id="pr_1" value="'.$price.'" />';
	$init_price = $price;
	?>
	<tr>
	<td width="120px">Запись на сушку: </td>
	<td width="380px">
	<select name="dryer" onchange="table_dispay(this.value)">
	<?php 
	$dryes = array("0.0.0"=>'не записываться');
	if($id_2) $dryes[$id_2] = 'сушильный барабан';
	if($id_3) $dryes[$id_3] = 'сушильное место';
	echo parse_select($dryes,0); 
	?>
	</select>
	</td>
	</tr>
	</table>
	<?php
	if($session['dry'][2]){ 
		$dhw = $HW[$session['dry'][2]['hw']];
		$start_time = $session_id + (($dhw['reserve_event']==1)?0:($hw['quant']));
		echo '<table style="display:none" cellspacing="4px" id="tbl_'.$id_2.'">';
		parse_session($dhw, $session['dry'][2]['session'], $start_time, $price); //price by reference
		echo '</table>';
		echo "<input type='hidden' id='inst_".$id_2."' value='".($session['dry'][2]['session'] - $start_time)."' />";
		echo  '<input type="hidden" id="pr_'.$id_2.'" value="'.$price.'" />';
	} 
	if($session['dry'][3]){ 
		$dhw = $HW[$session['dry'][3]['hw']];
		$start_time = $session_id + (($dhw['reserve_event']==1)?0:($hw['quant']));
		echo '<table style="display:none" cellspacing="4px" id="tbl_'.$id_3.'">';
		parse_session($dhw, $session['dry'][3]['session'], $start_time, $price); //price by reference
		echo "<input type='hidden' id='inst_".$id_3."' value='".($session['dry'][3]['session'] - $start_time)."' />";
		echo '</table>';
		echo  '<input type="hidden" id="pr_'.$id_3.'" value="'.$price.'" />';
	} 
	
	global $sms_pretime, $prefs;

	$cookie = @$prefs['general']['cookie'];
	
	$dry_only = (($session['s'] == 3)&&($session['uid']==UID));

	?>    
	<table cellspacing="4px">   
	<tr><th colspan="2" align="left">SMS - напоминания:</th></tr>  
	<tr id="r_bw" <?php if($dry_only) echo 'style="display:none"'; ?>>
	<td>Начало стирки:</td><td><select id="bw"><?php echo  parse_select($sms_pretime, @$_COOKIE[$cookie.'_sms_bw']?:15); ?></select></td>
	</tr>  
	<tr id="r_aw" <?php if($dry_only) echo 'style="display:none"'; ?>>
	<td>Конец стирки:</td><td><select id="aw"><?php echo  parse_select($sms_pretime, @$_COOKIE[$cookie.'_sms_aw']?:15); ?></select></td>
	</tr>   
	
	<tr id="r_bd" style="display:none">
	<td>Начало сушки:</td><td><select id="bd"><?php echo  parse_select($sms_pretime, @$_COOKIE[$cookie.'_sms_bd']?:15); ?></select></td>
	</tr>  
	<tr id="r_ad" style="display:none">
	<td>Конец сушки:</td><td><select id="ad"><?php echo  parse_select($sms_pretime, @$_COOKIE[$cookie.'_sms_ad']?:15); ?></select></td>
	</tr>        
	<tr>
	<td align="left"><strong>Итого:</strong></td>
	<td><span id='price'><?php echo $init_price/100; ?></span> руб.</td>
	</tr>  
	<tr>
	<td align="left" width="120px"><a href="javascript:finish('<?php echo $_GET['val'] ?>')">Запись</a></td>
	<td align="right" width="380px"><a onclick="$('#dialog').dialog('close'); return false;" href="#">Отмена</a></td>
	</tr>   
	</table>
	</form>
	
	<?php
	
}

function finish_signup() {
	$input = explode(",",$_GET['val']);
	
	$sign = explode(".",$input[0]);
	if(!(is_numeric($sign[0])&&is_numeric($sign[1])&&is_numeric($sign[2])&&is_numeric($sign[3])&&is_numeric($sign[4]))){
		echo "Ошибка 2 при записи, повторите попытку или обратитесь к администратору.";
		return ;
	}

	$hw_id = $sign[0];
	$sess_id = $sign[1];
	$dry_type = $sign[2];
	$dry_id = $sign[3];
	$dry_sess = $sign[4];

	$HW = get_timetable($sess_id);

	if($HW[$hw_id]['type']!=1){
		echo err("Запись возможна только на стирку.");
		return;
	}

	$valid = isset($HW[$hw_id]) && isset($HW[$hw_id]['sessions'][$sess_id]);
	$valid = $valid && ( ($HW[$hw_id]['sessions'][$sess_id]['s'] == 0) || 	(
		($HW[$hw_id]['sessions'][$sess_id]['s'] == 3)&&($HW[$hw_id]['sessions'][$sess_id]['dry_id'] == 0)
		&&($HW[$hw_id]['sessions'][$sess_id]['uid'] == UID)			)
	);

	if($dry_type != 0){
		$valid = $valid && isset($HW[$hw_id]['sessions'][$sess_id]['dry'][$dry_type]);	
		if($valid){
			$dry = 	$HW[$hw_id]['sessions'][$sess_id]['dry'][$dry_type];
			$valid = $valid && isset($dry['hw']);
			$valid = $valid && ($dry['hw'] == $dry_id) && isset($HW[$dry_id]);
			$valid = $valid && isset($HW[$dry_id]['sessions'][$dry_sess]);
			$valid = $valid && ($HW[$dry_id]['sessions'][$dry_sess]['s'] <= 0);
		}
		
	}else{
		$dry_id = $dry_sess = 0;
	}

	if(!$valid){
		echo err("Ошибка 1 при записи, повторите попытку или обратитесь к администратору.");
		return ;			
	}

	$sms = array();
	unset($input[0]);
	foreach($input as $key => $sms_t)
		$sms[$key] = max(min(is_numeric($sms_t)?$sms_t:0,60),0);


	if(signup($HW, $hw_id, $sess_id, $dry_id, $dry_sess, $sms, $error))
		echo msg("Запись на стирку сделана.");
	else
		echo err("Ошибка записи на стирку: ".$error);

}

function list_sessions() {
	$washes = get_washes();
	if($washes && count($washes)){
		echo "<ol  style='padding-left:15px'>";
		foreach($washes as $id => $row){
			echo "<li>".format_datetime($row['start'])." &mdash; ".format_time($row['stop']).", ".
			(($row['start'] <= t())?"сейчас":"через ".timetostr($row['start'] - t())).", $row[d_name]: $row[hw_name] ";
			
			if($row['dry_id'] == 0){
				$HW = get_timetable($row["start"]);
				if(isset($HW[$row['hw_id']]['sessions'][$row["start"]]['dry'])){
					$dry = $HW[$row['hw_id']]['sessions'][$row["start"]]['dry'];
					if(is_array($dry[2]) || is_array($dry[3]))
						echo "<a href='javascript:signup(\"$row[hw_id].$row[start]\")'>Запись на сушку</a>";
				}
			}

			echo " <a href='javascript:cancel($id)' onclick='return confirm(\"Отменить стирку?\")'>Отмена</a></li>";	
		}		
		echo "</ol>";
	}else
		echo "Записей на стирку нет.";	
}

function cancel_session() {
		if(isset($_GET['val']) && is_numeric($_GET['val']))
			if(calcel_wash($_GET['val'],$error))
				echo msg("Запись отменена".($error?(" ($error)"):""));
			else
				echo err($error);
		list_sessions();
}


if(isset($_GET['sub'])){

	if($_GET['sub'] == 'load'){
		load_timetable();
	}elseif($_GET['sub'] == 'signup'){
		signup_form();
	}elseif($_GET['sub'] == 'finish'){
		finish_signup();
	}elseif($_GET['sub'] == 'cancel'){
		cancel_session();		
	}elseif($_GET['sub'] == 'stirki'){
		list_sessions();
	}
	
	return;
}

function parse_session($hw,$session,$start, &$price){
		
		if(($hw['sessions'][$session]['s'] == 3)&&($hw['sessions'][$session]['uid']==UID)) {
			$price = 0;
			$signed_up = true;
		} else
			$price = get_cost($hw,$session,$comments);
		
	?>
    	<tr>
        	<td width="120px">Запись на<?php echo ($hw['type']==1)?' стирку':'' ?>:</td>
            <td width="380px"><strong><?php echo $hw['name']; ?></strong></td>
        </tr>
        <tr>
        	<td>Время <?php echo ($hw['type']==1)?'стирки':'сушки' ?>:</td>
            <td>
			<?php 
				echo format_datetime($session)." &mdash; ";
				if(start_of_the_day($session) == start_of_the_day($session+$hw['quant']))
					echo format_time($session+$hw['quant']);
				else
					echo format_datetime($session+$hw['quant']);
				if ($session-$start > 0) echo ", <span style='color:#272'>через ".timetostr($session-$start)."</span>";  
				$avail = $hw['quant']+min(0,$session-$start);
				if(($avail / $hw['quant']) < 0.75) echo "<br /><span style='color:#D44'>всего лишь</span> ".timetostr($avail);
			?>
            </td>
        </tr>
 
        <tr>
        	<td>Стоимость:</td>
            <td><?php if(isset($signed_up))
						echo '<span style="color:#090">Оплачено</span>';
					else
						echo costtostr($price, false).(is_array($comments)?"<br />".implode("<br />", $comments):"");  ?></td>
        </tr> 
	<?php	
}

if(isset($_COOKIE['def_dorm']))
	$def_dorm = $_COOKIE['def_dorm'];
else{
	$d = get_user_room_and_dorm();
	$def_dorm = $d['dorm'];
}

?>
<style>

</style>
             
<h3>Текущие стирки</h3>
<div id="stirki">Загрузка списка стирок...</div>
<h3>Запись на стирку</h3>
<p>
<label for"dormsel">Общежитие: </label>
<select id="dormsel" onchange="load_tt()" ><?php echo parse_select(get_dorms(),$def_dorm); ?></select> 
<label for="datesel" >Дата стирки: </label> 
<a href="javascript:tomorrow(-1);"><img alt="Предыдущий день" src="images/arrow_left.png" title="Предыдущий день" /></a>
<input type="text" id="idate" value="<?php echo format_date(); ?>" maxlength="10" readonly="readonly" onchange="load_tt()" 
		style="width:80px; text-align:center" /> 
       <a href="javascript:tomorrow(1);"><img alt="Следующий день" src="images/arrow_right.png" title="Следующий день" /></a>  
        <img src="images/arrow_refresh.png" onclick="load_tt(); return false;" alt="Обновить" title="Обновить" style="cursor:pointer"  />
         <span id="status"></span>
         
</p>
<script type="text/javascript">

function tomorrow(s){
	var d = new Date($('#idate').datetimepicker('getDate'));
	var ms = d.getTime();
	d.setTime(ms+s*1000*60*60*24);
	$('#idate').datetimepicker('setDate',d);
	load_tt();
}

function get_sms_val(id){
	if(document.getElementById("r_"+id).style.display == '')
		return document.getElementById(id).value;
	else
		return 0;
}

var last_tbl = undefined;
function table_dispay(id){
	if(last_tbl != undefined) {
		last_tbl.style.display = 'none';
		document.getElementById("r_bd").style.display = 'none';
		document.getElementById("r_ad").style.display = 'none';
	}
	last_tbl = document.getElementById("tbl_"+id);
	if(last_tbl != undefined) 	{
		last_tbl.style.display = '';
		if(document.getElementById("inst_"+id).value > 0)
			document.getElementById("r_bd").style.display = '';
		document.getElementById("r_ad").style.display = '';
	}
	
	
	var price = parseInt(document.getElementById('pr_1').value);
	var a = document.getElementById("pr_"+id);
	if(a != undefined)
		price = price + parseInt(a.value);
	document.getElementById("price").innerHTML = price/100;
	if(price > <?php echo BALANCE+ALLOW_NEGATIVE_MONEY ?>)
		document.getElementById("price").style.color = "#944";
	else
		document.getElementById("price").style.color = "";
}

function setCookie(c_name,value,exdays)	{
	var exdate=new Date();
	exdate.setDate(exdate.getDate() + exdays);
	var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
	document.cookie=c_name + "=" + c_value;
	//alert(document.cookie);
}

function load_tt(){
	var date = document.getElementById("idate").value;
  var dorm = document.getElementById("dormsel").value;
	setCookie("def_dorm",dorm,365);
	checkval("stirka","load",date+"|"+dorm,"status","load_d");
}

function load_d(text){
	document.getElementById("status").innerHTML = "";
	document.getElementById("tt").innerHTML = text;
	load_strirki();
}


function hint(text, d1, d2){
	var h = document.getElementById("mhint") ;
	if(h != undefined){
		h.innerHTML = "Статус: "+text;
    if(d1 != 0)
      h.innerHTML += "<br />"+document.getElementById("d1").innerHTML;
    if(d2 != 0)
      h.innerHTML += "<br />"+document.getElementById("d2").innerHTML;
    h.style.display = "";
  }
}

function unhint(){
  var h = document.getElementById("mhint") ;
  if(h != undefined){
    h.innerHTML = "";
    h.style.display = "none";
  }  
}

function signup(id){
	document.getElementById("dialog").innerHTML = "	<p><center>Загрузка данных о стирке.</center></p>";
  $( "#dialog" ).dialog( "option", "title", 'Запись на стирку.' );
	$("#dialog").dialog("open");
	checkval("stirka","signup",id,"status","set_signup_dialog_text");	
}

function info(id){
  document.getElementById("dialog").innerHTML = " <p><center>Загрузка данных о пользователе.</center></p>";
  $( "#dialog" ).dialog( "option", "title", 'Данные о пользователе.' );
  $("#dialog").dialog("open");
  checkval("member","info",id,"status","set_signup_dialog_text"); 
}

function set_signup_dialog_text(text){
	document.getElementById("dialog").innerHTML = text;	
}

function finish(id){
	var val = id+"."+document.forms['signup_form'].elements['dryer'].value;
	
	val = val + ',' + get_sms_val('bw');
	val = val + ',' + get_sms_val('aw');
	val = val + ',' + get_sms_val('bd');
	val = val + ',' + get_sms_val('ad');
	
	//alert(val);
	checkval("stirka","finish",val,"status","set_signup_dialog_text");	
}

function load_strirki(){
	checkval("stirka","stirki","","n","set_stirki");	
}


function cancel(id){
	checkval("stirka","cancel",id,"n","set_stirki");		

}

function set_stirki(text){
	document.getElementById("stirki").innerHTML = text;
	
}

window.onload = load_tt;
</script>

<?php dp("idate");  ?>

<div id="tt">
</div>


<?php include_once("jquery/jquery.php"); ?>

<div id="dialog"  title="Запись на стирку">
</div>

<script type="text/javascript">
  function min(a, b){
    return (a>b)?b:a;
  }

	$("#dialog").dialog({ autoOpen: false, modal: true, width: 500, minHeight: 200, close: load_tt, resizable: false });
  
  jQuery(document).ready(function(){
    $("#tt").mousemove(function(e){
      var hint = $("#mhint");
      var W = min($(window).width()+$(window).scrollLeft(), $(document).width());
      if(W > e.pageX + 7 + hint.outerWidth() + 20)
        hint.css("left",(e.pageX+7)+"px");
      else
        hint.css("left",(e.pageX-7-hint.outerWidth())+"px");
      
      var H = min($(window).height()+$(window).scrollTop(), $(document).height());
      if(H > e.pageY + 7 + hint.outerHeight() + 10)
        hint.css("top",(e.pageY+7)+"px");
      else
        hint.css("top",(e.pageY-7-hint.outerHeight())+"px");
    }); 
  })
  
  
</script>

