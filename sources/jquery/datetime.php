<?php include_once("jquery.php"); ?>	
	
	<script src="sources/jquery/jquery-ui-timepicker-addon.js" type="text/javascript"></script>
    <script type="text/javascript">
		$.datepicker.regional['ru'] = {
			closeText: 'Закрыть',
			prevText: '<Пред',
			nextText: 'След>',
			currentText: 'Сегодня',
			monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
			'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
			monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
			'Июл','Авг','Сен','Окт','Ноя','Дек'],
			dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
			dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
			dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
			weekHeader: 'Не',
			dateFormat: 'dd.mm.yy',
			firstDay: 1,
			isRTL: false,
			showMonthAfterYear: false,
			yearSuffix: ''
		};
		$.datepicker.setDefaults($.datepicker.regional['ru']);
		
		$.timepicker.regional['ru'] = {
			timeOnlyTitle: 'Выберите время',
			timeText: 'Время',
			hourText: 'Часы',
			minuteText: 'Минуты',
			secondText: 'Секунды',
			millisecText: 'миллисекунды',
			currentText: 'Сейчас',
			closeText: 'Закрыть',
			ampm: false
		};
		$.timepicker.setDefaults($.timepicker.regional['ru']);
	</script>
<?php
/* See http://trentrichardson.com/examples/timepicker/ */

function datetime_picker($id){
?>
<script type="text/javascript">
	$('#<?php echo $id; ?>').datetimepicker({hourGrid: 2, 	minuteGrid: 5, numberOfMonths: 2});
</script>
<?php	
}

function date_picker($id, $use_min_date = true, $add= ''){
?>
<script type="text/javascript">
	$('#<?php echo $id; ?>').datepicker({ numberOfMonths: 2 <?php if($use_min_date) echo ", minDate: -1";
															 if($add) echo ", $add"; ?>});
</script>
<?php	
}

?>
