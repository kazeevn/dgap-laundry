<?php 
if(!defined("SUPINIT")){exit;}
if(!USER){exit;}
if(!(ADMIN || CASHIER)){exit;}

if(!defined("CAPTION"))
  define('CAPTION',"Статистика переводов.");

if($_GET['since'] && ( ($s = strtotime($_GET['since'])) != -1 ) )
	$s = floor($s / 60);
else
	$s = start_of_the_day(t());
	
if($_GET['until'] && ( ($u = strtotime($_GET['until'])) != -1 ) )
	$u = floor($u / 60)+1;
else
	$u = t();

if(isset($_GET['id']) && ADMIN)
  $clause = " AND `t_cashier` = '".mysql_escape_string($_GET['id'])."'";
elseif(CASHIER && !ADMIN)
  $clause = " AND `t_cashier` = '".UID."'";
else
  $clause = "";
	

?>
<form action="" method="get">
<input type="hidden" name="act" value="admin_transactions" /> 
 Статистика с <input id="since" name="since" value="<?php echo format_datetime($s) ?>" /> 
 	по <input id="until"  name="until" value="<?php echo format_datetime($u) ?>" /> 
   <input type="submit" value="Показать"  />
</form>
<?php 
	include("jquery/datetime.php");
	datetime_picker("since");
	datetime_picker("until");
?>
<table width="100%"   style='border-collapse:collapse;' id='hw_list'>
  <tr>
    <td>Время</td>
    <td>Источник</td>
    <td>Пользователь</td>
    <td>Сумма</td>	
    <td>Статус</td>
    <td>Комментарий</td>
  </tr>
   <?php
    $mres = mysql_query("		
		SELECT `t_uid`, `t_time`, `t_summ`, `t_status`, `t_comment`, users.`u_firstname` as uf, users.`u_surname` as us,
            cashiers.`u_firstname` as cf, cashiers.`u_surname` as cs, `t_cashier`
		FROM `op_transactions`
		JOIN `op_users` as users ON `t_uid` = users.`u_id`
		LEFT OUTER JOIN `op_users` as cashiers ON `t_cashier` = cashiers.`u_id`
		WHERE `t_time` >= '".date("Y-m-d H:i:s",$s*60)."' AND `t_time` < '".date("Y-m-d H:i:s",$u*60)."'
		$clause
		ORDER BY `t_time` ASC 
		");
	
	echo mysql_error();
	$total_income = 0;
	
	while($row = @mysql_fetch_array($mres)){
    if($row['t_status'] == 1)
      $total_income += $row['t_summ'];
    $name = $row['cf']?"$row[cf] $row[cs]":"Внешний";
    $lnk = (!$clause)?"<a href='?act=admin_transactions&amp;id=$row[t_cashier]&amp;since=$_GET[since]&amp;until=$_GET[until]'>$name</a>":$name;
	echo "<tr  style='border-top: 1px dotted #CCC'>
    <td>".$row["t_time"]."</td>
	<td>$lnk</td>
	<td><a href='?act=member&amp;id=$row[t_uid]'>$row[uf] $row[us]</a></td>
  <td>".costtostr($row["t_summ"],false)."</td>
	<td>".($row['t_status']==1?"OK":"fail")."</td>
   	<td>$row[t_comment]</td>
	 </tr>";
	} 
  ?>
  <tr>
    <td></td>
    <td></td>
    <td align="right"><strong>Итого:</strong></td>
    <td><?php echo costtostr($total_income,false); ?></td>	
    <td></td>
    <td></td>
  </tr>  
</table>
