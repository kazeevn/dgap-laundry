<?php
if(!isset($openproject_init)){die();}



global $hw_types;
$hw_types = array(
				  	1 => "Стиральная машина",
					2 => "Сушильный барабан",
					3 => "Сушильнае место"
				  );

global $reserve_events;
$reserve_events = array(
				  	1 => "начала",
					2 => "конца"
				  );

global $quants;
$quants = array(
				  	1 => "1 час",
					2 => "2 часа",
					3 => "3 часа",
					4 => "4 часа",
					6 => "6 часов",
					8 => "8 часов",
					12 => "12 часов",
					24 => "24 часа"
				  );

global $hw_statuses;
$hw_statuses = array(
				  	0 => "работает",
					1 => "на ремонте",
					2 => "отключена"
				  );

global $a_statuses;
$a_statuses = array(
				  	0 => "включён",
					1 => "отключён",
				  );

global $sms_pretime;
$sms_pretime = array(
				  	0 => "не присылать",
					5 => "за 5 минут",
					10 => "за 10 минут",
					15 => "за 15 минут",
					20 => "за 20 минут",
					30 => "за 30 минут",
					45 => "за 45 минут",
					60 => "за 1 час",
				  );


function get_dorms(){
  $mres = mysql_query("SELECT `d_id`, `d_name` FROM `op_dorms`");
  if(!$mres) 
    return false;
  $r = array();
  while($row = mysql_fetch_array($mres))
    $r[$row['d_id']] = $row['d_name'];
  mysql_free_result($mres);
  return $r; 
}

function get_sessions($quant, $shift, $base_day = 0, $pre, $post){
  if($quant == 0)
    die("Zero quant!");
	$shift = $shift % ($quant);
	
	if($base_day == 0) $base_day = t();
	
	$base_day = start_of_the_day($base_day);
	
	
	$sessions = array();
	$time = $shift;
	
	while( ($time-($quant)) >= (-$pre) )
		$time = $time - ($quant);
        
          
	while($time < (24*60 + $post)){
		
		if($time < 0)
			$sessions[$base_day + $time]=array('day' => -1);
		elseif($time >= 24*60)
			$sessions[$base_day + $time]=array('day' => +1);
		else
			$sessions[$base_day + $time]=array('day' => 0);
			
		$time = $time + $quant;
	}	
	
	return $sessions;
}

function get_hardware(&$MAX_WASH_QUANT, &$MAX_RESERVE_TIME, &$MAX_DRY_PRESIGN, &$MAX_DRY_POSTSIGN){
	$mres = mysql_query("SELECT `hw_id`, `hw_name`, `hw_type`, `hw_quant`, `hw_timeshift`, `hw_presign`, 
				`hw_postsign`, `hw_cost`, `hw_status`, `hw_reserve`, `hw_reserve_event`, `hw_dorm`
				FROM `op_hardware` WHERE `hw_status` <= 1 ORDER BY `hw_type` ASC, `hw_name` ASC");	
	if(!$mres) 
		return false;
	

	$MAX_WASH_QUANT = 0;
	$MAX_RESERVE_TIME = 0;
	$MAX_DRY_PRESIGN = 0;
	$MAX_DRY_POSTSIGN = 0;
	
	
	$r = array();
	while ($row = mysql_fetch_array($mres)) {
		
		$r[$row['hw_id']]['id'] = $row['hw_id'];
		$r[$row['hw_id']]['name'] = $row['hw_name'];
		$r[$row['hw_id']]['type'] = $row['hw_type'];
		$r[$row['hw_id']]['dorm'] = $row['hw_dorm'];
		$r[$row['hw_id']]['quant'] = $row['hw_quant']*60;
		$r[$row['hw_id']]['shift'] = $row['hw_timeshift'];
		$r[$row['hw_id']]['presign'] = $row['hw_presign'];
		$r[$row['hw_id']]['postsign'] = $row['hw_postsign'];
		$r[$row['hw_id']]['cost'] = $row['hw_cost'];
		$r[$row['hw_id']]['status'] = $row['hw_status'];
		$r[$row['hw_id']]['reserve'] = $row['hw_reserve'];
		$r[$row['hw_id']]['reserve_event'] = $row['hw_reserve_event'];
		
		
		/*$stops = array();
		$m = mysql_query("SELECT `stop_id`, `stop_since`, `stop_until`, `stop_comment` 
							 FROM `op_stops` WHERE (`stop_hid` = $row[hw_id])");
		while($s = mysql_fetch_array($m)){
			$stop = array();
			$stop['id'] = $s['stop_id'];
			$stop['since'] = $s['stop_since'];
			$stop['until'] = $s['stop_until'];
			$stop['comment'] = $s['stop_comment'];
			$stops[] = $stop;
		}

		$r[$row['hw_id']]['stops'] = $stops;*/
		 
		if($row['hw_type'] == 1){
			if($MAX_WASH_QUANT < ($row['hw_quant']*60))
				$MAX_WASH_QUANT = ($row['hw_quant']*60);
		} else {
			if($MAX_RESERVE_TIME < $row['hw_reserve'])
				$MAX_RESERVE_TIME = $row['hw_reserve'];
			if($MAX_DRY_PRESIGN < $row['hw_presign'])
				$MAX_DRY_PRESIGN = $row['hw_presign'];
			if($MAX_DRY_POSTSIGN < $row['hw_postsign'])
				$MAX_DRY_POSTSIGN = $row['hw_postsign'];
		}
			
	}
	
	mysql_free_result($mres);
	return $r;		
}

function get_stops(&$HW){
		$m = mysql_query("SELECT `stop_id`, `stop_hid`, `stop_since`, `stop_until`, `stop_comment` FROM `op_stops`");
		while($s = mysql_fetch_array($m)){
			$stop = array();
			$stop['since'] = $s['stop_since'];
			$stop['until'] = $s['stop_until'];
			$stop['comment'] = $s['stop_comment'];
      if(isset($HW[$s['stop_hid']]))
        $HW[$s['stop_hid']]['stops'][$s['stop_id']] = $stop;
		}
}

function find_free_session($HW, $dorm, $type, $session_start, $session_stop){
	foreach($HW as $hw_id => $h)
	  if(($h['type'] == $type)&&($h['dorm'] == $dorm)){
			$value = ($h['reserve_event']==1)?$session_start:$session_stop;
			foreach($h['sessions'] as $st => $sess)
				if($sess['s'] <= 0){ //reserve or free
					if( abs($value - $st) < $h['reserve'] )	
						return array("hw" => $hw_id, "session" => $st);
				}
		}
		
	foreach($HW as $hw_id => $h)
	  if(($h['type'] == $type)&&($h['dorm'] == $dorm)){
			$value = ($h['reserve_event']==1)?$session_start:$session_stop;
			foreach($h['sessions'] as $st => $sess)
				if($sess['s'] == 0){ //free only
				
					if( (($value > $st)&&($value-$st <= $h['postsign']))||
							(($st > $value)&&($st-$value <= $h['presign']))  )
						return array("hw" => $hw_id, "session" => $st);					
						
				}
		}					
		
	return false;
}

function get_status($HW, $hwid, $sess_start){
	$hw = $HW[$hwid];
	$sess_stop	= $sess_start + $hw['quant'];
	
	if($hw['status'] > 0)
		return  array('s' => 1, 'text' => "не работает", 'color' => "#FBB", 'hint' => "Поломка оборудования");
		
	if(isset($hw['stops']) and is_array($hw['stops'])){
		foreach($hw['stops'] as $stop){
			if(($stop['since'] < $sess_stop)&&($stop['until'] > $sess_start))
				return  array('s' => 2, 'text' => "остановлена", 'color' => "#FEE", 'hint' => $stop['comment']);		
		}
	}
	
	if(isset($hw['su']) and is_array($hw['su'])){
		foreach($hw['su'] as $sid => $su){
			if(($su['start'] < $sess_stop)&&($su['stop'] > $sess_start))
				return  array('s' => 3, 'text' => "занята", 'uid' => $su['uid'],
								'dry_id' =>$su['dry_id'] , 'sid' => $sid, 'color' => (($su['start'] > t())?"#FFB":"#F5F5F5"),
								'hint' => "Занято ".$su['name']."", 'act' => "info($su[uid])");		
		}
	}
	
	
	$timeleft = $sess_start - t();
	if($timeleft < 0){
		$timepassed = - $timeleft;
		if($timepassed > $hw['postsign'])
			return  array('s' => 4, 'text' => "завершена", 'color' => "#F5F5F5", 'hint' => "Время стирки прошло");
	}else{
		if($hw['type'] == 1)
			if($timeleft > $hw['presign'])
				return  array('s' => -1, 'text' => "закрыта", 'color' => "#DDF", 'hint' => "Запись ещё не началась");		
	}

	if($hw['type'] != 1)
		return array('s' => 0, 'text' => "свободно", 'color' => "#DFD", 'hint' => "Запись только на сушку невозможна");
	
	$id = $hwid.".".$sess_start;
  return array('s' => 0, 'text' => "свободно", 'color' => "#DFD", 'hint' => "Запись возможна", 'act' => "signup(\"$id\")");	
}

function get_statuses(&$HW){
	foreach($HW as $key => $w)
		foreach($w['sessions'] as $start => $sess)
			$HW[$key]['sessions'][$start] = $HW[$key]['sessions'][$start] + get_status($HW,$key,$start);
	
}

function get_signups(&$HW, $date, $pre, $post){
	$prev = start_of_the_day($date)-$pre;
	$next = $date + 24*60 + $post;
	
	$mres = mysql_query("SELECT `s_id`, `s_hid`, `s_uid`, `s_start`, `s_stop` , `u_firstname`, `u_surname`, `u_room`, `s_dry_id`
						FROM `op_signups`, `op_users` WHERE (`s_start` < $next)AND(`s_stop` > $prev)AND(`s_uid`=`u_id`)");
	
	if(!$mres) 
		return false;
	
	
	while($row = mysql_fetch_array($mres)){
		if(isset($HW[$row['s_hid']])){
      $HW[$row['s_hid']]['su'][$row['s_id']]['start']	= $row['s_start'];
      $HW[$row['s_hid']]['su'][$row['s_id']]['stop']	= $row['s_stop'];
      $HW[$row['s_hid']]['su'][$row['s_id']]['uid']	= $row['s_uid'];
      $HW[$row['s_hid']]['su'][$row['s_id']]['name']	= $row['u_firstname']." ".$row['u_surname']." ($row[u_room])";
      $HW[$row['s_hid']]['su'][$row['s_id']]['dry_id']= $row['s_dry_id'];
    }
	}
	
	return true;

}

function setup_sessions(&$HW, $date, $pre_dry, $post_dry){
  foreach($HW as $key => $hw)
		$HW[$key]['sessions'] = get_sessions($hw['quant'], $hw['shift'], $date, $pre_dry, $post_dry);
}

function find_driers(&$HW){

	foreach($HW as $hw_id => $hw){
		if($hw['type'] != 1)
			continue;
		
		foreach($hw['sessions'] as $sess_start => $sess){
			if(!( ($sess['s'] == 0) || (($sess['s'] == 3)&&($sess['uid'] == UID)&&($sess['dry_id'] == 0))  ))
				continue;
			
			foreach(array(2,3) as $t)
				$found[$t] = find_free_session($HW, $hw['dorm'], $t, $sess_start, $sess_start + $hw['quant']);	
				
			$HW[$hw_id]['sessions'][$sess_start]['dry'] = $found;
			//echo "$a";
		}
	}
}

function find_reserve($HW,$dorm,$start,$res_time, $res_event){
	foreach($HW as $hw_id => $h)
		if(($h['type'] == 1)&&($h['dorm'] == $dorm))
			foreach($h['sessions'] as $session_start => $sess){
				$value = ($res_event==1)?$session_start:($session_start+$h['quant']);
				if($sess['s'] <= 0){
					if( abs($value - $start) < $res_time )	
						return true;
				}
			}
		
						
		
	return false;	
}

function find_reserves(&$HW){
	
	foreach($HW as $hw_id => $hw){
		if($hw['type'] == 1)
			continue;
		
		foreach($hw['sessions'] as $sess_start => $sess){
			if($sess['s'] > 0)
				continue;
			
			if(find_reserve($HW, $hw['dorm'], $sess_start, $hw['reserve'], $hw['reserve_event']))
				foreach(array('s' => -2, 'text' => "резерв", 'color' => "#FEE") as $key => $val)
					$HW[$hw_id]['sessions'][$sess_start][$key] = $val;
			
		
		}
	}		

}

function check_hardware($hwid, $start_time, $stop_time){
	$H  = get_hardware($max_wash_time, $max_reserve_time, $max_presing_time, $max_postsign_time); // all by refence
	get_stops($H); //by reference
	
	if(!isset($H[$hwid]))
		return false;
	$h = $H[$hwid];
	if($h['status'] != 0)
		return false;
		
	if(is_array($h['stops']))
		foreach($h['stops'] as $stop)
			if(($stop['since'] < $stop_time)&&($stop['until'] > $start_time))
				return false;

	return true;
}

function get_timetable($date){
	$H  = get_hardware($max_wash_time, $max_reserve_time, $max_presing_time, $max_postsign_time); // all by reference
	
	$pre_dry = $max_wash_time + $max_reserve_time + $max_reserve_time + $max_presing_time;	//Additional sessions over this day we might be interested in
	$post_dry = $max_wash_time + $max_reserve_time + $max_reserve_time + $max_postsign_time;
	
  
	get_stops($H);// all by reference
	setup_sessions($H, $date, $pre_dry, $post_dry); // H by refernce
	get_signups($H, $date, $pre_dry, $post_dry); // H by reference 
	get_statuses($H); // H by reference;
	find_reserves($H); // H by reference;
	find_driers($H); // H by reference;
	return $H;
}

function create_price_counter($code){
	$rep = error_reporting(E_ALL);
	$f = create_function(get_price_counter_arguments(), $code);
	if(!$f){
		error_reporting($rep);
		return false;
	}
	$a = array();
	$f(array(),0,array(),0,0,0,0,$price,$a,$c);
	error_reporting($rep);
	return $f;
}

function get_price_counter_arguments(){
	return '$hardware_info, $session, $user_last_sessions, $uid, $user_group, $user_dorm, $user_room, &$price, &$user_data, &$comment';	
}

function get_cost($hw, $session, &$comments, $final = false){
	$cost = $hw['cost'];
	
	//hardware_info, $session, $user_last_sessions,  $user_group, $user_dorm, $user_room, &$price, &$user_data, &$comment
	$mres = mysql_query("SELECT `a_code` FROM `op_actions` WHERE (`a_status` = 0)AND(`a_dorm` = ".($hw['dorm']).")");
	if($mres && mysql_num_rows($mres)){
		$ug = getuser(UID,false,'u_group');
		$place = get_user_room_and_dorm();
		$data = loadadata();
		$comments = array();
		$sessions = get_washes(60*24*31*2);
		
		while($row = mysql_fetch_array($mres)){
			$comm = '';
			$f = create_price_counter($row['a_code']);
			$f($hw,$session,$sessions,UID,$ug,$place['dorm'],$place['room'],$cost,$data,$comm);
			if($comm)
				$comments[] = $comm;
		}		
		
		if($final)
			saveadata($data);
	}
	mysql_free_result($mres);
		
	
	return round($cost);
}


function signup_session($hw_id, $sess_id, $sess_stop, $price, $pre_sms, $post_sms){
	if(mysql_query("INSERT INTO `op_signups` (`s_hid`, `s_uid`, `s_start`, `s_stop`, `s_cost`, `s_pre_sms`, `s_post_sms`) 
										  VALUES ($hw_id, ".UID.", $sess_id, $sess_stop, $price, $pre_sms, $post_sms) "))
		return mysql_insert_id();
	else
		return false;
}

function delete_session($sid){
	if(mysql_query("DELETE FROM `op_signups` WHERE (`s_id` = $sid)AND(`s_uid` = ".UID.") ")){
		$ar = mysql_affected_rows();
		if($ar)
			mysql_query("UPDATE `op_signups` SET `s_dry_id` = 0 WHERE `s_dry_id` = ".$sid);
		return $ar;
	}else
		return false;
}

function set_dry_id($wid, $did){
	if(mysql_query("UPDATE `op_signups` SET `s_dry_id` = $did WHERE (`s_id` = $wid)AND(`s_uid` = ".UID.") "))
		return mysql_affected_rows();
	else
		return false;		
}


function signup($HW, $hw_id, $sess_id, $dry_id, $dry_sess, $sms, &$error){
	if(!UID){
		$error = "Необходимо войти в систему";
		return false;
	}

	set_sms_cookies($sms);
	
	$signup_drier_only = ($HW[$hw_id]['sessions'][$sess_id]['s'] == 3)&&($HW[$hw_id]['sessions'][$sess_id]['dry_id'] == 0)
					&&($HW[$hw_id]['sessions'][$sess_id]['uid'] == UID);
					
		
	if($signup_drier_only)
		$price = 0;
	else
		$price = $price1 = get_cost($HW[$hw_id],$sess_id, $comments);
	
	if($dry_id){
		$price2 = get_cost($HW[$dry_id],$dry_sess, $comments);
		$price = $price + $price2;
	}
	
	if ($price > BALANCE+ALLOW_NEGATIVE_MONEY) {
		$error = "Недостаточно средств на счету";
		return false;
	}
		
	if($signup_drier_only)
		
		$wid = $HW[$hw_id]['sessions'][$sess_id]['sid'];
	
	else{
		
		$price1 = get_cost($HW[$hw_id],$sess_id,$comments,true);
		
		if(!($wid = signup_session($hw_id, $sess_id, $sess_id+$HW[$hw_id]['quant'],  $price1, $sms[1],$sms[2]))){
			$error = mysql_error();
			return false;
		}
	
		if(!reserve_money($price1, $wid, $sess_id)) {
			delete_session($wid);
			$error = money_error();
			return false;
		}
	}
	
	if($dry_id){
		$price2 =get_cost($HW[$dry_id],$dry_sess,$comments,true);
		
		if(!($did = signup_session($dry_id, $dry_sess, $dry_sess+$HW[$dry_id]['quant'], $price2, $sms[3],$sms[4]))){
			$error = "Стирка зарегистрированна, при записи на сушку возникла ошибка ".mysql_error();
			return false;					
		}
				
		if(!reserve_money($price2,$did,$dry_sess)) {
			delete_session($did);
			$error = "Стирка зарегистрированна, при записи на сушку возникла ошибка ".money_error();
			return false;
		}

		set_dry_id($wid,$did);	
	}
	
	return true;
	
}

function set_sms_cookies($sms){
	global $prefs;

	$cookie = @$prefs['general']['cookie'];

	if (isset($sms[1])) sendcookie($cookie.'_sms_bw', $sms[1], 0);
	if (isset($sms[2])) sendcookie($cookie.'_sms_aw', $sms[2], 0);
	if (isset($sms[3])) sendcookie($cookie.'_sms_bd', $sms[3], 0);
	if (isset($sms[4])) sendcookie($cookie.'_sms_ad', $sms[4], 0);
}

function get_washes($for_last = 0){
	$mres = mysql_query("SELECT `s_id`, `s_start`, `s_stop`, `hw_name`, `hw_id`, `hw_type`, `s_dry_id`, `d_name`, `d_id` FROM `op_signups` 
					JOIN `op_hardware` ON (`hw_id` = `s_hid`) 
					JOIN `op_dorms` ON (`hw_dorm` = `d_id`)
					WHERE (`s_stop` > ".(t()-$for_last).")AND(`s_uid` = ".UID.") 
					ORDER BY `s_start` ASC, `hw_type` ASC");
	if(!$mres) 
		return false;
	$washes = array();
	while($row = mysql_fetch_array($mres)){
		$washes[$row['s_id']]['start'] = $row['s_start'];
		$washes[$row['s_id']]['stop'] = $row['s_stop'];
		$washes[$row['s_id']]['hw_name'] = $row['hw_name'];
		$washes[$row['s_id']]['hw_id'] = $row['hw_id'];
		$washes[$row['s_id']]['d_name'] = $row['d_name'];
		$washes[$row['s_id']]['d_id'] = $row['d_id'];
		$washes[$row['s_id']]['type'] = $row['hw_type'];
		$washes[$row['s_id']]['dry_id'] = $row['s_dry_id'];
		
	}
	return $washes;
}

function calcel_wash($id, &$error){
	$washes = get_washes();
	if(!isset($washes[$id])){
		$error = "Запись не найдена.";		
		return true;
	}
	
	$w = $washes[$id];
	
	if( ($w['start'] - t()) < 60*2 ){
		$error = "Отменить запись можно не позже, чем за 2 часа.";		
		return false;
	}
	
	if($w['dry_id'])
		if(!calcel_wash($w['dry_id'],$error))
			return false;
		
	
		
	if(unreserve_money($id))
		delete_session($id);
	else{
		$error = money_error();		
		return false;
	}
		
		
	return true;
}

function current_washes(){
  $time = t();
  $mres = mysql_query("SELECT `hw_name`, c_name, c_room, c_start, c_stop, l_name, l_room,  l_start, l_stop, n_name, n_room, n_start, n_stop, stoped
   FROM `op_hardware`
   LEFT OUTER JOIN (
   SELECT c.`s_hid` as c_hid, c.`s_start` as c_start, c.`s_stop` as c_stop, CONCAT(`u_surname`,' ',SUBSTR(`u_firstname`,1,1),'.') as c_name,`u_room` as c_room FROM `op_signups` as c, 
                          (SELECT `s_hid`, MIN(`s_start`) as min FROM   `op_signups` WHERE (`s_start` <= $time)AND(`s_stop` > $time)AND(NOT(`s_status` & 4)) GROUP BY (`s_hid`)) as c_aux, `op_users` as c_usr 
                          WHERE (c.`s_hid` = c_aux.`s_hid`)AND(c.`s_start` = c_aux.min)AND(c_usr.`u_id` = c.`s_uid`)
   ) as current
   ON c_hid = `hw_id`
   
   LEFT OUTER JOIN (
     SELECT l.`s_hid` as l_hid, l.`s_start` as l_start, l.`s_stop` as l_stop,  CONCAT(`u_surname`,' ',SUBSTR(`u_firstname`,1,1),'.') as l_name,`u_room` as l_room FROM `op_signups` as l, 
                    (SELECT `s_hid`, MAX(`s_start`) as max FROM   `op_signups` WHERE (`s_stop` <= $time)AND(NOT(`s_status` & 4)) GROUP BY (`s_hid`)) as l_aux, `op_users` as l_usr 
                    WHERE (l.`s_hid` = l_aux.`s_hid`)AND(l.`s_start` = l_aux.max)AND(l_usr.`u_id` = l.`s_uid`)
   ) as last
   ON l_hid = `hw_id`
   
   LEFT OUTER JOIN (
   SELECT n.`s_hid` as n_hid, n.`s_start` as n_start, n.`s_stop` as n_stop, CONCAT(`u_surname`,' ',SUBSTR(`u_firstname`,1,1),'.') as n_name,`u_room` as n_room FROM `op_signups` as n, 
                          (SELECT `s_hid`, MIN(`s_start`) as min FROM   `op_signups` WHERE (`s_start` > $time)AND(NOT(`s_status` & 4)) GROUP BY (`s_hid`)) as n_aux, `op_users` as n_usr 
                          WHERE (n.`s_hid` = n_aux.`s_hid`)AND(n.`s_start` = n_aux.min)AND(n_usr.`u_id` = n.`s_uid`)
   ) as next
   ON n_hid = `hw_id`
   
   LEFT OUTER JOIN (
    SELECT `stop_hid`, COUNT(*) as stoped FROM `op_stops` WHERE (`stop_since` <= $time)AND(`stop_until` >= $time)
    GROUP BY `stop_hid`
   ) as stops
   ON `stop_hid` = `hw_id`
                            
                            WHERE (`hw_type` != 3)AND(`hw_status` < 2)  ORDER BY `hw_type` ASC, `hw_name` ASC");
  if(!$mres){
    return false;
  }
  $res = array();
  while($row = mysql_fetch_assoc($mres))
    $res[] = $row;
  return $res;
}


?>
