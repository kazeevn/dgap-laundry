<?php
/**
 * На этот скрипт приходят уведомления от QIWI Кошелька.
 * SoapServer парсит входящий SOAP-запрос, извлекает значения тегов login, password, txn, status,
 * помещает их в объект класса Param и вызывает функцию updateBill объекта класса TestServer.
 *
 * Логика обработки магазином уведомления должна быть в updateBill.
 */

define("LOG",'./phpdump.txt');

 $s = new SoapServer('IShopClientWS.wsdl', array('classmap' => array('tns:updateBill' => 'Param', 'tns:updateBillResponse' => 'Response')));
// $s = new SoapServer('IShopClientWS.wsdl');
 $s->setClass('TestServer');
 $s->handle();

 class Response {
  public $updateBillResult;
 }

 class Param {
  public $login;
  public $password;
  public $txn;      
  public $status;
 }

 class TestServer {
  function updateBill($param) {
  
	// Выводим все принятые параметры в качестве примера и для отладки
  $f = fopen(LOG, 'a');
	fwrite($f, date("r"));
	fwrite($f, ' l:');
	fwrite($f, $param->login);
	fwrite($f, ' p:');
	fwrite($f, $param->password);
	fwrite($f, ' t:');
	fwrite($f, $param->txn);
	fwrite($f, ' s:');
	fwrite($f, $param->status);
	
	$result = 1000;
	
	include("../../prefs.php");
	$conn = mysql_connect($prefs['dbase']['host'], $prefs['dbase']['username'], $prefs['dbase']['password']);
	if(!$conn){
		$result = 1;
	} elseif (!mysql_select_db($prefs['dbase']['database'])){
		$result = 2;
	} else {
	
		mysql_set_charset('utf8',$conn); 
	
		
		// проверить password, login
		include("auth.php");
		if( $param->login !=  QIWILOGIN)
			$result = 5;
		elseif ( $param->password != strtoupper(md5($param->txn . strtoupper(md5(QIWIIDPASSWORD)))))
			$result = 6;		
		// В зависимости от статуса счета $param->status меняем статус заказа в магазине
		else if ($param->status == 60) {
			$mres = mq("UPDATE `op_transactions`, `op_users` SET
                           	`t_status` = 1,
                           	`u_money` = `u_money` + `t_summ`
                           WHERE `u_id` = `t_uid` AND `t_id` = '".$param->txn."' AND `t_status` != 1",$f);
			if($mres)
				$result = 0;
			else
				$result = mysql_errno() * 10 + 3;
		} else if ($param->status > 100) {
			$mres = mq("UPDATE `op_transactions` SET `t_status` = 2  WHERE `t_id` = '".$param->txn."'",$f);
			if($mres)
				$result = 0;
			else
				$result = mysql_errno() * 10 + 4;
			
		} else if ($param->status >= 50 && $param->status < 60) {
			// счет в процессе проведения
			$result = 0;
		} else {
			// неизвестный статус заказа
			$result = 0;
		}
	
	}

	// формируем ответ на уведомление
	// если все операции по обновлению статуса заказа в магазине прошли успешно, отвечаем кодом 0
	// $temp->updateBillResult = 0
	// если произошли временные ошибки (например, недоступность БД), отвечаем ненулевым кодом
	// в этом случае QIWI Кошелёк будет периодически посылать повторные уведомления пока не получит код 0
	// или не пройдет 24 часа
	
	fwrite($f, ' r:');
	fwrite($f, $result);
	fwrite($f, "\n");
	fclose($f);
	
	$temp = new Response();
	$temp->updateBillResult = $result;
	return $temp;
  }
 }
 
function mq($query,$f){
	$mres = mysql_query($query);
	//$mres = false;
	
	fwrite($f, ' Query: ');
	fwrite($f, $query);
	fwrite($f, ' mr:');
	fwrite($f, $mres);
	fwrite($f, ' ar:');
	fwrite($f, mysql_affected_rows());

	
	return $mres;
}
 
?>
