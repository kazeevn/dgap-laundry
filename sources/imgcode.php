<?php 
if(!defined("SUPINIT")){exit;} 

header("Content-type: image/png", true);


$small_width = 60+40; //
$small_height = 120+20; // Single letter image.

$img_width = 170;
$img_height = 60;

$ib_width = $img_width*2;
$ib_height = $img_height*2;

$ib = imagecreatetruecolor($ib_width, $ib_height);

$white = imagecolorallocate($ib, 255, 255, 255);

$font = imageloadfont('images/font.gdf');

imagefill($ib,0,0,$white);

$str = 'abcdeghkmnpqsuvwxyz'.'ABDEFGHKLMNPRSTUVWXYZ'.'123456789';
$text = '';

$cl    = imagecolorallocate($ib, mt_rand(0,200), mt_rand(0,200), mt_rand(0,200));
for($i = 0; $i <6; $i++){
	$a = $str[mt_rand(0,strlen($str)-1)];
	$text .= $a;
	
	$small = imagecreatetruecolor($small_width,$small_height);
	imagefill($small,0,0,$white);
	imagechar($small,$font,0,0,$a,$cl);
	if(function_exists("imagerotate")){
	$rot =imagerotate($small,mt_rand(-25,25),$white);
	imagedestroy($small);
	}else{
	$rot = $small;
	}
	imagecolortransparent($rot,$white);
	imagecopymerge($ib,$rot,7+$i*48+mt_rand(-3,3) ,mt_rand(0,20) ,0,30, $small_width , $small_height -30, 100);
	imagedestroy($rot);
}

$im = imagecreatetruecolor($img_width, $img_height);
imagecopyresampled($im,$ib,0,0,0,0,$img_width, $img_height, $ib_width,$ib_height);
imagedestroy($ib);

imagepng($im);
imagedestroy($im);

$_SESSION['img_text'] = $text;
?>