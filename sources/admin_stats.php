<?php 
if(!defined("SUPINIT")){exit;}
if(!USER){exit;}
if(!(ADMIN || MODERATOR)){exit;}
if(!defined("ADMIN_DORM")) {
  $l = get_user_room_and_dorm();
  $dorm = $l['dorm'];
  if($dorm == 0)
    die(err("Не задан номер общежития."));

  define("ADMIN_DORM",$dorm);
}
if(!defined("CAPTION"))
  define('CAPTION',"Статистика пользования стиралкой № ".ADMIN_DORM);

if($_GET['since'] && ( ($s = strtotime($_GET['since'])) != -1 ) )
	$s = floor($s / 60);
else
	$s = start_of_the_day(t());
	
if($_GET['until'] && ( ($u = strtotime($_GET['until'])) != -1 ) )
	$u = floor($u / 60);
else
	$u = t();
	

?>
<form action="" method="get">
<input type="hidden" name="act" value="admin" /> <input type="hidden" name="sub" value="stats" />
 Статистика с <input id="since" name="since" value="<?php echo format_datetime($s) ?>" /> 
 	по <input id="until"  name="until" value="<?php echo format_datetime($u) ?>" /> 
   <input type="submit" value="Показать"  />
</form>
<?php 
	include("jquery/datetime.php");
	datetime_picker("since");
	datetime_picker("until");
?>
<table width="100%"   style='border-collapse:collapse;' id='hw_list'>
  <tr>
    <td>Оборудование</td>
    <td>Сессий</td>
    <td>Прибыль</td>	
    <td>Время работы</td>
    <td>Время остановок</td>
    <td>SMS</td>
  </tr>
   <?php
    $mres = mysql_query("		
		SELECT `hw_id`, `hw_type`, `hw_name`, sess, pay, work ,stops, pre_sms, post_sms
		FROM `op_hardware`
		LEFT OUTER JOIN 
			(SELECT `s_hid`, COUNT(*) as sess, SUM(`s_cost`) as pay, SUM(LEAST(`s_stop`,$u)-GREATEST($s,`s_start`))  as work 
			FROM `op_signups` WHERE (`s_status` & 2)AND(`s_start` <= $u)AND(`s_stop` >= $s) GROUP BY `s_hid` ) 
			as `sess_table` 
		ON (`s_hid` = `hw_id`) 
		LEFT OUTER JOIN 
			(SELECT `s_hid` as hid_1, COUNT(*) as pre_sms
			FROM `op_signups` WHERE (`s_status` & 8)AND(`s_start` <= $u)AND(`s_stop` >= $s) GROUP BY `s_hid` ) 
			as `pre_sms_table` 
		ON (hid_1 = `hw_id`)
		LEFT OUTER JOIN 
			(SELECT `s_hid` as hid_2, COUNT(*) as post_sms
			FROM `op_signups` WHERE (`s_status` & 16)AND(`s_start` <= $u)AND(`s_stop` >= $s) GROUP BY `s_hid` ) 
			as `post_sms_table` 
		ON (hid_2 = `hw_id`)		
		LEFT OUTER JOIN 
			(SELECT `stop_hid`, SUM(LEAST(`stop_until`,$u) - GREATEST($s,`stop_since`)) as stops 
			 FROM `op_stops` WHERE (`stop_until` >= $s)AND(`stop_since` <= $u) GROUP BY `stop_hid`) as `stops_table` 
		ON (`stop_hid` = `hw_id`)
		WHERE `hw_dorm` = ".ADMIN_DORM."
		ORDER BY `hw_type` ASC, `hw_id` ASC
		");
	
	echo mysql_error();
	$total_income = 0;
	$total_sms = 0;
	
	while($row = @mysql_fetch_array($mres)){
	$total_income += $row['pay'];
	$total_sms += ($row['pre_sms'] + $row['post_sms']);
	echo "<tr  style='border-top: 1px dotted #CCC'>
    <td>$row[hw_name]</td>
	<td>$row[sess]</td>
	<td>".costtostr($row["pay"],false)."</td>
	<td>".( round($row["work"] / ($u-$s) * 100) )."%, ".timetostr($row["work"])."</td>
	<td>".( round($row["stops"] / ($u-$s) * 100) )."%, ".timetostr($row["stops"])."</td>
   	<td>".($row['pre_sms']?$row['pre_sms']:"0")." + ".($row['post_sms']?$row['post_sms']:"0")
				." = ".($row['pre_sms'] + $row['post_sms'])."</td>
	 </tr>";
	} 
  ?>
  <tr>
    <td></td>
    <td align="right"><strong>Итого:</strong></td>
    <td><?php echo costtostr($total_income,false); ?></td>	
    <td></td>
    <td></td>
    <td><?php echo $total_sms." (".costtostr($total_sms*50,false).")"; ?></td>
  </tr>  
</table>
