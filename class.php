<?php 
$start_time = microtime(TRUE);
define('LOCAL',$_SERVER['HTTP_HOST']=='localhost');
if(!LOCAL) error_reporting(0);

$openproject_init = true;
require_once('prefs.php');
require_once("sources/init.pay.php");
require_once("sources/init.lang.php");
require_once("sources/init.utils.php");
require_once("sources/init.db.php");
require_once("sources/init.ip.php");
require_once("sources/init.card.php");
require_once("sources/init.user.php");
require_once("sources/init.page.php");
require_once("sources/init.wash.php");
unset($openproject_init);

if(defined('OFFMESSAGE')){ require('sources/offline.php'); exit ; }

// Page generation.

define("SUPINIT",TRUE);
	
$file = $_GET['act'].".php";
$script = "sources/".$file;
$lang = "sources/language/".LANG."/".$file;

if(file_exists($lang)){require_once($lang);}

header("Content-Type: text/html; charset=".CHARSET);

if(isset($_GET['plain'])){
	if(file_exists($script)) {	require_once($script);}
	exit;
	}	
	
	
if(isset($_GET['popup'])){
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
	<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET;?>" />
	<link href="default.css" rel="stylesheet" type="text/css" />
	<script src="ajax.js?v=2" type="text/javascript" ></script>
</head><body style="background:none">
<?php
define("POPUP",TRUE);
if(file_exists($script)) {	require_once($script);}
exit;
echo "</body></html>";
}		
	
$lflang = "sources/language/".LANG."/loginform.php";
if(file_exists($lflang)){include_once($lflang);}
define('LOGIN_FORM',addspace(get_require("sources/loginform.php"),4));
unset($lflang);

$lflang = "sources/language/".LANG."/latest.php";
if(file_exists($lflang)){include_once($lflang);}
define('LATEST_PROJ',addspace(get_require("sources/latest.php"),4));
unset($lflang);

if(file_exists($script)){
	    ob_start();
        require $script;
        $text = ob_get_contents();
        ob_end_clean();
}else{$text =  LAN_INDEX_10;}
define('PAGE',addspace($text,4)) ;
unset($text);
 

define('TITLE',gettitle() );
if(!defined('CAPTION')) define('CAPTION','');

define('EXTRA_LANG',parse_langs(false));

$end_time = microtime(TRUE);

define('TIME',round(($end_time-$start_time)*1000));

?>
