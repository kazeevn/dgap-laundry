<?
	if($_SERVER['SERVER_PORT'] != 11001)
		die();

define("LOG",'./phpdump.txt');
	global $f;
  $f = fopen(LOG, 'a');
	fwrite($f, date("r"));
	fwrite($f, ', ');
	fwrite($f, $_SERVER['QUERY_STRING']);


	$openproject_init = true;
	require('prefs.php');
	require("sources/init.pay.php");
	require("sources/init.lang.php");
	require("sources/init.utils.php");
	require("sources/init.db.php");
	unset($openproject_init);
	
	if(defined('OFFMESSAGE'))
		CPResponse(10,OFFMESSAGE);
		
	switch($_GET['action']) {
		case 'check': CPCheck();
		case 'payment': CPPayment();
		case 'status': CPStatus();
		case 'cancel': CPCancel();
	}
	
	CPResponse(1,'Неизвестный тип запроса.');
	
	function CPCancel() {
		if(!(isset($_GET['receipt']) &&  $_GET['receipt']))
			CPResponse(4,"Номер платежа не указан");	
		
		$receipt = $_GET['receipt'];
		if(strlen($receipt) > 255)
			CPResponse(4,"Превышение длины номера платежа");	
			
		$mes = $_GET['mes'];
		if(!is_numeric($mes))
			CPResponse(13,"Неверная причина отмены");	
		if($mes < 0)
			CPResponse(13,"Неверная причина отмены");	
			
	  $mres = mysql_query("SELECT `t_status`, `t_id` FROM `op_transactions`
												 WHERE `t_receipt` = '".mysql_escape_string($receipt)."'
												 ORDER BY `t_id` DESC LIMIT 0, 1");
		if(!$mres)
			CPResponse(10,"Ошибка базы данных");
		
		$row = mysql_fetch_array($mres);
		if($row) 
			if($row['t_status'] == 1)
				CPDoCancel($row['t_id'], $mes);
			elseif($row['t_status'] > 0)
				CPResponse(0,"Платеж отменен", array('authcode' => $row['t_id']));			
			else
				CPResponse(9,"Состояние платежа неизвестно", array('authcode' => $row['t_id']));
		else
			CPResponse(9,"Платеж не найден");
	}
	
	function CPDoCancel($id, $mes) {
		$mres = mysql_query("UPDATE `op_transactions`, `op_users` SET
													`t_status` = 1000+".$mes.",
													`u_money` = `u_money` - `t_summ`
												 WHERE `u_id` = `t_uid` AND 
												 			 `t_id` = '".$id."' AND 
															 `t_status` = 1");
		if($mres && mysql_affected_rows())
			CPResponse(0,"Платеж успешно отменен", array('authcode' => $id));
		else
			CPResponse(10,"Ошибка базы данных");		
	}
	
	function CPStatus() {
		if(!(isset($_GET['receipt']) &&  $_GET['receipt']))
			CPResponse(4,"Номер платежа не указан");		
			
		$receipt = $_GET['receipt'];
		if(strlen($receipt) > 255)
			CPResponse(4,"Превышение длины номера платежа");
		
			
	  $mres = mysql_query("SELECT `t_status`, `t_id` FROM `op_transactions`
												 WHERE `t_receipt` = '".mysql_escape_string($receipt)."'
												 ORDER BY `t_id` DESC LIMIT 0, 1");
		if(!$mres)
			CPResponse(10,"Ошибка базы данных");
		
		$row = mysql_fetch_array($mres);
		if($row) 
			if($row['t_status'] == 1)
				CPResponse(0,"Платеж успешно проведен", array('authcode' => $row['t_id']));
			elseif($row['t_status'] > 0)
				CPResponse(7,"Платеж отменен", array('authcode' => $row['t_id']));			
			else
				CPResponse(8,"Состояние платежа неизвестно", array('authcode' => $row['t_id']));
		else
			CPResponse(6,"Успешный платеж с таким номером не найден");
	}
	
	function CPCHeck() {
		$id = CPGetId();
		if(!$id)
			CPResponse(2,"Абонент не найден");
		CPResponse(0,"Абонент существует, возможен прием платежей");
	}
	
	function CPPayment() {
		$uid = CPGetId();
		if(!$uid)
			CPResponse(2,"Абонент не найден");
		$amount = $_GET['amount'];
		if(!is_numeric($amount))
			CPResponse(3,"Нечисловая сумма платежа");
		if($amount*100 != round($amount*100))
			CPResponse(3,"Превышение точности суммы платежа");
		if($amount < 0)
			CPResponse(3,"Отрицательная сумма");			
			
		$summ = $amount*100;
			
		if(!(isset($_GET['receipt']) &&  $_GET['receipt']))
			CPResponse(4,"Номер платежа не указан");	
			
		$receipt = $_GET['receipt'];
		if(strlen($receipt) > 255)
			CPResponse(4,"Превышение длины номера платежа");
		
		$mres = mysql_query("SELECT `t_status`, `t_summ`, `t_id`, `t_uid` FROM `op_transactions`
												 WHERE `t_receipt` = '".mysql_escape_string($receipt)."'
												 ORDER BY `t_id` DESC LIMIT 0, 1");
		if(!$mres)
			CPResponse(10,"Ошибка базы данных");
		
		$row = mysql_fetch_array($mres);
		if($row) {
			if($row['t_uid'] == $uid)
				if($row['t_summ'] == $summ)
					if($row['t_status'] == 1)
						CPResponse(0,"Платеж уже успешно проведен");
					elseif($row['t_status'] == 0)
						CPDoPay($row['t_id']);
					else
						CPResponse(7,"Платеж с таким номером отменен");
				else
					CPResponse(11,"Сумма платежа не соответствует первоначальной");
			else
				CPResponse(12,"Получатель платежа не соответствует первоначальному");
		
		} else {
			$mres = mysql_query("INSERT INTO `op_transactions` 
																	(`t_uid`, `t_time`, `t_summ`, `t_status`, `t_comment`,
													 				 `t_cashier`, `t_receipt`)
													VALUES ('$uid', NOW() ,  '$summ',  '0',  
																	'".mysql_escape_string("Пополнение баланса через CyberPay #".$receipt)."',  
																	'0',  '".mysql_escape_string($receipt)."')");
			if(!$mres)
				CPResponse(10,"Ошибка базы данных");
			CPDoPay(mysql_insert_id());			
		}		
	}
	
	function CPDoPay($id){
		$mres = mysql_query("UPDATE `op_transactions`, `op_users` SET
													`t_status` = 1,
													`u_money` = `u_money` + `t_summ`
												 WHERE `u_id` = `t_uid` AND 
												 			 `t_id` = '".$id."' AND 
															 `t_status` = 0");
		if($mres)
			CPResponse(0,"Платеж успешно проведен", array('authcode' => $id));
		else
			CPResponse(10,"Ошибка базы данных");
	}
	
	function CPGetId() {
		$number = $_GET['number'];
		$mres = mysql_query("SELECT `u_id` FROM `op_users` WHERE `u_id` = '".mysql_escape_string($number)."'");
		if(!$mres)
			CPResponse(10,"Ошибка базы данных");
		$row = mysql_fetch_array($mres);
		if(!$row)
			return false;
		return $row['u_id'];
	}
	
	
	function CPResponse($code, $message, $other = false) {
		$res = '<?phpxml version="1.0" encoding="windows-1251"?>'."\n".
					 '<response>'."\n".
     			 '<code>'.$code.'</code>'."\n";
		if($_GET['action'] != 'check')
			$res .= "<date>".date("Y-m-d\TH:i:s")."</date>\n";
		if(is_array($other))
			foreach($other as $key=>$value)
				$res .= "<$key>$value</$key>\n";
    $res .= '<message>'.$message.'</message>'."\n".
					  '</response>'."\n";
						
		global $f;
		fwrite($f, ", ");
		fwrite($f, $res);
		fwrite($f, "\n");
		fclose($f);

		
		die(iconv("UTF-8", "CP1251", $res));
	}

?>