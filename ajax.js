var request;
var status_element; 
var updateproc = null;
var ajax_busy = false;

function createRequest() {
  if (request) return;
	
  try {
    request = new XMLHttpRequest();
  } catch (trymicrosoft) {
    try {
      request = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (othermicrosoft) {
      try {
        request = new ActiveXObject("Microsoft.XMLHTTP");
      } catch (failed) {
        request = false;
      }
    }
  }

  if (!request)
    alert("Error initializing XMLHttpRequest!");
}


function checkval_updatePage() {
    if (request.readyState == 4){
		if (request.status == 200)
			if (updateproc == null)
				document.getElementById(status_element).firstChild.nodeValue =  request.responseText;
			else
				setTimeout(updateproc+"(unescape('"+escape(request.responseText)+"')); ", 10);		
		else
			if (request.status == 404)
				alert("Request URL does not exist"); 
			else
				if(request.status != 0)
					alert("Error: status code is " + request.status);
			 
	ajax_busy = false;
	}
}

function checkval(act, subact, val, stat, proc, nonqueue){
	
	 if (!document.getElementById(stat)) {
      var newstat = document.createElement("span");
      newstat.setAttribute("id", stat);
      document.body.appendChild(newstat);
      document.getElementById(stat).style.display = 'none';
	 }
     if (document.getElementById(stat).firstChild == null){
		var text = document.createTextNode("");
		document.getElementById(stat).appendChild(text);
	 }	 
	 document.getElementById(stat).firstChild.nodeValue = 'Загрузка...';
	 
	 if(!request)createRequest();
	 if(!request){return;}
     
	 var par = "";
	 if(proc!=null){
		par = "'"+proc+"'"; 
	 }else{
		par = "null" ;
	 }
	 
	 
     if(ajax_busy){ 
	 	if(nonqueue != null) return; 
		setTimeout("checkval('"+act+"','"+subact+"', unescape('"+escape(val)+"'),'"+stat+"', "+par+")", 250); 
		return; 
	 }
	 ajax_busy = true;
	
	 status_element = stat;	 
	 updateproc = proc;
	 
	 var url = "?plain=1&act="+act+"&sub="+subact+"&val=" + encodeURIComponent(val)+"&rand=" + Math.random();
	 request.open("GET", url, true);
     request.onreadystatechange = checkval_updatePage;
     request.send(null);
}