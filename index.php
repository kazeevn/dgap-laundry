<?php 
//<!--
require('class.php');
//-->
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<!--LinkEchange. Coding by Dimannn. -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?php echo TITLE;?></title>
	<meta name="keywords" content="OpenProject, banner, ad, Adsence, BidVertiser, hit, promote, project, coders, developers, sourceforge" />
	<meta name="description" content="OpenProject developers association" />
	<meta name="verify-v1" content="fA0K2/n8fHJdT62DU1/QaoQd3IHubdjzZx3V6M7VFbQ=" />
	<link href="default.css?v=5" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon"  href="images/wmicon.ico" />
	<script src="ajax.js?v=2" type="text/javascript" ></script>
</head>
<body>
<div id="menu">
	<ul>
		<li <?php if(REAL_ACT=='default'){echo 'class="active" ';}?>><a href="index.php" title=""><?php echo LAN_INDEX_1; ?></a></li>
		<li <?php if(REAL_ACT=='info'){echo 'class="active" ';}?>><a href="?act=info" title="">Информация</a></li>
		    <li <?php if(REAL_ACT=='faq'){echo 'class="active" ';}?>><a href="?act=faq" title="">FAQ</a></li>
        <li <?php if(REAL_ACT=='terms'){echo 'class="active" ';}?>><a href="?act=terms" title=""><?php echo LAN_INDEX_5; ?></a></li>
    </ul>
</div>

<div id="logo">
	<h1><a href="">Стиралка</a></h1>
	<!--- <h2><a href="">.fopf.mipt.ru</a></h2> -->
	<!---<div style="padding-top:22px; padding-left:300px"><?php echo LAN_INDEX_14; ?></div>-->
</div>
	
<div id="content">
	<table width="100%" border="0">
	<tr>
		<td width="170px" style="vertical-align:top ">
		<div id="sidebar">
			<div id="login" class="boxed">
				<h2 class="title"><?php echo LAN_INDEX_8; ?></h2>
				<div class="content">
<?php echo LOGIN_FORM; ?>
				</div>
			</div>
			<div class="boxed">
				<h2 class="title"><?php echo LAN_INDEX_9; ?></h2>
				<div class="content">
<?php echo LATEST_PROJ; ?>
				</div>
			</div>
		</div>
		</td>	
		<td style="vertical-align:top ">
		<div id="main">
			<div id="welcome" class="post">
				<h2 class="title" id="ttl"><?php echo CAPTION; ?></h2>
				<div class="story">
<?php echo PAGE; ?>
				</div>
			</div>
		</div>
		</td>
	</tr>
	</table>
</div>

<div id="footer">
<table width="100%" border="0"><tr>
	<td width="20%" style="text-align:center ">
<?php //echo LANGS; ?> &nbsp;
	</td>
	<td width="10%" style="text-align:center ">&nbsp;
	
    </td>
	<td style="text-align:center ">
		<small>
			<i>
				&copy; <a href="http://dgap-mipt.ru">ФОПФ МФТИ</a>, 2012-<?php echo date("Y"); ?>. 
				<?php echo LAN_INDEX_13; ?> <a href="http://www.freecsstemplates.org/" target='_blank'>Free CSS Templates</a>
			</i>
		</small>
        <div style="color:#999; font-size:9px">Page generation time: <?php echo TIME; ?> msec.</div>
	</td>
	<td width="10%" style="text-align:center ">&nbsp;
    
	</td>
	<td width="20%">
		<center>
		</center>
	</td>
</tr></table>
</div>
<?php echo EXTRA_LANG; ?>
<?php if(@$prefs['ga']['acc']){
	echo "<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', '".$prefs['ga']['acc']."', 'mipt.ru');
  ga('send', 'pageview');

</script>";
}?>
</body>
</html>
